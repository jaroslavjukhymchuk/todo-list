﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.WebApi.Service.Services.CategoryServices
{
    public class CategoryService : ICategoryService
    {

        private readonly CreateClient _setting;
        
        public CategoryService()
        {
            _setting = new CreateClient();
        }

        public IEnumerable<ICategoryModel> Get(string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var categories = new List<CategoryModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Categories/").Result;
                if (response.IsSuccessStatusCode)
                {
                    categories = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                return categories;
            }
        }

        public ICategoryModel GetById(int id, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var category = new CategoryModel();
                var response = client.GetAsync(_setting.AppPath + "/api/Categories?idCategory=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    category = response.Content.ReadAsAsync<List<CategoryModel>>().Result.SingleOrDefault();
                }
                return category;
            }
        }

        public void Add(ICategoryModel newCategory, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                client.PostAsJsonAsync(_setting.AppPath+"/api/Category", newCategory).GetAwaiter().GetResult();
            }
        }

        public void Update(ICategoryModel updateCategory, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                client.PutAsJsonAsync(_setting.AppPath+"/api/Category/" + updateCategory.IDCategory, updateCategory).GetAwaiter().GetResult();
            }
        }

        public void Delete(int idCategory, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DeleteAsync(_setting.AppPath+"/api/Category/" + idCategory).GetAwaiter().GetResult();
            }
        }

        public IEnumerable<ICategoryModel> Search(string nameCategory, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var categories = new List<CategoryModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Categories/?nameCategory=" + nameCategory).Result;
                if (response.IsSuccessStatusCode)
                {
                    categories = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                return categories;
            }
        }
        
    }
}
