﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WebApi.Service.Models.Dto;

namespace TDL.WebApi.Service.Services.TaskServices
{
    public class SharedTaskService : ISharedTaskService
    {
        private readonly CreateClient _setting;

        public SharedTaskService()
        {
            _setting = new CreateClient();
        }

        public IEnumerable<ISharedTaskModel> Get(int idTask, string token)
        {
            using (var client = new HttpClient())
            {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var users = new List<SharedTaskModel>();
            var response = client.GetAsync(_setting.AppPath + "/api/SharedTasks/" + idTask).Result;
            if (response.IsSuccessStatusCode)
            {
                users = SwapToModels(response.Content.ReadAsAsync<List<SharedTaskDto>>().Result);
            }
            return users;
            }
        }

        public void Add(ISharedTaskModel newShared, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                client.PostAsJsonAsync(_setting.AppPath + "/api/SharedTask", newShared).GetAwaiter().GetResult();
            }
        }

        public void Delete(int idSharedTask, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DeleteAsync(_setting.AppPath + "/api/SharedTask/" + idSharedTask).GetAwaiter().GetResult();
            }
        }
        private static SharedTaskModel Swap(SharedTaskDto dto)
        {
            return new SharedTaskModel
            {
                IDSharedTask = dto.IDSharedTask,
                IDTask = dto.IDTask,
                IDUser = dto.IDUser,
                User = dto.User
            };
        }

        private static List<SharedTaskModel> SwapToModels(IEnumerable<SharedTaskDto> dtoes)
        {
            return dtoes.Select(Swap).ToList();

        }
    }
}
