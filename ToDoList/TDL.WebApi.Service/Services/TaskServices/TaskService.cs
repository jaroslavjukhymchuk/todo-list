﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.WebApi.Service.Services.TaskServices
{
    public class TaskService : ITaskService
    {
        private readonly CreateClient _setting;
        
        public TaskService()
        {
            _setting = new CreateClient();

        }

        public IListShortTaskModel Get(int idCategory, string token, int pageSize, int? pageNumber = 1)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var tasksList = new ListShortTaskModel();
                var response = client.GetAsync(_setting.AppPath + "/api/Tasks?idCategory=" +
                                               idCategory + "&pagingParameterModel.pageNumber=" + pageNumber +
                                               "&pagingParameterModel.pageSize=" + pageSize).Result;
                if (!response.IsSuccessStatusCode) return tasksList;
                tasksList.Tasks = response.Content.ReadAsAsync<List<ShortTaskModel>>().Result;
                tasksList.PagingParameter =
                    JsonConvert.DeserializeObject<PagingParameterModel>(response.Headers.GetValues("paging-headers").FirstOrDefault());

                return tasksList;
            }
        }

        public ITaskModel GetById(int idTask, string token, int pageSize = 1)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var task = new TaskModel();
                var response = client.GetAsync(_setting.AppPath + "/api/Tasks?idCategory=" +
                                               0 + "&pagingParameterModel.pageNumber=" + 0 +
                                               "&pagingParameterModel.pageSize=" + pageSize + "&idTask=" + idTask).Result;
                if (response.IsSuccessStatusCode)
                {
                     task = Swap(response.Content.ReadAsAsync<List<TaskDto>>().Result.FirstOrDefault());
                }
                return task;
            }
        }

        public IEnumerable<IShortTaskModel> Search(int idCategory, string nameTask, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var tasks = new List<ShortTaskModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Tasks?idCategory=" +
                                               idCategory + "&nameTask=" + nameTask).Result;
                if (response.IsSuccessStatusCode)
                {
                    tasks = response.Content.ReadAsAsync<List<ShortTaskModel>>().Result;
                }
                return tasks;
            }
        }

        public IListShortTaskModel GetByUser(string token, int pageSize, int? pageNumber = 1)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var tasks = new ListShortTaskModel();
                var response = client.GetAsync(_setting.AppPath +
                                               "/api/Tasks/User?pagingParameterModel.pageNumber=" + pageNumber +
                                               "&pagingParameterModel.pageSize=" + pageSize).Result;
                if (!response.IsSuccessStatusCode) return tasks;
                tasks.Tasks = response.Content.ReadAsAsync<List<ShortTaskModel>>().Result;
                tasks.PagingParameter =JsonConvert.DeserializeObject<PagingParameterModel>(response.Headers.GetValues("paging-headers").FirstOrDefault());
                return tasks;
            }
        }
       
        public void Add(ITaskModel newTask, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                client.PostAsJsonAsync(_setting.AppPath + "/api/Task", newTask).GetAwaiter().GetResult();
            }
        }

        public void Update(ITaskModel updateTask, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                client.PutAsJsonAsync(_setting.AppPath + "/api/Task/" + updateTask.IDTask, updateTask).GetAwaiter().GetResult();
            }
        }

        public void Delete(int idTask, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DeleteAsync(_setting.AppPath + "/api/Task/" + idTask).GetAwaiter().GetResult();
            }
        }

        private TaskModel Swap(TaskDto task)
        {
            return new TaskModel {
                IDTask = task.IDTask,
                IDCategory = task.IDCategory,
                Name = task.Name,
                Description = task.Description,
                Priority = task.Priority,
                DateCreation = task.DateCreation,
                DateFinish = task.DateFinish,
                Status = task.Status
            };
        }
        
    }
}
