﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WebApi.Service.Models.Dto;

namespace TDL.WebApi.Service.Services.CommentServices
{
    public class CommentService : ICommentService
    {

        private readonly CreateClient _setting;

        public CommentService()
        {
            _setting = new CreateClient();
        }

        public IEnumerable<ICommentModel> Get(int idTask, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var comments = new List<CommentModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Comments/" + idTask).Result;
                if (response.IsSuccessStatusCode)
                {
                    comments = SwapToModels(response.Content.ReadAsAsync<List<CommentDto>>().Result);
                }
                return comments;
            }
        }

        public void Add(ICommentModel comment, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                client.PostAsJsonAsync(_setting.AppPath + "/api/Comment", comment).GetAwaiter().GetResult();
            }
        }

        public void Delete(int idComment, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DeleteAsync(_setting.AppPath + "/api/Comment/" + idComment).GetAwaiter().GetResult(); 
            }
        }

        private static CommentModel Swap(CommentDto dto)
        {
            return new CommentModel
            {
                IDComment = dto.IDComment,
                IDTask = dto.IDTask,
                Description = dto.Description,
                DateCreation = dto.DateCreation,
                IDUser = dto.IDUser,
                User = dto.User
            };
        }

        private static List<CommentModel> SwapToModels(IEnumerable<CommentDto> dtoes)
        {
            return dtoes.Select(Swap).ToList();

        }
    }
}
