﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.WebApi.Service.Services.OAuthServices
{
    public class UserService : IUserService
    {
        private readonly CreateClient _setting;
        
        public UserService()
        {
            _setting = new CreateClient();
        }

        public IEnumerable<IShortUserModel> Get(string nameUser, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var users = new List<ShortUserModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Users/Search?name=" + nameUser).Result;
                if (response.IsSuccessStatusCode)
                {
                    users = response.Content.ReadAsAsync<List<ShortUserModel>>().Result;
                }
                return users;
            }
        }

        public IUserModel Get(string token, int? idUser = null)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var user = new UserModel();
                var response = client.GetAsync(_setting.AppPath + "/api/User?idUser=" + idUser).Result;
                if (response.IsSuccessStatusCode)
                {
                    user = response.Content.ReadAsAsync<UserModel>().Result;
                    user.IsAdmin = bool.Parse(response.Headers.GetValues("IsAdmin").FirstOrDefault());
                }
                return user;
            }
        }

        public IEnumerable<IUserModel> GetAll(string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var user = new List<UserModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Users").Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                if (response.IsSuccessStatusCode)
                {
                    user = response.Content.ReadAsAsync<List<UserModel>>().Result;
                }
                return user;
            }
        }

        public IMessageModel Update(IUserModel updateUser, string token, int? idUser = null)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                var response = client.PutAsJsonAsync(_setting.AppPath + "/api/User?idUser=" + idUser, updateUser).Result;
                var message = new MessageModel();
                if (!response.IsSuccessStatusCode)
                {
                 
                    message.Messages = response.Headers.GetValues("ValidationErrors").FirstOrDefault();
                }
                return message;
            }
        }

        public void Delete(string token, int? idUser = null)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DeleteAsync(_setting.AppPath + "/api/User?idUser=" + idUser).GetAwaiter().GetResult(); 
            }
        }
    }
}
