﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using System.Configuration;
using System.Net.Http.Headers;
using System;

namespace TDL.WebApi.Service.Services.OAuthServices
{
    public class OAuthService : IOAuthService
    {
        public Dictionary<string, string> GetTokenDictionary(string userName, string password)
        {
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>( "grant_type", "password" ),
                new KeyValuePair<string, string>( "username", userName ),
                new KeyValuePair<string, string> ( "Password", password )
            };
            var content = new FormUrlEncodedContent(pairs);
            using (var client = new HttpClient())
            {
                var response = client.PostAsync(ConfigurationManager.AppSettings["AppPath"] + "/Token", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var tokenDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                return tokenDictionary;
            }
        }

        public IEnumerable<IExternalLoginModel> GetExternalLogins()
        {
            using (var client = new HttpClient())
            {
                var externalLogins = new List<ExternalLoginModel>();
                var response = client.GetAsync(ConfigurationManager.AppSettings["AppPath"] + "/api/Auth/ExternalLogins?returnUrl=%2F&generateState=true").Result;
                if (response.IsSuccessStatusCode)
                {
                    externalLogins = response.Content.ReadAsAsync<List<ExternalLoginModel>>().Result;
                }
                return externalLogins;
            }
        }
        

        public Dictionary<string, string> ExternalLogin(IExternalLoginDataModel model)
        {
            using (var client = new HttpClient())
            {
                var response = client.PostAsJsonAsync(ConfigurationManager.AppSettings["AppPath"] + "/api/Auth/ExternalLogin",model).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var tokenDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                return tokenDictionary;
            }
        }
       
        public IMessageModel Register(IOAuthModel registerModel)
        {
            var message = new MessageModel();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                var response = client.PostAsJsonAsync(ConfigurationManager.AppSettings["AppPath"] + "/api/Auth/Register", registerModel).Result;
                message.StatusCode = response.StatusCode.ToString();
                if(!response.IsSuccessStatusCode)
                    message.Messages = response.Headers.GetValues("ValidationErrors").FirstOrDefault();
                return message;
            }
        }

        public void LogOut(string token)
        {
            using (var client = new HttpClient())
            {
                var setting = new CreateClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.PostAsync(setting.AppPath + "/api/Auth/Logout", null).GetAwaiter().GetResult(); 
            }
        }
    }
}
