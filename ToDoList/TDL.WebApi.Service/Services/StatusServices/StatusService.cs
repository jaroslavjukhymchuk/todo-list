﻿
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.WebApi.Service.Services.StatusServices
{
    public class StatusService : IStatusService
    {
        private readonly CreateClient _setting;

        public StatusService()
        {
            _setting = new CreateClient();
        }

        public IEnumerable<IStatusModel> Get(string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var statuses = new List<StatusModel>();
                var response = client.GetAsync(_setting.AppPath + "/api/Statuses/").Result;
                if (response.IsSuccessStatusCode)
                {
                    statuses = response.Content.ReadAsAsync<List<StatusModel>>().Result;
                }
                return statuses;
            }
        }

        public IStatusModel GetById(int id, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var status = new StatusModel();
                var response = client.GetAsync(_setting.AppPath + "/api/Status/"+id).Result;
                if (response.IsSuccessStatusCode)
                {
                    status = response.Content.ReadAsAsync<StatusModel>().Result;
                }
                return status;
            }
        }

        public void Add(IStatusModel newStatus, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                var response = client.PostAsJsonAsync(_setting.AppPath + "/api/Status", newStatus).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
            }
        }

        public void Update(IStatusModel updateStatus, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(_setting.MediaType);
                var response = client.PutAsJsonAsync(_setting.AppPath + "/api/Status/" + updateStatus.IDStatus, updateStatus).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
            }
        }

        public void Delete(int idStatus, string token)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Clear();
                var response = client.DeleteAsync(_setting.AppPath + "/api/Status/" + idStatus).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
            }
        }
    }
}
