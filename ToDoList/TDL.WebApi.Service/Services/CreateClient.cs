﻿using System;
using System.Configuration;
using System.Net.Http.Headers;

namespace TDL.WebApi.Service.Services
{
    public class CreateClient
    {
        
        public CreateClient()
        {
            AppPath = ConfigurationManager.AppSettings["AppPath"];
            MediaType = new MediaTypeWithQualityHeaderValue("application/json");
        }

        public string AppPath { get; set; }
        public MediaTypeWithQualityHeaderValue MediaType { get; set; }
    }
}
