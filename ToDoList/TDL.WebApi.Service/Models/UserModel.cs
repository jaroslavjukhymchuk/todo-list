﻿using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class UserModel : IUserModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [RegularExpression("[0-9]{10}")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        public bool IsAdmin { get; set; }
    }
}
