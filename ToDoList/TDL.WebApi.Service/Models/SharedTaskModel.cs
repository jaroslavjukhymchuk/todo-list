﻿
using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class SharedTaskModel : ISharedTaskModel
    {
        [ScaffoldColumn(false)]
        public int IDSharedTask { get; set; }

        [ScaffoldColumn(false)]
        public int IDTask { get; set; }

        [ScaffoldColumn(false)]
        public int IDUser { get; set; }
        
        public IShortUserModel User { get; set; }
    }
}
