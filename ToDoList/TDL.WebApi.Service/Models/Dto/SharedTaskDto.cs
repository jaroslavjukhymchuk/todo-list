﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDL.WebApi.Service.Models.Dto
{
    class SharedTaskDto
    {
        public int IDSharedTask { get; set; }

        public int IDTask { get; set; }

        public int IDUser { get; set; }

        public ShortUserModel User { get; set; }
    }
}
