﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDL.WebApi.Service.Models
{
    class TaskDto
    {
        public int IDTask { get; set; }

        public int IDCategory { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public int Priority { get; set; }
        
        public DateTime DateCreation { get; set; }

        public DateTime? DateFinish { get; set; }
        
        public StatusModel Status { get; set; }
    }
}
