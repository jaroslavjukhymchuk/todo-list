﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDL.WebApi.Service.Models.Dto
{
    class CommentDto
    {
        public int IDComment { get; set; }

        public int IDTask { get; set; }
        
        public string Description { get; set; }

        public DateTime DateCreation { get; set; }

        public int IDUser { get; set; }

        public ShortUserModel User { get; set; }
    }
}
