﻿using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class CategoryModel : ICategoryModel
    {
        [ScaffoldColumn(false)]
        public int IDCategory { get; set; }

        [ScaffoldColumn(false)]
        public int IDUser { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
