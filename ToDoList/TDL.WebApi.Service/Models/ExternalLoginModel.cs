﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class ExternalLoginModel: IExternalLoginModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }
}
