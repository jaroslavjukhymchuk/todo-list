﻿using System;
using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
     public class TaskModel : ITaskModel
    {
        [ScaffoldColumn(false)]
        public int IDTask { get; set; }

        [ScaffoldColumn(false)]
        public int IDCategory { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Range(1, 10)]
        public int Priority { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateCreation { get; set; }

        public DateTime? DateFinish { get; set; }

        [Required]
        public IStatusModel Status { get; set; }
    }
}
