﻿using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class ShortUserModel : IShortUserModel
    {
        public string UserName { get; set; }

        [ScaffoldColumn(false)]
        public int IdUser { get; set; }
    }
}
