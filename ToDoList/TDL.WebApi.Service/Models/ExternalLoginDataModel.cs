﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
   public class ExternalLoginDataModel : IExternalLoginDataModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

        public string DefaultUserName { get; set; }

        public string Email { get; set; }
    }
}
