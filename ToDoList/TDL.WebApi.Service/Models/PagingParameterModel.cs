﻿using Newtonsoft.Json;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public struct PagingParameterModel : IPagingParameterModel
    {
        [JsonProperty("pageSize")]
        public int PageSize { get; set; }

        [JsonProperty("currentPage")]
        public int CurrentPage { get; set; }

        [JsonProperty("totalPages")]
        public int TotalPages { get; set; }

        [JsonProperty("previousPage")]
        public string PreviousPage { get; set; }

        [JsonProperty("nextPage")]
        public string NextPage { get; set; }
    }
}
