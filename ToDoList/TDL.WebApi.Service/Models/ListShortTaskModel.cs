﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class ListShortTaskModel : IListShortTaskModel
    {
        public IEnumerable<IShortTaskModel> Tasks { get; set; }

        public IPagingParameterModel PagingParameter { get; set; }
    }
}
