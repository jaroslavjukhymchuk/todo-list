﻿using System;
using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class StatusModel : IEquatable<StatusModel>, IStatusModel
    {
        [ScaffoldColumn(false)]
        public int IDStatus { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public bool Equals(StatusModel other)
        {
            return other != null && this.IDStatus == other.IDStatus;
        }

        public override int GetHashCode()
        {
            return IDStatus.GetHashCode();
        }
    }
      
    
}
