﻿using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class OAuthModel : IOAuthModel
    {
        [Required]
        [StringLength(50)]
        public string Login { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        
        public string ConfirmPassword { get; set; }

    }
}
