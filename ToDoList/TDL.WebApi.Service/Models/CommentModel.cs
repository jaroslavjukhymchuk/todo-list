﻿using System;
using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class CommentModel : ICommentModel
    {
        [ScaffoldColumn(false)]
        public int IDComment { get; set; }

        [ScaffoldColumn(false)]
        public int IDTask { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        
        public DateTime DateCreation { get; set; }

        [ScaffoldColumn(false)]
        public int IDUser { get; set; }

        public IShortUserModel User { get; set; }

        public string DateCreationString { get; set; }
    }
}
