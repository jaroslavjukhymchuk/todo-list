﻿using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class MessageModel : IMessageModel
    {
        public string StatusCode { get; set; }

        public string Messages { get; set; }
    }
}
