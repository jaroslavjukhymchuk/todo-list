﻿using System;
using System.ComponentModel.DataAnnotations;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.Service.Models
{
    public class ShortTaskModel : IShortTaskModel
    {
        [ScaffoldColumn(false)]
        public int IDTask { get; set; }

        [ScaffoldColumn(false)]
        public int IDCategory { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public int Priority { get; set; }

        public DateTime? DateFinish { get; set; }

        public string DateFinishString { get; set; }

        public string UserName { get; set; }
    }
}
