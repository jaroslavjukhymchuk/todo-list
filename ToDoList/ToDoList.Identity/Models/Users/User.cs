﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ToDoList.Identity.Infrastructure;
using ToDoList.Identity.Models.IDS;

namespace ToDoList.Identity.Models.Users
{
    public class User : IdentityUser<Int32, UserLoginInt32Pk, UserRoleInt32Pk, UserClaimInt32Pk>
    {
        public ClaimsIdentity GenerateUserIdentityAsync(UserManager<User, Int32> manager, string authenticationType)
        {
            var userIdentity =  manager.CreateIdentityAsync(this, authenticationType).Result;
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(EPUserManager manager)
        {
            var userIdentity =  manager.CreateIdentityAsync(this,
                               DefaultAuthenticationTypes.ApplicationCookie).Result;
            
            return userIdentity;
        }
    }
    
}
