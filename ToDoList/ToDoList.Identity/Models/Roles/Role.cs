﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using ToDoList.Identity.Models.IDS;

namespace ToDoList.Identity.Models.Roles
{
    public class Role : IdentityRole<Int32, UserRoleInt32Pk>
    {
        public Role() { }

        public Role(string name) {
            Name = name; }
    }
}
