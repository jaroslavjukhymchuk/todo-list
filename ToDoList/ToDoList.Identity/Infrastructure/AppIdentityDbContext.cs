﻿using ToDoList.Identity.Models.IDS;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using ToDoList.Identity.Models.Roles;
using ToDoList.Identity.Models.Users;

namespace ToDoList.Identity.Infrastructure
{
    public class AppIdentityDbContext : IdentityDbContext<User, Role, Int32,
        UserLoginInt32Pk, UserRoleInt32Pk, UserClaimInt32Pk>
    {
        public AppIdentityDbContext() : base("name=ToDoListContext")
        { }

        static AppIdentityDbContext()
        {
            Database.SetInitializer<AppIdentityDbContext>(new IdentityDbInit());
        }

        public static AppIdentityDbContext ContextCreate()
        {
            return new AppIdentityDbContext();
        }
    }
}
