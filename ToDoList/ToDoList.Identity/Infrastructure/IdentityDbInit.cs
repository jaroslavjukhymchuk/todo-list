﻿using ToDoList.Identity.Models.IDS;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using ToDoList.Identity.Models.Roles;
using ToDoList.Identity.Models.Users;

namespace ToDoList.Identity.Infrastructure
{
    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<AppIdentityDbContext>
    {
        protected override void Seed(AppIdentityDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }

        public void PerformInitialSetup(AppIdentityDbContext context)
        {
            EPUserManager userMgr = new EPUserManager(new UserStore<User, Role, Int32, UserLoginInt32Pk, UserRoleInt32Pk, UserClaimInt32Pk>(context));
            EPRoleManager roleMgr = new EPRoleManager(new RoleStore<Role, Int32, UserRoleInt32Pk>(context));

            string roleName = "Administrators";
            string userName = "God";
            string password = "123qwE";
            string email = "godonly@sky.com";

            if (!roleMgr.RoleExists(roleName))
            {
                roleMgr.Create(new Role(roleName));
            }

            User user = userMgr.FindByName(userName);
            if (user == null)
            {
                userMgr.Create(new User { UserName = userName, Email = email }, password);
                user = userMgr.FindByName(userName);
            }

            if (!userMgr.IsInRole(user.Id, roleName))
            {
                userMgr.AddToRole(user.Id, roleName);
            }
        }
    }
}
