﻿using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Identity.Infrastructure.Validators
{
    public class CustomPasswordValidator : PasswordValidator
    {
        private readonly string[] failPasswords = new[]
        {
            "12345",
            "QWERTY",
            "111111",
            "1q2w3E"
        };

        public override async Task<IdentityResult> ValidateAsync(string pass)
        {
            IdentityResult result = await base.ValidateAsync(pass);
            var upperPass = pass.ToUpper();
            if (failPasswords.Any(x=>x.Contains(upperPass)))
            {
                var errors = result.Errors.ToList();
                errors.Add("The password can not be so simple.");
                result = new IdentityResult(errors);
            }

            return result;
        }
    }
}
