﻿using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ToDoList.Identity.Models.Users;

namespace ToDoList.Identity.Infrastructure.Validators
{
    public class CustomUserValidator : UserValidator<User,Int32>
    {
        private string _userNamePattern = @"^[a-zA-Z0-9]+$";

        public CustomUserValidator(EPUserManager manager) : base(manager) { }

        public override async Task<IdentityResult> ValidateAsync(User user)
        {
            IdentityResult result = await base.ValidateAsync(user);
            List<string> errors = new List<string>();
            if (user.Email.ToLower().EndsWith("@mail.ru"))
            {
                errors.Add("E-mail \"mail.ru\" is not allowed.");
            }
            if (String.IsNullOrEmpty(user.UserName.Trim()))
            {
                errors.Add("You specified an empty name.");
            }
            if (errors.Count > 0)
            {
                return IdentityResult.Failed(errors.ToArray());
            }

            return result;
        }
    }
}
