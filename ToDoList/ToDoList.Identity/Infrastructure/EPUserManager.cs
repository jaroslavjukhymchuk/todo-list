﻿using System.Threading.Tasks;
using ToDoList.Identity.Infrastructure.Validators;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using ToDoList.Identity.Models.IDS;
using Microsoft.AspNet.Identity.EntityFramework;
using ToDoList.Identity.Models.Users;

namespace ToDoList.Identity.Infrastructure
{
    public class EPUserManager : UserManager<User, Int32>
    {

        public EPUserManager(IUserStore<User, Int32> store) : base(store)
        { }

        public static EPUserManager Create(IdentityFactoryOptions<EPUserManager> options, IOwinContext context)
        {
            AppIdentityDbContext db = context.Get<AppIdentityDbContext>();
            EPUserManager manager = new EPUserManager(new UserStoreInt32Pk(db))
            {
                PasswordValidator = new CustomPasswordValidator
                {
                    RequiredLength = 6,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = true,
                    RequireUppercase = false
                }
            };

            manager.UserValidator = new CustomUserValidator(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };
            var dataProtectionProvider = options.DataProtectionProvider;
           
            return manager;
        }

        public override Task<User> FindAsync(UserLoginInfo login)
        {
            return base.FindAsync(login);
        }

        public override Task<IdentityResult> CreateAsync(User user, string password)
        {
            return base.CreateAsync(user,password);
        }
        public override Task<User> FindByIdAsync(Int32 userId)
        {
            return base.FindByIdAsync(userId);
        }
      
    }
}
