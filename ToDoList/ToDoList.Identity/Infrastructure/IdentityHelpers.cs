﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System;

namespace ToDoList.Identity.Infrastructure
{
    public static class IdentityHelpers
    {
        public static MvcHtmlString GetUserName(this HtmlHelper html, Int32 id)
        {
            EPUserManager mgr = HttpContext.Current
                .GetOwinContext().GetUserManager<EPUserManager>();

            return new MvcHtmlString(mgr.FindByIdAsync(id).Result.UserName);
        }
    }
}
