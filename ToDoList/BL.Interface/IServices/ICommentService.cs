﻿using BL.Interface.IDTOs;
using System.Collections.Generic;

namespace BL.Interface.IServices
{
    public interface ICommentService
    {
      
        bool Create(IBLCommentDto dto);
    
        bool Update(IBLCommentDto dto);
   
        bool Delete(int id);

        IBLCommentDto GetById(int id);

        IEnumerable<IBLCommentDto> GetByTask(int taskId);
    }
}
