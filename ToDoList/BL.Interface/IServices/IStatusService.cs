﻿using BL.Interface.IDTOs;
using System.Collections.Generic;

namespace BL.Interface.IServices
{
    public interface IStatusService
    {
      
        bool Create(IBLStatusDto dto);
   
        bool Update(IBLStatusDto dto);
      
        bool Delete(int id);

        IBLStatusDto GetById(int id);
      
        IEnumerable<IBLStatusDto> GetAll();
    }
}
