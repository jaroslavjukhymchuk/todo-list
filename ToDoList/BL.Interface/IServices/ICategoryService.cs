﻿using BL.Interface.IDTOs;
using System.Collections.Generic;

namespace BL.Interface.IServices
{
   public interface ICategoryService
    {
 
        bool Create(IBLCategoryDto dto);
  
        bool Update(IBLCategoryDto dto);
 
        bool Delete(int id);

        IBLCategoryDto GetById(int idUser, int id);

        IEnumerable<IBLCategoryDto> GetByUser(int userId);

        IEnumerable<IBLCategoryDto> GetByName(int userId, string name);
    }
}
