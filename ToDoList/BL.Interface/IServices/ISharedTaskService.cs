﻿using BL.Interface.IDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interface.IServices
{
    public interface ISharedTaskService
    {
        bool Create(IBLSharedTaskDto dto);

        bool Delete(int id);

        IEnumerable<IBLSharedTaskDto> GetUsers(int idTask);
    }
}
