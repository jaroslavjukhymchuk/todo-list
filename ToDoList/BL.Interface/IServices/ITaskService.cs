﻿using BL.Interface.IDTOs.IBLTaskDtoes;
using System.Collections.Generic;

namespace BL.Interface.IServices
{
    public interface ITaskService
    {
    
        bool Create(IBLTaskDto dto);
 
        bool Update(IBLTaskDto dto);
 
        bool Delete(int id);

        IBLTaskDto GetById(int id);

        IBLListShortTaskDto GetByUserId(int id, int numPage, int howGive);

        IBLListShortTaskDto GetByCategory(int idCategory, int numPage, int howGive);

        IEnumerable<IBLShortTaskDto> GetByName(int categoryId, string name);
    }
}
