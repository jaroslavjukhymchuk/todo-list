﻿namespace BL.Interface.IDTOs
{
    public interface IBLStatusDto
    {
        int IDStatus { get; }

        string Name { get; }
    }
}
