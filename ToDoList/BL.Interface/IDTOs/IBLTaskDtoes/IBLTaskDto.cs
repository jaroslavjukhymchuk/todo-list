﻿using System;

namespace BL.Interface.IDTOs.IBLTaskDtoes
{
    public interface IBLTaskDto
    {
        int IDTask { get; }

        int IDCategory { get; }

        string Name { get; }

        string Description { get; }

        int Priority { get; }

        IBLStatusDto Status { get; }

        DateTime DateCreation { get; }

        DateTime? DateFinish { get; }
    }
}
