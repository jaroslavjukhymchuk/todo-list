﻿using System.Collections.Generic;

namespace BL.Interface.IDTOs.IBLTaskDtoes
{
    public interface IBLListShortTaskDto
    {
        int TotalPages { get; }

        IEnumerable<IBLShortTaskDto> Tasks { get; }
    }
}
