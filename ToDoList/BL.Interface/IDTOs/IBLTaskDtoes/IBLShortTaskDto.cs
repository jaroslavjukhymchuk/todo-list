﻿using System;

namespace BL.Interface.IDTOs.IBLTaskDtoes
{
    public interface IBLShortTaskDto
    {
        int IDTask { get; }

        int IDCategory { get; }

        string Name { get; }

        int Priority { get; }

        DateTime? DateFinish { get; }

        string UserName { get; }
    }
}
