﻿using System;

namespace BL.Interface.IDTOs
{
    public interface IBLCommentDto
    {
        int IDComment { get; }

        int IDTask { get; }

        string Description { get; }

        DateTime DateCreation { get; }

        int IDUser { get; }
    }
}
