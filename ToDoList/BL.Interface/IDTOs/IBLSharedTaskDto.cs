﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interface.IDTOs
{
    public interface IBLSharedTaskDto
    {
        int IDSharedTask { get; }

        int IDTask { get; }

        int IDUser { get; }
    }
}
