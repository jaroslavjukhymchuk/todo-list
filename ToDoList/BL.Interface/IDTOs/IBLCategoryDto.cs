﻿namespace BL.Interface.IDTOs
{
    public interface IBLCategoryDto
    {
        int IDCategory { get; }

        int IDUser { get; }

        string Name { get; }
    }
}
