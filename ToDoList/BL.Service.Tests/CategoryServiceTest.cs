﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.Interface.IDTOs;
using BL.Service.Services;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;
using DAL.Repository.DTOs;
using DAL.Repository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BL.Service.Tests
{
    [TestClass]
    public class CategoryServiceTest
    {
        [TestMethod]
        public void GetCategory()
        {
            var mock = new Mock<CategoryRepository>();
            var catService = new CategoryService(mock.Object);
           
            var category = catService.GetByUser(2);

            CollectionAssert.AllItemsAreNotNull(category.ToList());
        }
    }
}
