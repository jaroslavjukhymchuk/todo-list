﻿namespace BL.Service.Resources
{
    class ErrorConstants
    {
        public const string NOT_FOUND_DATA = "Not found data";

        public const string ID = "id =";

        public const string NOT_NULL = "Entity must not be null";
    }
}
