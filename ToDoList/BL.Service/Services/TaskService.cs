﻿using BL.Interface.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using BL.Interface.IDTOs.IBLTaskDtoes;
using BL.Service.Util;
using BL.Service.Resources;
using BL.Service.Dto.TaskDtoes;
using BL.Service.Model.TaskModel;
using DAL.Interface.IDTOs.IDataTasks;
using DAL.Interface.IRepositories;

namespace BL.Service.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;

        private const string _className = nameof(TaskService);

        public TaskService(ITaskRepository taskRep)
        {
            Logger.InitLogger();
            _taskRepository = taskRep;
        }

        public bool Create(IBLTaskDto dto)
        {
            try
            {
                ValidationUtil.NotNull(dto, ErrorConstants.NOT_NULL);
                var result = _taskRepository.Create(BlDtoToDataDto(dto));
                ValidationUtil.CheckNotFound(result, dto.IDTask.ToString(), _className, LogConstants.WasCreated);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            var result = _taskRepository.Delete(id);
            ValidationUtil.CheckNotFoundWithId(result, id, _className);
            return result;
        }


        public IEnumerable<IBLShortTaskDto> GetByName(int categoryId, string name)
        {
            try
            {
                var dataDto = _taskRepository.GetByName(categoryId, name);
                return DataDtoesToShortBlDtoes(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(CategoryService)}: {ex.Message}");
                return null;
            }
        }

        public IBLListShortTaskDto GetByCategory(int idCategory, int numPage, int howGive)
        {
            try
            {
                var dataDto = _taskRepository.GetByCategory(idCategory, numPage, howGive);
                return DataListDtoToBLDtoes(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(TaskService)}: {ex.Message}");
                return null;
            }
        }

        public IBLTaskDto GetById(int id)
        {
            try
            {
                var dataDto = _taskRepository.GetById(id);
                return DataDtoToBlDto(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(TaskService)}: {ex.Message}");
                return null;
            }
        }

        public IBLListShortTaskDto GetByUserId(int id, int numPage, int howGive)
        {
            try
            {
                var dataDto = _taskRepository.GetByUserId(id, numPage, howGive);
                return DataListDtoToBLDtoes(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(TaskService)}: {ex.Message}");
                return null;
            }
        }

        public bool Update(IBLTaskDto dto)
        {
            var result = _taskRepository.Update(BlDtoToDataDto(dto));
            ValidationUtil.CheckNotFoundWithId(result, dto.IDTask, _className);
            return result;
        }

        #region DTO conversion

        /// <summary>
        /// convert bl dto to data dto
        /// </summary>
        /// <param name="model"> bl dto test</param>
        /// <returns>data dto test</returns>
        private IDataTask BlDtoToDataDto(IBLTaskDto model)
        {
            return new BLTaskModel
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                Status = StatusService.BlDtoToDataDto(model.Status),
                DateCreation = model.DateCreation,
                DateFinish = model.DateFinish
            };
        }

        /// <summary>
        /// convert data dto to bl dto
        /// </summary>
        /// <param name="model"> data dto test</param>
        /// <returns>bl dto test</returns>
        private IBLTaskDto DataDtoToBlDto(IDataTask model)
        {
            return new BLTaskDto
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                Status = StatusService.DataDtoToBlDto(model.Status),
                DateCreation = model.DateCreation,
                DateFinish = model.DateFinish
            };
        }

        /// <summary>
        /// convert data short dto in short form bl dto
        /// </summary>
        /// <param name="model">data short dto task</param>
        /// <returns>bl dto short task</returns>
        private IBLShortTaskDto DataDtoToShortBlDto(IDataShortTask model)
        {
            return new BLShortTaskDto
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Priority = model.Priority,
                DateFinish = model.DateFinish,
                UserName = model.UserName
            };
        }

        /// <summary>
        /// convert list data short dto by short form list bl dto
        /// </summary>
        /// <param name="entities">list data short dto task</param>
        /// <returns>short form list bl dto task</returns>
        private IBLListShortTaskDto DataListDtoToBLDtoes(IDataListShortTask entities)
        {
            return new BLListShortTaskDto
            {
                TotalPages = entities.TotalPages,
                Tasks = entities.Tasks.Select(entity => DataDtoToShortBlDto(entity))
            };

        }

        /// <summary>
        /// convert list data dto by list bl dto
        /// </summary>
        /// <param name="entity">list data dto test</param>
        /// <returns>list bl dto test</returns>
        private IEnumerable<IBLTaskDto> DataDtoesToBlDtoes(IEnumerable<IDataTask> entity)
        {
            return entity.Select(task => DataDtoToBlDto(task));

        }

        /// <summary>
        /// convert list data dto by list bl dto
        /// </summary>
        /// <param name="entity">list data dto test</param>
        /// <returns>list bl dto test</returns>
        private IEnumerable<IBLShortTaskDto> DataDtoesToShortBlDtoes(IEnumerable<IDataShortTask> entity)
        {
            return entity.Select(task => DataDtoToShortBlDto(task));

        }


        #endregion
    }
}
