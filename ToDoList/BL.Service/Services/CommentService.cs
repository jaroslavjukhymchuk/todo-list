﻿using BL.Interface.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interface.IDTOs;
using BL.Service.Util;
using BL.Service.Resources;
using BL.Service.Dto;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;

namespace BL.Service.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        private const string _className = nameof(CommentService);

        public CommentService(ICommentRepository commentRep)
        {
            Logger.InitLogger();
            _commentRepository = commentRep;
        }

        public bool Create(IBLCommentDto dto)
        {
            try
            {
                ValidationUtil.NotNull(dto, ErrorConstants.NOT_NULL);
                var result = _commentRepository.Create(BlDtoToDataDto(dto));
                ValidationUtil.CheckNotFound(result, dto.IDComment.ToString(), _className, LogConstants.WasCreated);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            var result = _commentRepository.Delete(id);
            ValidationUtil.CheckNotFoundWithId(result, id, _className);
            return result;
        }

        public IBLCommentDto GetById(int id)
        {
            try
            {
                var dataDto = _commentRepository.GetById(id);
                return DataDtoToBlDto(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(CommentService)}: {ex.Message}");
                return null;
            }
        }

        public IEnumerable<IBLCommentDto> GetByTask(int taskId)
        {
            try
            {
                var dataDto = _commentRepository.GetByTask(taskId);
                return DataDtoesToBlDtoes(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(CommentService)}: {ex.Message}");
                return null;
            }
        }

        public bool Update(IBLCommentDto dto)
        {
            var result = _commentRepository.Update(BlDtoToDataDto(dto));
            ValidationUtil.CheckNotFoundWithId(result, dto.IDComment, _className);
            return result;
        }

        #region DTO conversion

        /// <summary>
        /// convert bl dto to data dto
        /// </summary>
        /// <param name="model"> bl dto comment</param>
        /// <returns>data dto comment</returns>
        private IDataComment BlDtoToDataDto(IBLCommentDto model)
        {
            return new BLCommentDto
            {
                IDComment = model.IDComment,
                IDTask = model.IDTask,
                Description = model.Description,
                DateCreation = model.DateCreation,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert data dto to bl dto
        /// </summary>
        /// <param name="model"> data dto comment</param>
        /// <returns>bl dto comment</returns>
        private IBLCommentDto DataDtoToBlDto(IDataComment model)
        {
            return new BLCommentDto
            {
                IDComment = model.IDComment,
                IDTask = model.IDTask,
                Description = model.Description,
                DateCreation = model.DateCreation,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert list data dto by list bl dto
        /// </summary>
        /// <param name="entity">list data dto comment</param>
        /// <returns>list bl dto comment</returns>
        private IEnumerable<IBLCommentDto> DataDtoesToBlDtoes(IEnumerable<IDataComment> entity)
        {
            return entity.Select(comment => DataDtoToBlDto(comment));

        }

        #endregion
    }
}
