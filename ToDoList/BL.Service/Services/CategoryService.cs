﻿using BL.Interface.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interface.IDTOs;
using BL.Service.Util;
using BL.Service.Resources;
using BL.Service.Dto;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;

namespace BL.Service.Services
{
    public class CategoryService : ICategoryService
    {

        private readonly ICategoryRepository _categoryRepository;

        private const string _className = nameof(CategoryService);

        public CategoryService(ICategoryRepository categoryRep)
        {
            Logger.InitLogger();
            _categoryRepository = categoryRep;
        }

        public bool Create(IBLCategoryDto dto)
        {
            try
            {
                ValidationUtil.NotNull(dto, ErrorConstants.NOT_NULL);
                var result = _categoryRepository.Create(BlDtoToDataDto(dto));
                ValidationUtil.CheckNotFound(result, dto.IDCategory.ToString(), _className, LogConstants.WasCreated);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            var result = _categoryRepository.Delete(id);
           ValidationUtil.CheckNotFoundWithId(result, id, _className);
            return result;
        }
        
        public IBLCategoryDto GetById(int idUser,int id)
        {
            try
            {
                return DataDtoToBlDto(_categoryRepository.GetById(idUser, id));
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(CategoryService)}: {ex.Message}");
                return null;
            }
        }

        public IEnumerable<IBLCategoryDto> GetByName(int userId, string name)
        {
            try
            {
                var dataDto = _categoryRepository.GetByName(userId, name);
                return DataDtoesToBlDtoes(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(CategoryService)}: {ex.Message}");
                return null;
            }
        }

        public IEnumerable<IBLCategoryDto> GetByUser(int userId)
        {
            try
            {
                return DataDtoesToBlDtoes(_categoryRepository.GetByUser(userId));
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
        }

        public bool Update(IBLCategoryDto dto)
        {
            var result = _categoryRepository.Update(BlDtoToDataDto(dto));
            ValidationUtil.CheckNotFoundWithId(result, dto.IDCategory, _className);
            return result;
        }

        #region DTO conversion

        /// <summary>
        /// convert bl dto to data dto
        /// </summary>
        /// <param name="model"> bl dto category</param>
        /// <returns>data dto category</returns>
        private IDataCategory BlDtoToDataDto(IBLCategoryDto model)
        {
            return new BLCategoryDto
            {
                IDCategory = model.IDCategory,
                Name = model.Name,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert data dto to bl dto
        /// </summary>
        /// <param name="model"> data dto category</param>
        /// <returns>bl dto category</returns>
        private IBLCategoryDto DataDtoToBlDto(IDataCategory model)
        {
            return new BLCategoryDto
            {
                IDCategory = model.IDCategory,
                Name = model.Name,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert list data dto by list bl dto
        /// </summary>
        /// <param name="entity">list data dto category</param>
        /// <returns>list bl dto category</returns>
        private IEnumerable<IBLCategoryDto> DataDtoesToBlDtoes(IEnumerable<IDataCategory> entity)
        {
            return entity.Select(category => DataDtoToBlDto(category));

        }

        #endregion
    }
}
