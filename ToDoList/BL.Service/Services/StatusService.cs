﻿using BL.Interface.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using BL.Interface.IDTOs;
using BL.Service.Util;
using BL.Service.Resources;
using BL.Service.Dto;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;

namespace BL.Service.Services
{
    public class StatusService : IStatusService
    {
        private readonly IStatusRepository _statusRepository;

        private const string _className = nameof(StatusService);

        public StatusService(IStatusRepository statusRep)
        {
            Logger.InitLogger();
            _statusRepository = statusRep;
        }

        public bool Create(IBLStatusDto dto)
        {
            try
            {
                ValidationUtil.NotNull(dto, ErrorConstants.NOT_NULL);
                var result = _statusRepository.Create(BlDtoToDataDto(dto));
                ValidationUtil.CheckNotFound(result, dto.IDStatus.ToString(), _className, LogConstants.WasCreated);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            var result = _statusRepository.Delete(id);
            ValidationUtil.CheckNotFoundWithId(result, id, _className);
            return result;
        }

        public IEnumerable<IBLStatusDto> GetAll()
        {
            try
            {
                var dataDto = _statusRepository.GetAll();
                return DataDtoesToBlDtoes(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(StatusService)}: {ex.Message}");
                return null;
            }
        }

        public IBLStatusDto GetById(int id)
        {
            try
            {
                var dataDto = _statusRepository.GetById(id);
                return DataDtoToBlDto(dataDto);
            }
            catch (Exception ex)
            {
                Logger.Log.Fatal($"{typeof(StatusService)}: {ex.Message}");
                return null;
            }
        }

        public bool Update(IBLStatusDto dto)
        {
            var result = _statusRepository.Update(BlDtoToDataDto(dto));
            ValidationUtil.CheckNotFoundWithId(result, dto.IDStatus, _className);
            return result;
        }

        #region DTO conversion

        /// <summary>
        /// convert bl dto to data dto
        /// </summary>
        /// <param name="model"> bl dto status</param>
        /// <returns>data dto status</returns>
        internal static IDataStatus BlDtoToDataDto(IBLStatusDto model)
        {
            return new BLStatusDto
            {
                IDStatus = model.IDStatus,
                Name = model.Name
            };
        }

        /// <summary>
        /// convert data dto to bl dto
        /// </summary>
        /// <param name="model"> data dto status</param>
        /// <returns>bl dto status</returns>
        internal static IBLStatusDto DataDtoToBlDto(IDataStatus model)
        {
            return new BLStatusDto
            {
                IDStatus = model.IDStatus,
                Name = model.Name
            };
        }

        /// <summary>
        /// convert list data dto by list bl dto
        /// </summary>
        /// <param name="entity">list data dto status</param>
        /// <returns>list bl dto status</returns>
        private IEnumerable<IBLStatusDto> DataDtoesToBlDtoes(IEnumerable<IDataStatus> entity)
        {
            return entity.Select(status => DataDtoToBlDto(status));

        }

        #endregion
    }
}
