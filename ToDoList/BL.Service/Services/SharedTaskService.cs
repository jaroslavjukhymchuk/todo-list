﻿using BL.Interface.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interface.IDTOs;
using DAL.Interface.IRepositories;
using BL.Service.Util;
using BL.Service.Resources;
using DAL.Interface.IDTOs;
using BL.Service.Dto;

namespace BL.Service.Services
{
    public class SharedTaskService : ISharedTaskService
    {
        private readonly ISharedTaskRepository _sharedTaskRepository;

        private const string _className = nameof(SharedTaskService);

        public SharedTaskService(ISharedTaskRepository sharedTaskRep)
        {
            Logger.InitLogger();
            _sharedTaskRepository = sharedTaskRep;
        }

        public bool Create(IBLSharedTaskDto dto)
        {
            try
            {
                ValidationUtil.NotNull(dto, ErrorConstants.NOT_NULL);
                var result = _sharedTaskRepository.Create(DtoToEntity(dto));
                return result;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var result = _sharedTaskRepository.Delete(id);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
            
        }
        
        public IEnumerable<IBLSharedTaskDto> GetUsers(int idTask)
        {
            try
            {
                var result = _sharedTaskRepository.GetUsers(idTask);
                return EntitiesToDtoes(result);
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                throw ex;
            }
            
        }

        #region DTO conversion

        /// <summary>
        /// convert bl dto in data dto
        /// </summary>
        /// <param name="model">bl dto shared task</param>
        /// <returns>data dto shared task</returns>
        private IDataSharedTask DtoToEntity(IBLSharedTaskDto model)
        {
            return new BLSharedTaskDto
            {
                IDSharedTask = model.IDSharedTask,
                IDTask = model.IDTask,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert data dto in bl dto
        /// </summary>
        /// <param name="model">data dto shared task</param>
        /// <returns>bl dto shared task</returns>
        private IBLSharedTaskDto EntityToDto(IDataSharedTask model)
        {
            return new BLSharedTaskDto
            {
                IDSharedTask = model.IDSharedTask,
                IDTask = model.IDTask,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert list data dto by list bl dto
        /// </summary>
        /// <param name="entity">list data dto category</param>
        /// <returns>list bl dto category</returns>
        private IEnumerable<IBLSharedTaskDto> EntitiesToDtoes(IEnumerable<IDataSharedTask> entity)
        {
            return entity.Select(category => EntityToDto(category));

        }

        private IEnumerable<IDataSharedTask> DtoesToEntities(IEnumerable<IBLSharedTaskDto> entity)
        {
            return entity.Select(category => DtoToEntity(category));

        }


        #endregion
    }
}
