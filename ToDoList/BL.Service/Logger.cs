﻿using log4net;
using log4net.Config;

namespace BL.Service
{
    static class Logger
    {
        private static ILog log = LogManager.GetLogger("BLLogger");


        public static ILog Log
        {
            get { return log; }
        }

        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}
