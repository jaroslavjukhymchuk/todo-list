﻿using System.Collections.Generic;
using DAL.Interface.IDTOs.IDataTasks;

namespace BL.Service.Model.TaskModel
{
    public class BLListShortTaskModel : IDataListShortTask
    {
        public int TotalPages { get; set; }

        public IEnumerable<IDataShortTask> Tasks { get; set; }
    }
}
