﻿using BL.Interface.IDTOs.IBLTaskDtoes;
using System.Collections.Generic;

namespace BL.Service.Dto.TaskDtoes
{
    public class BLListShortTaskDto : IBLListShortTaskDto
    {
        public int TotalPages { get; set; }

        public IEnumerable<IBLShortTaskDto> Tasks { get; set; }
    }
}
