﻿using BL.Interface.IDTOs.IBLTaskDtoes;
using System;
using DAL.Interface.IDTOs.IDataTasks;

namespace BL.Service.Dto.TaskDtoes
{
    public class BLShortTaskDto : IBLShortTaskDto, IDataShortTask
    {
        public int IDTask { get; set; }

        public int IDCategory { get; set; }

        public string Name { get; set; }

        public int Priority { get; set; }

        public DateTime? DateFinish { get; set; }

        public string UserName { get; set; }
    }
}
