﻿using BL.Interface.IDTOs;
using BL.Interface.IDTOs.IBLTaskDtoes;
using System;

namespace BL.Service.Dto.TaskDtoes
{
    public class BLTaskDto : IBLTaskDto
    {
        public int IDTask { get; set; }

        public int IDCategory { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Priority { get; set; }

        public IBLStatusDto Status { get; set; }

        public DateTime DateCreation { get; set; }

        public DateTime? DateFinish { get; set; }
    }
}
