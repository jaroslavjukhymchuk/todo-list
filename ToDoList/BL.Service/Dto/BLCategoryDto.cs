﻿using BL.Interface.IDTOs;
using DAL.Interface.IDTOs;

namespace BL.Service.Dto
{
    public class BLCategoryDto : IBLCategoryDto, IDataCategory
    {
        public int IDCategory { get; set; }

        public int IDUser { get; set; }

        public string Name { get; set; }
    }
}
