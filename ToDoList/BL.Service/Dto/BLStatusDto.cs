﻿using BL.Interface.IDTOs;
using DAL.Interface.IDTOs;

namespace BL.Service.Dto
{
    public class BLStatusDto : IBLStatusDto, IDataStatus
    {
        public int IDStatus { get; set; }

        public string Name { get; set; }
    }
}
