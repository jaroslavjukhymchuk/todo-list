﻿using BL.Interface.IDTOs;
using DAL.Interface.IDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Service.Dto
{
    public class BLSharedTaskDto : IBLSharedTaskDto, IDataSharedTask
    {
        public int IDSharedTask { get; set; }

        public int IDTask { get; set; }

        public int IDUser { get; set; }
    }
}
