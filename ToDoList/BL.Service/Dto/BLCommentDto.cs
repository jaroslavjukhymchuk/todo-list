﻿using BL.Interface.IDTOs;
using System;
using DAL.Interface.IDTOs;

namespace BL.Service.Dto
{
    public class BLCommentDto : IBLCommentDto, IDataComment
    {
        public int IDComment { get; set; }

        public int IDTask { get; set; }

        public string Description { get; set; }

        public DateTime DateCreation { get; set; }

        public int IDUser { get; set; }
    }
}
