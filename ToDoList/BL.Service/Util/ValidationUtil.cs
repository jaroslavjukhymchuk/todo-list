﻿using BL.Service.Resources;
using System;

namespace BL.Service.Util
{
    class ValidationUtil
    {
        private ValidationUtil()
        {
        }

        public static void CheckNotFoundWithId(bool found, int id, string className)
        {
            CheckNotFound(found, ErrorConstants.ID + id, className);
        }

        public static void CheckNotFound(bool result, String errorMsg, string className, string msg)
        {
            CheckNotFound(result , errorMsg, className);
            Logger.Log.Info($"{className}: {result} {msg}");

        }

        public static void CheckNotFound(bool found, String msg, string className)
        {
            if (!found)
            {
                throw new ArgumentException(ErrorConstants.NOT_FOUND_DATA + msg);
            }
        }

        public static void NotNull(Object o, String message)
        {
            if (o == null)
            {
                throw new ArgumentNullException(message);
            }
        }
    }
}
