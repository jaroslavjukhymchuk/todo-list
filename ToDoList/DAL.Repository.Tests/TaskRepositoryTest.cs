﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using DAL.Repository.DTOs;
using DAL.Repository.DTOs.TaskDtos;
using DAL.Repository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DAL.Repository.Tests
{
    [TestClass]
    public class TaskRepositoryTest
    {
        [TestMethod]
        public void Create_Correct_Result_True()
        {
            var taskRep = new TaskRepository();

            var task = new TaskDto
            {
                IDTask = 0,
                IDCategory = 1,
                Name = "Test",
                Description = "New test",
                Priority = 1,
                Status = new StatusDto { IDStatus = 1 },
                DateCreation = DateTime.Now,
                DateFinish = DateTime.Now
            };

            Assert.IsTrue(taskRep.Create(task));
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateException))]
        public void Create_GenerateExeption()
        {
            var taskRep = new TaskRepository();

            var task = new TaskDto
            {
                IDTask = 0,
                IDCategory = 0,
                Name = "Test",
                Description = "New test",
                Priority = 1,
                Status = new StatusDto { IDStatus = 1 },
                DateCreation = DateTime.Now,
                DateFinish = DateTime.Now
            };

            Assert.IsFalse(taskRep.Create(task));
        }

        [TestMethod]
        public void GetTaskByCategory_IdCat_1_NumPage_0_HowGive_2_Result_NotNull()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetByCategory(1,0,2);

            Assert.IsNotNull(task);
        }

        [TestMethod]
        public void GetTaskByCategory_IdCat_10_NumPage_0_HowGive_2_GenerateExeption()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetByCategory(10, 0, 2);
            CollectionAssert.AllItemsAreNotNull(task.Tasks.ToList());
        }

        [TestMethod]
        public void GetTaskByUserId_Id_2_NumPage_0_HowGive_2_Result_NotNull()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetByUserId(2, 0, 2);

            Assert.IsNotNull(task);
        }

        [TestMethod]
        public void GetTaskByUserId_Id_10_NumPage_0_HowGive_2_GenerateExeption()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetByUserId(10, 0, 2);

            CollectionAssert.AllItemsAreNotNull(task.Tasks.ToList());
        }

        [TestMethod]
        public void GetTaskById_Id_2_Result_NotNull()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetById(2);

            Assert.IsNotNull(task);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetTaskById_Id_3_GenerateExeption()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetById(3);

            Assert.IsNull(task);
        }

        [TestMethod]
        public void GetTaskByName_IdCategory_1_Name_Test_Result_NotNull()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetByName(1, "Test");

            Assert.IsNotNull(task);
        }

        [TestMethod]
        public void GetTaskByName_IdCategory_10_Name_Test_GenerateExeption()
        {
            var taskRep = new TaskRepository();

            var task = taskRep.GetByName(10, "Test");
           
            CollectionAssert.AllItemsAreNotNull(task.ToList());
        }

        [TestMethod]
        public void Delete_Correct_Result_True()
        {
            var taskRep = new TaskRepository();

            Assert.IsTrue(taskRep.Delete(34));
        }

        [TestMethod]
        public void Delete_Correct_Result_False()
        {
            var taskRep = new TaskRepository();

            Assert.IsFalse(taskRep.Delete(32));
        }

        [TestMethod]
        public void Update_Correct_Result_True()
        {
            var taskRep = new TaskRepository();

            var task = new TaskDto
            {
                IDTask = 2,
                IDCategory = 1,
                Name = "Test",
                Description = "New test",
                Priority = 1,
                Status = new StatusDto { IDStatus = 1 },
                DateCreation = DateTime.Now,
                DateFinish = DateTime.Now
            };

            Assert.IsTrue(taskRep.Update(task));
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateConcurrencyException))]
        public void Update_GenerateExeption()
        {
            var taskRep = new TaskRepository();

            var task = new TaskDto
            {
                IDTask = 0,
                IDCategory = 10,
                Name = "Test",
                Description = "New test",
                Priority = 1,
                Status = new StatusDto { IDStatus = 1 },
                DateCreation = DateTime.Now,
                DateFinish = DateTime.Now
            };

            Assert.IsFalse(taskRep.Update(task));
        }
    }
}
