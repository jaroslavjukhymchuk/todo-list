﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using DAL.Repository.DTOs;
using DAL.Repository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DAL.Repository.Tests
{
    [TestClass]
    public class CommentRepositoryTest
    {
        [TestMethod]
        public void Create_Correct_Result_True()
        {
            var commmentRep = new CommentRepository();

            var comment = new CommentDto
            {
                IDComment =0,
                IDTask =2,
                IDUser = 2,
                Description = "test",
                DateCreation = DateTime.Now
            };
            
            Assert.IsTrue(commmentRep.Create(comment));
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateException))]
        public void Create_GenerateExeption()
        {
            var commmentRep = new CommentRepository();

            var comment = new CommentDto
            {
                IDComment = 0,
                IDTask = 23,
                IDUser = 2,
                Description = "test",
                DateCreation = DateTime.Now
            };

            Assert.IsFalse(commmentRep.Create(comment));
        }

       [TestMethod]
        public void Delete_Correct_Result_True()
        {
            var commmentRep = new CommentRepository();
            
            Assert.IsTrue(commmentRep.Delete(2038));
        }

       [TestMethod]
       public void Delete_Correct_Result_False()
       {
           var commmentRep = new CommentRepository();
            
           Assert.IsFalse(commmentRep.Delete(2036));
       }

        [TestMethod]
        public void GetCommentById_Id_2033_Result_NotNull()
        {
            var commmentRep = new CommentRepository();

            var comment = commmentRep.GetById(2033);
            
            Assert.IsNotNull(comment);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetCommentById_Id_2020_GenerateExeption()
        {
            var commmentRep = new CommentRepository();

            var comment = commmentRep.GetById(2020);

            Assert.IsNull(comment);
        }

        [TestMethod]
        public void GetCommentByTask_IdTask_2_Result_NotNull()
        {
            var commmentRep = new CommentRepository();

            var comment = commmentRep.GetByTask(2);
            
            Assert.IsNotNull(comment);
        }

        [TestMethod]
        public void GetCommentByTask_IdTask_5_GenerateExeption()
        {
            var commmentRep = new CommentRepository();

            var comment = commmentRep.GetByTask(5);

            CollectionAssert.AllItemsAreNotNull(comment.ToList());
        }

       [TestMethod]
       public void Update_Correct_Result_True()
       {
           var commmentRep = new CommentRepository();

            var comment = new CommentDto
            {
                IDComment = 2033,
                IDTask = 2,
                IDUser = 2,
                Description = "tests",
                DateCreation = DateTime.Now
            };
            
            Assert.IsTrue(commmentRep.Update(comment));
       }
 
       [TestMethod]
       [ExpectedException(typeof(DbUpdateException))]
       public void Update_GenerateExeption()
       {
           var commmentRep = new CommentRepository();

            var comment = new CommentDto
            {
                IDComment = 2033,
                IDTask = 0,
                IDUser = 2,
                Description = "tests",
                DateCreation = DateTime.Now
            };
            
            Assert.IsFalse(commmentRep.Update(comment));
       }
    }
}
