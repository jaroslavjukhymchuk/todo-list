﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using DAL.Repository.DTOs;
using DAL.Repository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DAL.Repository.Tests
{
    [TestClass]
    public class SharedTaskRepositoryTest
    {
        [TestMethod]
        public void Create_Correct_Result_True()
        {
            var sharedTaskRep = new SharedTaskRepository();

            var sharedTask = new SharedTaskDto
            {
                IDSharedTask = 0,
                IDTask = 2,
                IDUser = 2
            };

            Assert.IsTrue(sharedTaskRep.Create(sharedTask));
        }

         [TestMethod]
         [ExpectedException(typeof(DbUpdateException))]
         public void Create_GenerateExeption()
         {
             var sharedTaskRep = new SharedTaskRepository();

            var sharedTask = new SharedTaskDto
            {
                IDSharedTask = 0,
                IDTask = 23,
                IDUser = 2
            };

            Assert.IsFalse(sharedTaskRep.Create(sharedTask));
         }

         [TestMethod]
         public void Delete_Correct_Result_True()
         {
            var sharedTaskRep = new SharedTaskRepository();

            Assert.IsTrue(sharedTaskRep.Delete(2057));
         }

        [TestMethod]
        public void Delete_Correct_Result_False()
        {
            var sharedTaskRep = new SharedTaskRepository();

            Assert.IsFalse(sharedTaskRep.Delete(2055));
        }

        [TestMethod]
        public void GetSharedTaskByTask_Id_2_Result_NotNull()
        {
            var sharedTaskRep = new SharedTaskRepository();

            var sharedTask = sharedTaskRep.GetUsers(2);

            Assert.IsNotNull(sharedTask);
        }

        [TestMethod]
        public void GetSharedTaskById_Id_2020_GenerateExeption()
        {
            var sharedTaskRep = new SharedTaskRepository();

            var sharedTask = sharedTaskRep.GetUsers(2020);

            CollectionAssert.AllItemsAreNotNull(sharedTask.ToList());
        }
    }
}
