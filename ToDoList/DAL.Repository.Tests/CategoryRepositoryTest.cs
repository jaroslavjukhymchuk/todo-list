﻿using System;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using DAL.Repository.DTOs;
using DAL.Repository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DAL.Repository.Tests
{
    [TestClass]
    public class CategoryRepositoryTest
    {
      
        [TestMethod]
        public void Create_Correct_Result_True()
        {
            var categoryRep = new CategoryRepository();

            var category = new CategoryDto
            {
                IDCategory=2,
                IDUser=2,
                Name="Test"
            };
            
            Assert.IsTrue( categoryRep.Create(category));
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateException))]
        public void Create_GenerateExeption()
        {
            var categoryRep = new CategoryRepository();

            var category = new CategoryDto
            {
                IDCategory = 2,
                IDUser = 0,
                Name = "Test"
            };
            
            Assert.IsFalse(categoryRep.Create(category));
        }

        [TestMethod]
        public void Delete_Correct_Result_True()
        {
            var categoryRep = new CategoryRepository();
            
            Assert.IsTrue(categoryRep.Delete(2148));
        }

        [TestMethod]
        public void Delete_Correct_Result_False()
        {
            var categoryRep = new CategoryRepository();
            
            Assert.IsFalse(categoryRep.Delete(2132));
        }

        [TestMethod]
        public void GetCategoryById_IdUser_5_Id_55_Result_NotNull()
        {
            var categoryRep = new CategoryRepository();

            var category = categoryRep.GetById(5,55);
            
            Assert.IsNotNull(category);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetCategoryById_IdUser_6_Id_55_GenerateExeption()
        {
            var categoryRep = new CategoryRepository();

            var category = categoryRep.GetById(6, 55);
            
            Assert.IsNull(category);
        }

        [TestMethod]
        public void GetCategoryByName_IdUser_5_Name_Good_Result_NotNull()
        {
            var categoryRep = new CategoryRepository();

            var category = categoryRep.GetByName(5, "Good");
            
            Assert.IsNotNull(category);
        }

        [TestMethod]
        public void GetCategoryByName_IdUser_6_Name_Good_GenerateExeption()
        {
            var categoryRep = new CategoryRepository();

            var category = categoryRep.GetByName(6, "Good");

            CollectionAssert.AllItemsAreNotNull(category.ToList());
        }

        [TestMethod]
        public void GetCategoryByUser_Id_2_Result_NotNull()
        {
            var categoryRep = new CategoryRepository();

            var category = categoryRep.GetByUser(2);
            
            Assert.IsNotNull(category);
        }

        [TestMethod]
        public void GetCategoryByUser_Id_2_GenerateExeption()
        {
            var categoryRep = new CategoryRepository();

            var category = categoryRep.GetByUser(50);

            CollectionAssert.AllItemsAreNotNull(category.ToList());
        }

        [TestMethod]
        public void Update_Correct_Result_True()
        {
            var categoryRep = new CategoryRepository();

            var category = new CategoryDto
            {
                IDCategory = 2139,
                IDUser = 2,
                Name = "Tests"
            };
            
            Assert.IsTrue(categoryRep.Update(category));
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateConcurrencyException))]
        public void Update_GenerateExeption()
        {
            var categoryRep = new CategoryRepository();

            var category = new CategoryDto
            {
                IDCategory = 2,
                IDUser = 0,
                Name = "Test"
            };
            
            Assert.IsFalse(categoryRep.Update(category));
        }
    }
}
