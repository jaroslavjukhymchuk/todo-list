﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using DAL.Repository.DTOs;
using DAL.Repository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DAL.Repository.Tests
{
    [TestClass]
    public class StatusRepositoryTest
    {
        [TestMethod]
        public void Create_Correct_Result_True()
        {
            var statusRep = new StatusRepository();

            var status = new StatusDto
            {
                IDStatus = 0,
                Name = "Test"
            };

            Assert.IsTrue(statusRep.Create(status));
        }

        [TestMethod]
        [ExpectedException(typeof(DbEntityValidationException))]
        public void Create_GenerateExeption()
        {
            var statusRep = new StatusRepository();

            var status = new StatusDto();

            Assert.IsFalse(statusRep.Create(status));
        }

        [TestMethod]
        public void Delete_Correct_Result_True()
        {
            var statusRep = new StatusRepository();

            Assert.IsTrue(statusRep.Delete(1007));
        }

        [TestMethod]
        public void Delete_Correct_Result_False()
        {
            var statusRep = new StatusRepository();

            Assert.IsFalse(statusRep.Delete(2132));
        }

        [TestMethod]
        public void GetAllStatus_Result_NotNull()
        {
            var statusRep = new StatusRepository();

            var status = statusRep.GetAll();

            Assert.IsNotNull(status);
        }

        [TestMethod]
        public void GetStatusById_Id_1_Result_NotNull()
        {
            var statusRep = new StatusRepository();

            var status = statusRep.GetById(1);

            Assert.IsNotNull(status);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetStatusById_Id_6_GenerateExeption()
        {
            var statusRep = new StatusRepository();

            var status = statusRep.GetById(6);

            Assert.IsNull(status);
        }

       [TestMethod]
        public void Update_Correct_Result_True()
        {
            var statusRep = new StatusRepository();

            var status = new StatusDto
            {
                IDStatus = 2,
                Name = "News"
            };

            Assert.IsTrue(statusRep.Update(status));
        }

        [TestMethod]
       [ExpectedException(typeof(DbUpdateConcurrencyException))]
       public void Update_GenerateExeption()
       {
           var statusRep = new StatusRepository();

            var status = new StatusDto
            {
                IDStatus = 0,
                Name = "Test"
            };

           Assert.IsFalse(statusRep.Update(status));
       }
    }
}
