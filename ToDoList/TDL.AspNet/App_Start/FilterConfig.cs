﻿using System.Web;
using System.Web.Mvc;
using TDL.AspNet.Filters;

namespace TDL.AspNet
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new HandleAllErrorAttribute());
        }
    }
}
