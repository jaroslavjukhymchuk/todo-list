﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TDL.AspNet.Models;
using TDL.AspNet.PaginationSetting;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskService _taskService;

        private readonly IStatusService _statusService;

        private readonly ICommentService _commentService;

        private string _token;

        private PaginationSeting _paginationSetting;
        

        public TaskController(ITaskService taskService, IStatusService statusService, ICommentService commentService)
        {
            _taskService = taskService;
            _statusService = statusService;
            _commentService = commentService;
            _paginationSetting = new PaginationSeting();
            _token = new Token().Tokens;
        }
        
        public PartialViewResult Index(int idCategory, int page = 1)
        {
            var tasks = SwapToModel(_taskService.Get(idCategory, _token, _paginationSetting.GetPaginationSetting(), page));
            return PartialView(tasks);
        }

        public ActionResult SharedTask(int page = 1)
        {
            var tasks = SwapToModel(_taskService.GetByUser(_token, _paginationSetting.GetPaginationSetting(), page));
            return View(tasks);
        }

        [HttpGet]
        public ActionResult Create(int idCategory)
        {
            ViewBag.IdCategory = idCategory;
            ViewBag.Statuses = new SelectList(SwapToStatusModels(_statusService.Get(_token)), "IDStatus", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateTaskModel task)
        {
            if (ModelState.IsValid)
            {
                task.DateCreation = DateTime.Now;
                _taskService.Add(SwapCreateModelToModel(task), _token);
                return RedirectToAction("Index", "Category");
            }
            return View();
        }

        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var task = SwapModelToCreateModel( _taskService.GetById(id, _token));
            var statuses = SwapToStatusModels(_statusService.Get(_token));
            var status = statuses.FirstOrDefault(s => (s.IDStatus == task.IDStatus));
            ViewBag.Statuses = new SelectList(statuses, "IDStatus", "Name", IndexOf(statuses, status));
            if(task.DateFinish != null)
            task.DateFinish = task.DateFinish.Value.Date;
            return View(task);
        }

        private int IndexOf(IEnumerable<StatusModel> source, StatusModel value)
        {
            int index = 0;
            var comparer = EqualityComparer<StatusModel>.Default;
            foreach (var item in source)
            {
                if (comparer.Equals(item, value)) return index;
                index++;
            }
            return 0;
        }

        [HttpPost]
        public ActionResult Edit(CreateTaskModel task)
        {
            _taskService.Update(SwapCreateModelToModel(task), _token);
            return RedirectToAction("Edit");
        }

        
        public ActionResult Delete(int id)
        {
            _taskService.Delete(id, _token);
            return RedirectToAction("Index","Category");
        }

        [HttpPost]
        public ActionResult TaskSearch(string title, int idCategory)
        {
            var tasks = SwapToShortModels(_taskService.Search(idCategory,title, _token));
            if (tasks == null)
            {
                ViewBag.Message = "Tasks not found";
                return View();
            }
            return View(tasks);
        }

        #region Swap

        private static ListShortTaskModel SwapToModel(IListShortTaskModel dto)
        {
            return new ListShortTaskModel
            {
                Tasks = dto.Tasks,
                PagingParameter = dto.PagingParameter
            };
        }

        private static ShortTaskModel SwapToShortModel(IShortTaskModel dto)
        {
            return new ShortTaskModel
            {
                IDTask = dto.IDTask,
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                Priority = dto.Priority,
                DateFinish = dto.DateFinish,
                DateFinishString = dto.DateFinishString,
                UserName = dto.UserName
            };
        }

        private static TaskModel SwapToModel(ITaskModel dto)
        {
            return new TaskModel
            {
                IDTask = dto.IDTask,
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                Priority = dto.Priority,
                Description = dto.Description,
                DateFinish = dto.DateFinish,
                DateCreation = dto.DateCreation,
                Status = dto.Status
            };
        }

        private static IEnumerable<ShortTaskModel> SwapToShortModels(IEnumerable<IShortTaskModel> dtoes)
        {
            return dtoes.Select(SwapToShortModel).ToList();

        }

        private static StatusModel SwapToStatusModel(IStatusModel dto)
        {
            return new StatusModel
            {
                IDStatus = dto.IDStatus,
                Name = dto.Name,
            };
        }

        private static IEnumerable<StatusModel> SwapToStatusModels(IEnumerable<IStatusModel> dtoes)
        {
            return dtoes.Select(SwapToStatusModel).ToList();

        }

        private static TaskModel SwapCreateModelToModel(CreateTaskModel createModel)
        {
            return new TaskModel
            {
                IDTask = createModel.IDTask,
                IDCategory = createModel.IDCategory,
                Name = createModel.Name,
                Priority = createModel.Priority,
                Description = createModel.Description,
                DateFinish = createModel.DateFinish,
                DateCreation = createModel.DateCreation,
                Status = new StatusModel { IDStatus = createModel.IDStatus}
            };
        }

        private static CreateTaskModel SwapModelToCreateModel(ITaskModel model)
        {
            return new CreateTaskModel
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Priority = model.Priority,
                Description = model.Description,
                DateFinish = model.DateFinish,
                DateCreation = model.DateCreation,
                IDStatus = model.Status.IDStatus
            };
        }
        #endregion
    }
}