﻿using System.Web.Mvc;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        private string _token;
        

        public UserController(IUserService userService)
        {
            _userService = userService;
            _token = new Token().Tokens;
        }
        
        public ActionResult Information()
        {
           var user = _userService.Get(_token);
           if (user.IsAdmin)
            {
                return View("AdminInformation",user);
            }
            return View(user);
        }

        public ActionResult Edit(UserModel user)
        {
            var result = _userService.Update(user, _token);
            ModelState.AddModelError("", result.Messages);
            return View("Information",user);
        }
        

    }
}