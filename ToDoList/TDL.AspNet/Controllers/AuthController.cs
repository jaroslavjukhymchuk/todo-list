﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using TDL.AspNet.Results;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class AuthController : Controller
    {
        private readonly IOAuthService _authService;

        public AuthController(IOAuthService authService)
        {
            _authService = authService;
        }
        
        public ActionResult LogOff()
        {
            _authService.LogOut(Session["Token"].ToString());
            Session["Token"] = null;
            return RedirectToAction("Login", "Auth");
        }
        
        public ActionResult Login(string error =null)
        {
            ModelState.AddModelError("", error);
            ViewBag.ExtLogins = _authService.GetExternalLogins();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(OAuthModel user)
        {
            try
            {
                Session["Token"] = _authService.GetTokenDictionary(user.Login, user.Password)["access_token"];
                return RedirectToAction("Index", "Category");
            }
            catch (KeyNotFoundException)
            {
                ModelState.AddModelError("", "Login failed! Please provide some valid credentials");
            }
            
            return View();
        }

        [HttpPost]
        public ActionResult ExternalLogin(ExternalLoginModel model)
        {
            return new ChallengeResult(model.Name, Url.Action("ExternalLoginCallback", "Auth"));
        }

        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            var loginInfo =  AuthenticationManager.GetExternalLoginInfo();
            if (loginInfo == null || loginInfo.Email == null)
            {
                var error = "Please add in your account email adress and try againe";
                return RedirectToAction("Login", new { error = error});
            }
            Session["Token"] = _authService.ExternalLogin(SwapToExternalLogin(loginInfo))["access_token"];
            return RedirectToAction("Index", "Category");
        }

        [HttpPost]
        public ActionResult Register(OAuthModel user)
        {
            string error= null;
            if (ModelState.IsValid)
            {
                var result = _authService.Register(user);

                if (result.StatusCode == "OK")
                {
                    return RedirectToAction("Login");
                }
                error = result.Messages;
            }
            return RedirectToAction("Login", new { error = error });
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private IExternalLoginDataModel SwapToExternalLogin(ExternalLoginInfo info)
        {
            return new ExternalLoginDataModel
            {
                LoginProvider = info.Login.LoginProvider,
                ProviderKey = info.Login.ProviderKey,
                DefaultUserName = info.DefaultUserName,
                Email = info.Email
            };
        }
    }
}