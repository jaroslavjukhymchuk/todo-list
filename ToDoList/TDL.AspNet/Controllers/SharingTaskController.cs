﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class SharingTaskController : Controller
    {
        private readonly IUserService _userService;

        private readonly ISharedTaskService _sharedTaskService;

        private string _token;
        

        public SharingTaskController(IUserService userService, ISharedTaskService sharedTaskService)
        {
            _sharedTaskService = sharedTaskService;
            _userService = userService;
            _token = new Token().Tokens;
        }

        [HttpGet]
        public ActionResult Sharing(int idTask, string name )
        {
            ViewBag.TaskName = name;
            ViewBag.IdTask = idTask;
            return View();
        }

        public PartialViewResult AddedUser(int idTask)
        {
            var addedUser = _sharedTaskService.Get(idTask, _token);
            return PartialView(addedUser);
        }

        public PartialViewResult SearchUser(int idTask, string partName)
        {
            ViewBag.IdTask = idTask;
            var availableUser = SwapToUserModels(_userService.Get(partName, _token));
            var users = new List<ShortUserModel>();
            var addedUser = _sharedTaskService.Get(idTask, _token);
           
                for (var i = availableUser.Count() - 1; i >= 0; i--)
                {
                    foreach (var user in addedUser)
                    {
                        if (availableUser[i].IdUser == user.IDUser)
                            users.Add(availableUser[i]);
                    }
                }
            var result = availableUser.Except(users);
            return PartialView(result);
        }

        public void Delete(int id)
        {
            _sharedTaskService.Delete(id, _token);
        }

        public void CreateSharing(int idTask, int idUser)
        {
            _sharedTaskService.Add(new SharedTaskModel
            {
                IDTask = idTask,
                IDUser = idUser
            }, _token);
        }

        #region Swap

        private static SharedTaskModel SwapToModel(ISharedTaskModel dto)
        {
            return new SharedTaskModel
            {
                IDSharedTask = dto.IDSharedTask,
                IDTask = dto.IDTask,
                IDUser = dto.IDUser,
                User = dto.User
            };
        }

        private static IEnumerable<SharedTaskModel> SwapToModels(IEnumerable<ISharedTaskModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        private static ShortUserModel SwapToUserModel(IShortUserModel dto)
        {
            return new ShortUserModel
            {
                UserName = dto.UserName,
                IdUser = dto.IdUser
            };
        }

        private static List<ShortUserModel> SwapToUserModels(IEnumerable<IShortUserModel> dtoes)
        {
            return dtoes.Select(SwapToUserModel).ToList();

        }

        #endregion
    }
}