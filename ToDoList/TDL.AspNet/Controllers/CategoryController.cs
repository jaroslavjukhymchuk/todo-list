﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        private  string _token;
        
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
            _token = new Token().Tokens;
        }
        
        public ActionResult Index()
        {
            var categories = SwapToModels(_categoryService.Get(_token));
            return View(categories);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CategoryModel category)
        {
            _categoryService.Add(category, _token);
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var category = SwapToModel(_categoryService.GetById(id, _token));
            return PartialView(category);
        }

        [HttpPost]
        public ActionResult Edit(CategoryModel category)
        {
            _categoryService.Update(category, _token);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            _categoryService.Delete(id, _token);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CategorySearch(string title)
        {
            var categories = SwapToModels(_categoryService.Search(title, _token));
            if (categories == null)
            {
                ViewBag.Message = "Category not found";
                return View();
            }
            return View("Index", categories);
        }
        
        #region Swap

        private static CategoryModel SwapToModel(ICategoryModel dto)
        {
            return new CategoryModel
            {
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                IDUser = dto.IDUser
            };
        }

        private static IEnumerable<CategoryModel> SwapToModels(IEnumerable<ICategoryModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        #endregion
    }
}