﻿using System;
using System.Web.Mvc;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class StatusController : Controller
    {
        private readonly IStatusService _statusService;

        private  string _token;
        

        public StatusController(IStatusService statusService)
        {
            _statusService = statusService;
            _token = new Token().Tokens;
        }
        
        public ActionResult Index()
        {
            var statuses = _statusService.Get(_token);
            return View(statuses);
        }

        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var status = _statusService.GetById(id, _token);
            return PartialView(status);
        }

        [HttpPost]
        public ActionResult Edit(StatusModel status)
        {
            try
            {
                _statusService.Update(status, _token);
            }
            catch (UnauthorizedAccessException)
            {
                Response.StatusCode = 401;
                return View();
            }
            return RedirectToAction("Index");
        }
        

        [HttpPost]
        public ActionResult Create(StatusModel status)
        {
            try
            {
                _statusService.Add(status, _token);
            }
            catch (UnauthorizedAccessException)
            {
                Response.StatusCode = 401;
                return View();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                _statusService.Delete(id, _token);
            }
            catch (UnauthorizedAccessException)
            {
                Response.StatusCode = 401;
                return View();
            }
            return RedirectToAction("Index");
        }
        
    }
}