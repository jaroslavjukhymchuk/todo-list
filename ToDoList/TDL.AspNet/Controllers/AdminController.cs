﻿using System;
using System.Web.Mvc;
using TDL.AspNet.Models;
using TDL.AspNet.PaginationSetting;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class AdminController : Controller
    {
        private readonly IUserService _userService;

        private string _token;
        

        public AdminController(IUserService userService)
        {
            _userService = userService;
            _token = new Token().Tokens;
        }
        
        public ActionResult Index()
        {
            try
            {
                var users = _userService.GetAll(_token);
                return View(users);
            }
            catch (UnauthorizedAccessException)
            {
                Response.StatusCode = 401;
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var user = _userService.Get(_token, id);
            return View(user);
        }

        [HttpPost]
        public ActionResult Edit(UserModel user)
        {
            var result = _userService.Update(user, _token, user.Id);
            if(result.Messages == null)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", result.Messages);
            return View();
        }

        [HttpGet]
        public ActionResult EditPagination()
        {
            var paginationSetting = new PaginationSeting();
            var paginationModel = new PageSizeSettingModel();
            paginationModel.PageSize = paginationSetting.GetPaginationSetting();
            return View(paginationModel);
        }

        [HttpPost]
        public ActionResult EditPagination(int pageSize)
        {
            var paginationSetting = new PaginationSeting();
            paginationSetting.EditPaginationSetting(pageSize);
            return View();
        }
        
    }
}
