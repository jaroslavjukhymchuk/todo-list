﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.AspNet.Controllers
{
    public class CommentController : Controller
    {

        private readonly ICommentService _commentService;

        private string _token;
        

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
            _token = new Token().Tokens;
        }
        
        public PartialViewResult Index(int idTask)
        {
            ViewBag.IdTask = idTask;
            var comments = SwapToModels(_commentService.Get(idTask, _token));
            return PartialView(comments);
        }

        [HttpPost]
        public ActionResult Create(CommentModel comment)
        {
            comment.DateCreation = DateTime.Now;
            _commentService.Add(comment, _token);
            return RedirectToAction("Edit","Task", new { id= comment.IDTask } );
        }

        public ActionResult Delete(int id, int idTask)
        {
            _commentService.Delete(id, _token);
            return RedirectToAction("Edit", "Task", new { id = idTask });
        }

        private static CommentModel Swap(ICommentModel dto)
        {
            return new CommentModel
            {
                IDComment = dto.IDComment,
                IDTask = dto.IDTask,
                Description = dto.Description,
                DateCreation = dto.DateCreation,
                IDUser = dto.IDUser,
                User = dto.User
            };
        }

        private static List<CommentModel> SwapToModels(IEnumerable<ICommentModel> dtoes)
        {
            return dtoes.Select(Swap).ToList();

        }
    }
}