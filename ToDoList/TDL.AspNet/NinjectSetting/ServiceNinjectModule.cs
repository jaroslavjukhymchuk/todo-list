﻿using Ninject.Modules;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Services.CategoryServices;
using TDL.WebApi.Service.Services.CommentServices;
using TDL.WebApi.Service.Services.OAuthServices;
using TDL.WebApi.Service.Services.StatusServices;
using TDL.WebApi.Service.Services.TaskServices;

namespace TDL.AspNet.NinjectSetting
{
    public class ServiceNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(ICategoryService)).To(typeof(CategoryService));
            Bind(typeof(ICommentService)).To(typeof(CommentService));
            Bind(typeof(IOAuthService)).To(typeof(OAuthService));
            Bind(typeof(ISharedTaskService)).To(typeof(SharedTaskService));
            Bind(typeof(IStatusService)).To(typeof(StatusService));
            Bind(typeof(ITaskService)).To(typeof(TaskService));
            Bind(typeof(IUserService)).To(typeof(UserService));
        }
    }
}