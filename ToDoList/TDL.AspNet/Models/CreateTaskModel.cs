﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TDL.AspNet.Models
{
    public class CreateTaskModel
    {
        public int IDTask { get; set; }
        
        public int IDCategory { get; set; }
        
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        
        [StringLength(50)]
        public string Description { get; set; }
        
        [Range(1, 10)]
        public int Priority { get; set; }
        
        public DateTime DateCreation { get; set; }
        
        public DateTime? DateFinish { get; set; }
        
        public int IDStatus { get; set; }

    }
}