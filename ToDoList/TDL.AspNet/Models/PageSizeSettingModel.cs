﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TDL.AspNet.Models
{
    public class PageSizeSettingModel
    {
        [Range(1, 10)]
        public int PageSize { get; set; }
    }
}