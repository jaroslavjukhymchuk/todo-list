﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace TDL.AspNet.PaginationSetting
{
    public class PaginationSeting
    {
        public void EditPaginationSetting(int newPageSize)
        {
            ConfigurationManager.AppSettings["PageSize"] = newPageSize.ToString();
        }

        public int GetPaginationSetting()
        {
            return int.Parse(ConfigurationManager.AppSettings["PageSize"]);
        }
    }
}