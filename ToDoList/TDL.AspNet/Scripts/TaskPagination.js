﻿$(document).ready(function () {
    $(function () {
        $.ajaxSetup({ cache: false });
        $('.btn-group').on('click', '.pagination-btn', function (e) {
            e.preventDefault();
            $('#taksShow').hide();
            $.get(this.href, function (data) {
                $('#taksShow').html(data).show("slow");
            });
        });
    });
});