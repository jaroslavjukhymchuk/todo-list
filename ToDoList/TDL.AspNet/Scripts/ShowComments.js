﻿$(document).ready(function () {
    
    $(".se-pre-con").fadeIn("slow");
  
    $.get(hrefComment, function (data) {
        $(".se-pre-con").fadeOut("slow");
        $('#comments').hide();
        $('#comments').html(data).slideDown("slow", function () {
            $('#showAddComment').on('click', function (e) {
                e.preventDefault();
                $('#newComment').slideToggle("slow");
            });
        });
        });
});