﻿$(document).ready(function () {
 
    getAddedUser();
    $(function () {
        $.ajaxSetup({ cache: false });
        $('.form-group').on('click', '.search', function (e) {
            e.preventDefault();
            searchUser();
        });
    });
});

function getAddedUser() {
    $.get(hrefAddedUser, function (data) {
        $('#addedUser').addClass("well");
        $('#addedUser').html(data);
    });
}

function deleteUser(href) {
    $.get(href, function (data) {
        getAddedUser();
    });
}

function searchUser() {
   
        
    $.get(hrefSearchUser + '&partName=' + $("#searchName").val(), function (data) {
        $('#availableUser').html(data);
        $('#availableUser').addClass("well");
        });
 

}