﻿$(document).ready(function () {

    $(function () {
        $.ajaxSetup({ cache: false });
        $('.addSharing').on('click', function (e) {
            e.preventDefault();
            $.get(this.href, function (data) {
                getAddedUser();
                searchUser();
            });
        });

    });
});
