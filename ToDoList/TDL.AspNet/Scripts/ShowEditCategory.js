﻿$(document).ready(function () {
    $(".se-pre-con").fadeOut("slow");
    $(function () {
        $.ajaxSetup({ cache: false });
        $('.container').on('click', '.compItem', function (e) {
            e.preventDefault();
            var id = this.id;
            $.get(this.href, function (data) {
                $('#dialogContent_' + id).removeAttr('hidden');
                $('#dialogContent_' + id).hide();
                $('#dialogContent_' + id).html(data).slideDown("slow");
            });
        });
        $('.categoryName').on('click', function (e) {
            e.preventDefault();
           
            $(".se-pre-con").fadeIn("slow");
            $.get(this.href, function (data) {
                $(".se-pre-con").fadeOut("slow");
                $('#taksShow').hide();
                $('#taksShow').addClass("wells");
               
                    $('#taksShow').html(data).show("slow");
               
            });
        });
    });
});
function Cancel(id) {
    $('#dialogContent_' + id).slideUp("slow");
}