﻿using System;
using System.Globalization;
using System.Text;
using System.Web.Mvc;

namespace TDL.AspNet.Helpers
{
    public static class PagingHelpers
    {
            public static MvcHtmlString PageLinks(this HtmlHelper html,
                                        int totalPages, int pageNumber, Func<int, string> pageUrl)
            {
                StringBuilder result = new StringBuilder();
                for (int i = 1; i <= totalPages; i++)
                {
                    TagBuilder tag = new TagBuilder("a");
                    tag.MergeAttribute("href", pageUrl(i));
                    tag.InnerHtml = i.ToString();
                    if (i == pageNumber)
                    {
                        tag.AddCssClass("selected");
                        tag.AddCssClass("btn-primary");
                    }
                    tag.AddCssClass("btn btn-default pagination-btn");
                    result.Append(tag.ToString());
                }
                return MvcHtmlString.Create(result.ToString());
            }
    }
}