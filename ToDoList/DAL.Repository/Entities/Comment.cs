using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.Identity.Models;
using ToDoList.Identity.Models.Users;

namespace DAL.Repository.Entities
{
    [Table("Comment")]
    public partial class Comment 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDComment { get; set; }

        public int IDTask { get; set; }

        public int IDUser { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DateCreation { get; set; }

        [ForeignKey("IDTask")]
        public virtual Task Task { get; set; }

        [ForeignKey("IDUser")]
        public virtual User User { get; set; }
    }
}
