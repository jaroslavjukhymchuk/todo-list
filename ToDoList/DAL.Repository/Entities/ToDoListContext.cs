using System.Data.Entity;

namespace DAL.Repository.Entities
{
    public partial class ToDoListContext : DbContext
    {
        public ToDoListContext()
            : base("name=ToDoListContext")
        {
        }

        public virtual DbSet<Category> Category { get; set; }

        public virtual DbSet<Comment> Comment { get; set; }

        public virtual DbSet<Task> Task { get; set; }

        public virtual DbSet<Status> Status { get; set; }

        public virtual DbSet<SharedTask> SharedTask { get; set; }


    }
}
//Update-Database -ProjectName DAL.Repository -ConnectionString "data source=PC-WN-KR01-0201\SQLEXPRESS;initial catalog=ToDoList;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework" -ConnectionProviderName "System.Data.SqlClient"
//Update-Database -ProjectName DAL.Repository
//Add-Migration InitDb -ProjectName DAL.Repository