using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Repository.Entities
{
    [Table("Task")]
    public partial class Task 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDTask{ get; set; }

        public int IDCategory { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public int Priority { get; set; }
        
        public int IDStatus { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DateCreation { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DateFinish { get; set; }

        [ForeignKey("IDCategory")]
        public virtual Category Category { get; set; }

        [ForeignKey("IDStatus")]
        public virtual Status Status { get; set; }

        public virtual IEnumerable<Comment> Comment { get; set; }

        public virtual IEnumerable<SharedTask> SharedTask { get; set; }
    }
}
