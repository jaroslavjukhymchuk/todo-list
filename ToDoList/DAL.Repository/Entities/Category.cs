using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.Identity.Models;
using ToDoList.Identity.Models.Users;

namespace DAL.Repository.Entities
{
    [Table("Category")]
    public partial class Category 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDCategory { get; set; }

        public int IDUser { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        
        public virtual ICollection<Task> Task { get; set; }

        [ForeignKey("IDUser")]
        public virtual User User { get; set; }
    }
}
