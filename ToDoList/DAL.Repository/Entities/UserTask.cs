﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.Identity.Models;
using ToDoList.Identity.Models.Users;

namespace DAL.Repository.Entities
{
    [Table("SharedTask")]
    public partial class SharedTask
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDSharedTask { get; set; }

        public int IDTask { get; set; }

        public int IDUser { get; set; }

        [ForeignKey("IDTask")]
        public virtual Task Task { get; set; }

        [ForeignKey("IDUser")]
        public virtual User User { get; set; }
    }
}
