﻿using System;
using DAL.Interface.IDTOs;

namespace DAL.Repository.DTOs
{
    public class CommentDto : IDataComment
    {
        public int IDComment { get; set; }

        public int IDTask { get; set; }

        public int IDUser { get; set; }

        public string Description { get; set; }

        public DateTime DateCreation { get; set; }
    }
}
