﻿using DAL.Interface.IDTOs;

namespace DAL.Repository.DTOs
{
    public class CategoryDto : IDataCategory
    {
        public int IDCategory { get; set; }

        public int IDUser { get; set; }

        public string Name { get; set; }
    }
}
