﻿using System;
using DAL.Interface.IDTOs;
using DAL.Interface.IDTOs.IDataTasks;

namespace DAL.Repository.DTOs.TaskDtos
{
    public class TaskDto : IDataTask
    {
        public int IDTask { get; set; }

        public int IDCategory { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Priority { get; set; }

        public IDataStatus Status { get; set; }

        public DateTime DateCreation { get; set; }

        public DateTime? DateFinish { get; set; }
    }
}
