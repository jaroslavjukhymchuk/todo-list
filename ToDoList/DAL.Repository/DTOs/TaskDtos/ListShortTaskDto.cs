﻿using System.Collections.Generic;
using DAL.Interface.IDTOs.IDataTasks;

namespace DAL.Repository.DTOs.TaskDtos
{
    public class ListShortTaskDto : IDataListShortTask
    {
        public int TotalPages { get; set; }

        public IEnumerable<IDataShortTask> Tasks { get; set; }
    }
}
