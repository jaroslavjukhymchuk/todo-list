﻿using System;
using DAL.Interface.IDTOs.IDataTasks;

namespace DAL.Repository.DTOs.TaskDtos
{
    public class ShortTaskDto : IDataShortTask
    {
        public int IDTask { get; set; }

        public int IDCategory { get; set; }

        public string Name { get; set; }

        public int Priority { get; set; }

        public DateTime? DateFinish { get; set; }

        public string UserName { get; set; }
    }
}
