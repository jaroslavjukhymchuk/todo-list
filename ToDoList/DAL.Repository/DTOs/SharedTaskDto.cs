﻿using DAL.Interface.IDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.DTOs
{
    public class SharedTaskDto : IDataSharedTask
    {
        public int IDSharedTask { get; set; }

        public int IDTask { get; set; }

        public int IDUser { get; set; }
    }
}
