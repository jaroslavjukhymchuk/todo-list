﻿using DAL.Interface.IDTOs;

namespace DAL.Repository.DTOs
{
    public class StatusDto : IDataStatus
    {
        public int IDStatus { get; set; }

        public string Name { get; set; }
    }
}
