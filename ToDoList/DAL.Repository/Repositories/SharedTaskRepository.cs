﻿using DAL.Interface.IRepositories;
using DAL.Repository.Entities;
using System.Collections.Generic;
using System.Linq;
using DAL.Interface.IDTOs;
using DAL.Repository.DTOs;

namespace DAL.Repository.Repositories
{
    public class SharedTaskRepository : BaseRepository<SharedTask>, ISharedTaskRepository
    {
        public SharedTaskRepository() : base(new ToDoListContext())
        {

        }

        public bool Create(IDataSharedTask dto)
        {
            return base.Create(DtoToEntity(dto));
        }

        public new bool Delete(int id)
        {
            return base.Delete(id);
        }

        public bool Delete(IEnumerable<IDataSharedTask> dto)
        {
            return base.Delete(DtoesToEntities(dto));
        }

        public IEnumerable<IDataSharedTask> GetUsers(int idTask)
        {
            var result = base.Get(shared => shared.IDTask == idTask);
            return EntitiesToDtoes(result);
        }
        

        #region DTO conversion

        /// <summary>
        /// convert dto in entity
        /// </summary>
        /// <param name="model">dto shared task</param>
        /// <returns>entity shared task</returns>
        private SharedTask DtoToEntity(IDataSharedTask model)
        {
            return new SharedTask
            {
                IDSharedTask = model.IDSharedTask,
                IDTask = model.IDTask,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert entity in dto
        /// </summary>
        /// <param name="model">entity shared task</param>
        /// <returns>dto shared task</returns>
        private IDataSharedTask EntityToDto(SharedTask model)
        {
            return new SharedTaskDto
            {
                IDSharedTask = model.IDSharedTask,
                IDTask = model.IDTask,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert list entity by list dto
        /// </summary>
        /// <param name="entity">list entity category</param>
        /// <returns>list dto category</returns>
        private IEnumerable<IDataSharedTask> EntitiesToDtoes(IEnumerable<SharedTask> entity)
        {
            return entity.Select(category => EntityToDto(category));

        }

        private IEnumerable<SharedTask> DtoesToEntities(IEnumerable<IDataSharedTask> entity)
        {
            return entity.Select(category => DtoToEntity(category));

        }

        #endregion
    }
}
