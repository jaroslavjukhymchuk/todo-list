﻿using System.Collections.Generic;
using System.Linq;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;
using DAL.Repository.Entities;
using DAL.Repository.DTOs;

namespace DAL.Repository.Repositories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository

    {
        public CategoryRepository() : base(new ToDoListContext())
        {

        }
         
        public bool Create(IDataCategory dto)
        {
            return base.Create(DtoToEntity(dto));
        }

        public new bool Delete(int id)
        {
            return base.Delete(id);
        }

        public IDataCategory GetById(int idUser, int id)
        {
            var entity = base.Get(e => e.IDUser == idUser && e.IDCategory == id).SingleOrDefault();
            return EntityToDto(entity);
        }

        public IEnumerable<IDataCategory> GetByName(int userId, string name)
        {
            var listEntities = base.Get(entity => entity.Name.Contains(name) && entity.IDUser == userId);
            return EntitiesToDtoes(listEntities);
        }

        public IEnumerable<IDataCategory> GetByUser(int userId)
        {
            var listEntities = base.Get(entity => entity.IDUser == userId);
            return EntitiesToDtoes(listEntities);
        }

        public bool Update(IDataCategory dto)
        {
            return base.Update(DtoToEntity(dto));
        }

        #region DTO conversion

        /// <summary>
        /// convert dto in entity
        /// </summary>
        /// <param name="model">dto category</param>
        /// <returns>entity category</returns>
        private Category DtoToEntity(IDataCategory model)
        {
            return new Category
            {
                IDCategory = model.IDCategory,
                Name = model.Name,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert entity in dto
        /// </summary>
        /// <param name="model">entity category</param>
        /// <returns>dto category</returns>
        private static IDataCategory EntityToDto(Category model)
        {
            return new CategoryDto
            {
                IDCategory = model.IDCategory,
                Name = model.Name,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert list entity by list dto
        /// </summary>
        /// <param name="entity">list entity category</param>
        /// <returns>list dto category</returns>
        private static IEnumerable<IDataCategory> EntitiesToDtoes(IEnumerable<Category> entity)
        {
            return entity.Select(category => EntityToDto(category));

        }
        
        #endregion
    }

}
