﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Interface.IDTOs.IDataTasks;
using DAL.Interface.IRepositories;
using DAL.Repository.DTOs;
using DAL.Repository.Entities;
using DAL.Repository.DTOs.TaskDtos;

namespace DAL.Repository.Repositories
{
    public class TaskRepository : BaseRepository<Task>, ITaskRepository
    {
        public TaskRepository() : base(new ToDoListContext())
        {

        }

        public bool Create(IDataTask dto)
        {
            return base.Create(DtoToEntity(dto));
        }

        public IDataListShortTask GetByCategory(int idCategory, int numPage, int howGive)
        {
            var count = base.Get(entity => entity.IDCategory == idCategory).Count();
            var entities= base.Get(entity => entity.IDCategory == idCategory).Skip(numPage* howGive).Take(howGive);
            return EntitiesToDtoes(entities,howGive, count);
        }

        public IDataListShortTask GetByUserId(int id, int numPage, int howGive)
        {
            var entities = new List<ShortTaskDto>();
                using (var context = new ToDoListContext())
                {
                    System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@userId", id);
                    entities = context.Database.SqlQuery<ShortTaskDto>("GetTaskByUserId @userId", param).ToListAsync().Result;

                }
            return new ListShortTaskDto
            {
                TotalPages = GetTotalPages(entities.Count, howGive),
                Tasks = entities.Select(entity => entity)
            };
        }

        public IDataTask GetById(int id)
        {
            var entity = base.FindById(id);
            return EntityToDto(entity);
        }

        public IEnumerable<IDataShortTask> GetByName(int categoryId, string name)
        {
            var listEntities = base.Get(entity => entity.Name.Contains(name) && entity.IDCategory == categoryId);
            return EntitiesToShortDtoes(listEntities);
        }

        bool ITaskRepository.Delete(int id)
        {
            SharedTaskRepository shared = new SharedTaskRepository();
            var sharedDto = shared.GetUsers(id);
            var result = shared.Delete(sharedDto);
            return result && base.Delete(id);
        }

        public bool Update(IDataTask dto)
        {
            return base.Update(DtoToEntity(dto));
        }

        private int GetTotalPages(int count, int howGive)
        {
            if (count % howGive > 0)
            {
                return count / howGive + 1;
            }
            return count / howGive;
        }

        #region DTO conversion

        /// <summary>
        /// convert dto in entity
        /// </summary>
        /// <param name="model">dto task</param>
        /// <returns>entity task</returns>
        private Task DtoToEntity(IDataTask model)
        {
            return new Task
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                IDStatus = model.Status.IDStatus,
                DateCreation = model.DateCreation,
                DateFinish = model.DateFinish

            };
        }

        /// <summary>
        /// convert entity in dto
        /// </summary>
        /// <param name="model">entity task</param>
        /// <returns>dto task</returns>
        private IDataTask EntityToDto(Task model)
        {
            return new TaskDto
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                Status = StatusRepository.EntityToDto(model.Status),
                DateCreation = model.DateCreation,
                DateFinish = model.DateFinish
            };
        }
        
        /// <summary>
        /// convert entity in short form dto
        /// </summary>
        /// <param name="model">entity task</param>
        /// <returns>dto short task</returns>
        private IDataShortTask EntityToShortDto(Task model)
        {
            return new ShortTaskDto
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Priority = model.Priority,
                DateFinish = model.DateFinish
            };
        }

        /// <summary>
        /// convert list entity by list dto
        /// </summary>
        /// <param name="entities">list entity task</param>
        /// <returns>list dto task</returns>
        private IEnumerable<IDataTask> EntitiesToDtoes(IEnumerable<Task> entities)
        {
            return entities.Select(entity => EntityToDto(entity));

        }

        /// <summary>
        /// convert list entity by list dto
        /// </summary>
        /// <param name="entities">list entity task</param>
        /// <returns>list dto task</returns>
        private IEnumerable<IDataShortTask> EntitiesToShortDtoes(IEnumerable<Task> entities)
        {
            return entities.Select(entity => EntityToShortDto(entity));

        }

        /// <summary>
        /// convert list entity by short form list dto
        /// </summary>
        /// <param name="entities">list entity task</param>
        /// <param name="howGive">count element what need</param>
        /// <returns>dto list short task</returns>
        private IDataListShortTask EntitiesToDtoes(IEnumerable<Task> entities, int howGive, int count)
        {
            return new ListShortTaskDto
            {
                TotalPages = GetTotalPages(count, howGive),
                Tasks = entities.Select(entity => EntityToShortDto(entity))
            };

        }

       

        #endregion
    }
}
