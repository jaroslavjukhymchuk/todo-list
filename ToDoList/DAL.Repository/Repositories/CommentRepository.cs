﻿using DAL.Repository.Entities;
using System.Collections.Generic;
using System.Linq;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;
using DAL.Repository.DTOs;

namespace DAL.Repository.Repositories
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository() : base(new ToDoListContext())
        {

        }

        public bool Create(IDataComment dto)
        {
            return base.Create(DtoToEntity(dto));
        }

        bool ICommentRepository.Delete(int id)
        {
            return base.Delete(id);
        }

        public IDataComment GetById(int id)
        {
            var entity = base.FindById(id);
            return EntityToDto(entity);
        }

        public IEnumerable<IDataComment> GetByTask(int taskId)
        {
            var listEntities = base.Get(entity => entity.IDTask == taskId);
            return EntitiesToDtoes(listEntities);
        }

        public bool Update(IDataComment dto)
        {
            return base.Update(DtoToEntity(dto));
        }

        #region DTO conversion

        /// <summary>
        /// convert dto in entity
        /// </summary>
        /// <param name="model">dto comment</param>
        /// <returns>entity comment</returns>
        private Comment DtoToEntity(IDataComment model)
        {
            return new Comment
            {
                IDComment = model.IDComment,
                IDTask = model.IDTask,
                Description = model.Description,
                DateCreation = model.DateCreation,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert entity in dto
        /// </summary>
        /// <param name="model">entity comment</param>
        /// <returns>dto comment</returns>
        private IDataComment EntityToDto(Comment model)
        {
            return new CommentDto
            {
                IDComment = model.IDComment,
                IDTask = model.IDTask,
                Description = model.Description,
                DateCreation = model.DateCreation,
                IDUser = model.IDUser
            };
        }

        /// <summary>
        /// convert list entity by list dto
        /// </summary>
        /// <param name="entity">list entity comment</param>
        /// <returns>list dto comment</returns>
        private IEnumerable<IDataComment> EntitiesToDtoes(IEnumerable<Comment> entity)
        {
            return entity.Select(comment => EntityToDto(comment));

        }
        
        #endregion
    }
}
