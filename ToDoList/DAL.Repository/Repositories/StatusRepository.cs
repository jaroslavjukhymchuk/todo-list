﻿using DAL.Repository.Entities;
using System.Collections.Generic;
using System.Linq;
using DAL.Interface.IDTOs;
using DAL.Interface.IRepositories;
using DAL.Repository.DTOs;

namespace DAL.Repository.Repositories
{
    public class StatusRepository : BaseRepository<Status>, IStatusRepository
    {
        public StatusRepository() : base(new ToDoListContext())
        {

        }

        public bool Create(IDataStatus dto)
        {
            return base.Create(DtoToEntity(dto));
        }

        bool IStatusRepository.Delete(int id)
        {
            return base.Delete(id);
        }

        public new IEnumerable<IDataStatus> GetAll()
        {
            return EntitiesToDtoes(base.GetAll());
        }

        public IDataStatus GetById(int id)
        {
            return EntityToDto(base.FindById(id));
        }

        public bool Update(IDataStatus dto)
        {
            return base.Update(DtoToEntity(dto));
        }
        
        #region DTO conversion

        /// <summary>
        /// convert dto in entity
        /// </summary>
        /// <param name="model">dto status</param>
        /// <returns>entity status</returns>
        private Status DtoToEntity(IDataStatus model)
        {
            return new Status
            {
                IDStatus = model.IDStatus,
                Name = model.Name
            };
        }

        /// <summary>
        /// convert entity in dto
        /// </summary>
        /// <param name="model">entity status</param>
        /// <returns>dto status</returns>
        internal static IDataStatus EntityToDto(Status model)
        {
            return new StatusDto
            {
                IDStatus = model.IDStatus,
                Name = model.Name
            };
        }

        /// <summary>
        /// convert list entity by list dto
        /// </summary>
        /// <param name="entity">list entity status</param>
        /// <returns>list dto status</returns>
        private IEnumerable<IDataStatus> EntitiesToDtoes(IEnumerable<Status> entity)
        {
            return entity.Select(category => EntityToDto(category));

        }

        #endregion
    }
}
