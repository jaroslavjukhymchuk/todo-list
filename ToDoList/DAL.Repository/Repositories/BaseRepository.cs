﻿using DAL.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repository.Repositories
{
    public class BaseRepository<TEntity> where TEntity : class
    {
        DbContext _context;
        DbSet<TEntity> _dbSet;

        public BaseRepository(DbContext context)
        {
                _dbSet = context.Set<TEntity>();
                _context = context;
        }

        /// <summary>
        /// get all elemenets by entity
        /// </summary>
        /// <returns>list elements</returns>
        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        /// <summary>
        /// get elements by some condition
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <returns>list elements</returns>
        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }

        /// <summary>
        /// find element by id
        /// </summary>
        /// <param name="id">id element</param>
        /// <returns>element entity</returns>
        public TEntity FindById(int id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// create new element
        /// </summary>
        /// <param name="item">element entity</param>
        /// <returns>true if the created object else false</returns>
        public bool Create(TEntity item)
        {
            
                _dbSet.Add(item);
           return _context.SaveChanges() == 1;
        }

        public bool Create(IEnumerable<TEntity> item)
        {
            
                _dbSet.AddRange(item);
            
            return _context.SaveChanges() > 0;
        }

        /// <summary>
        /// update entity
        /// </summary>
        /// <param name="item">update dto </param>
        /// <returns>true if the update object else false</returns>
        public bool Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            return _context.SaveChanges() == 1;
        }

        /// <summary>
        /// delete element
        /// </summary>
        /// <param name="id">id element what need delete</param>
        /// <returns>true if the deleted object else false</returns>
        public bool Delete(int id)
        {
            var entity = _dbSet.Find(id);
            TEntity deletedEntity = null;
            if(entity != null)
            {
                deletedEntity = _dbSet.Remove(entity);
                _context.SaveChanges();
            }
            return deletedEntity != null;
        }

        public bool Delete(IEnumerable<SharedTask> entities)
        {
            List<TEntity> entity = new List<TEntity>();
            foreach(var index in entities) {
            entity.Add(_dbSet.Find(index.IDSharedTask));
            }
              var  deletedEntity = _dbSet.RemoveRange(entity);
                _context.SaveChanges();
            return deletedEntity != null;
        }
    }
}
