namespace DAL.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        IDCategory = c.Int(nullable: false, identity: true),
                        IDUser = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.IDCategory)
                .Index(t => t.IDUser);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        IDTask = c.Int(nullable: false, identity: true),
                        IDCategory = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 20),
                        Description = c.String(maxLength: 50),
                        Priority = c.Int(nullable: false),
                        IDStatus = c.Int(nullable: false),
                        DateCreation = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        DateFinish = c.DateTime(storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.IDTask)
                .ForeignKey("dbo.Category", t => t.IDCategory, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.IDStatus, cascadeDelete: true)
                .Index(t => t.IDCategory)
                .Index(t => t.IDStatus);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        IDStatus = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.IDStatus)
                .Index(t => t.Name, unique: true);
            
            
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        IDComment = c.Int(nullable: false, identity: true),
                        IDTask = c.Int(nullable: false),
                        IDUser = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 50),
                        DateCreation = c.DateTime(nullable: false, storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.IDComment)
                .ForeignKey("dbo.Task", t => t.IDTask, cascadeDelete: true)
                .Index(t => t.IDTask)
                .Index(t => t.IDUser);
            
            CreateTable(
                "dbo.SharedTask",
                c => new
                    {
                        IDSharedTask = c.Int(nullable: false, identity: true),
                        IDTask = c.Int(nullable: false),
                        IDUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDSharedTask)
                .ForeignKey("dbo.Task", t => t.IDTask, cascadeDelete: true)
                .Index(t => t.IDTask)
                .Index(t => t.IDUser);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SharedTask", "IDUser", "dbo.Users");
            DropForeignKey("dbo.SharedTask", "IDTask", "dbo.Task");
            DropForeignKey("dbo.Comment", "IDUser", "dbo.Users");
            DropForeignKey("dbo.Comment", "IDTask", "dbo.Task");
            DropForeignKey("dbo.Category", "IDUser", "dbo.Users");
            DropForeignKey("dbo.UserRoleInt32Pk", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLoginInt32Pk", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaimInt32Pk", "UserId", "dbo.Users");
            DropForeignKey("dbo.Task", "IDStatus", "dbo.Status");
            DropForeignKey("dbo.Task", "IDCategory", "dbo.Category");
            DropIndex("dbo.SharedTask", new[] { "IDUser" });
            DropIndex("dbo.SharedTask", new[] { "IDTask" });
            DropIndex("dbo.Comment", new[] { "IDUser" });
            DropIndex("dbo.Comment", new[] { "IDTask" });
            DropIndex("dbo.UserRoleInt32Pk", new[] { "UserId" });
            DropIndex("dbo.UserLoginInt32Pk", new[] { "UserId" });
            DropIndex("dbo.UserClaimInt32Pk", new[] { "UserId" });
            DropIndex("dbo.Status", new[] { "Name" });
            DropIndex("dbo.Task", new[] { "IDStatus" });
            DropIndex("dbo.Task", new[] { "IDCategory" });
            DropIndex("dbo.Category", new[] { "IDUser" });
            DropTable("dbo.SharedTask");
            DropTable("dbo.Comment");
            DropTable("dbo.UserRoleInt32Pk");
            DropTable("dbo.UserLoginInt32Pk");
            DropTable("dbo.UserClaimInt32Pk");
            DropTable("dbo.Users");
            DropTable("dbo.Status");
            DropTable("dbo.Task");
            DropTable("dbo.Category");
        }
    }
}
