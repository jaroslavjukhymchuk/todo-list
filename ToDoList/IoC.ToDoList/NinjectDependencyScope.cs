﻿using Ninject;
using Ninject.Syntax;
using System;
using System.Web.Http.Dependencies;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;

namespace IoC.ToDoList
{
    /// <summary>
    /// confige ninject
    /// </summary>
    public class NinjectDependencyScope : IDependencyScope
    {
        private IResolutionRoot resolver;

        internal NinjectDependencyScope(IResolutionRoot resolver)
        {
            Contract.Assert(resolver != null);

            this.resolver = resolver;
        }

        /// <summary>
        /// dispose Resolution Root
        /// </summary>
        public void Dispose()
        {
            var disposable = this.resolver as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }

            this.resolver = null;
        }

        /// <summary>
        /// get service
        /// </summary>
        /// <param name="serviceType">type service</param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            if (this.resolver == null)
            {
                throw new ObjectDisposedException("this", "This scope has already been disposed");
            }
            return this.resolver.TryGet(serviceType);
        }

        /// <summary>
        /// get services
        /// </summary>
        /// <param name="serviceType">type service</param>
        /// <returns></returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (this.resolver == null)
            {
                throw new ObjectDisposedException("this", "This scope has already been disposed");
            }

            return this.resolver.GetAll(serviceType).ToList();
        }
    }
    }