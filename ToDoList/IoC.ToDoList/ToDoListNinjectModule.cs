﻿using BL.Interface.IServices;
using BL.Service.Services;
using DAL.Interface.IRepositories;
using DAL.Repository.Repositories;
using Ninject.Modules;

namespace IoC.ToDoList
{
    public class ToDoListNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(ICategoryRepository)).To(typeof(CategoryRepository));
            Bind(typeof(ITaskRepository)).To(typeof(TaskRepository));
            Bind(typeof(ICommentRepository)).To(typeof(CommentRepository));
            Bind(typeof(IStatusRepository)).To(typeof(StatusRepository));
            Bind(typeof(ISharedTaskRepository)).To(typeof(SharedTaskRepository));
            Bind(typeof(ICategoryService)).To(typeof(CategoryService));
            Bind(typeof(ITaskService)).To(typeof(TaskService));
            Bind(typeof(ICommentService)).To(typeof(CommentService));
            Bind(typeof(IStatusService)).To(typeof(StatusService));
            Bind(typeof(ISharedTaskService)).To(typeof(SharedTaskService));
        }
    }
}