﻿using System.Web.Http.Dependencies;
using Ninject;

namespace IoC.ToDoList
{
    /// <summary>
    /// configuration ninject
    /// </summary>
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel kernel;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="kernel">kernel</param>
        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this.kernel = kernel;
        }

        /// <summary>
        /// Starts a resolution scope.
        /// </summary>
        /// <returns></returns>
        public IDependencyScope BeginScope()
        {

            return new NinjectDependencyScope(kernel.BeginBlock());
        }
    }
}