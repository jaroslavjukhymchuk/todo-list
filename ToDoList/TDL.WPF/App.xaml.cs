﻿using System;
using System.Windows;
using System.Windows.Threading;
using TDL.WPF.Infrastructure;
namespace TDL.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var bs = new Bootstrapper();
            bs.Run();
        }

        private void ApplicationDispatcherUnhadledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(
                "Unexpected error occured. Please inform the admin." + Environment.NewLine + e.Exception.Message,
                "Unexpected error");
            e.Handled = true;
        }
    }
}
