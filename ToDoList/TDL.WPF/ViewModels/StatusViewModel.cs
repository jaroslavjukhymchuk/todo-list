﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using TDL.WPF.Infrastructure;
using Prism.Commands;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class StatusViewModel : NotifyDataErrorInfoBase
    {
        
        private readonly IStatusService _statusService;

        private readonly string _token;

        public StatusViewModel(IStatusService statusService)
        {
            _token = Application.Current.Resources["Token"].ToString();
            _statusService = statusService;
            _selectedStatus = new StatusModel();
            StatusCollection = new ObservableCollection<StatusModel>(SwapToModels(_statusService.Get(_token)));
            AddCommand = new DelegateCommand(AddStatus, CanAdd).ObservesProperty(() => Name);
            UpdateCommand = new DelegateCommand(UpdateStatus, CanExecute).ObservesProperty(() => SelectedStatus).ObservesProperty(() => SelectedName);
            DeleteCommand = new DelegateCommand(DeleteStatus, CanExecute).ObservesProperty(() => SelectedStatus);
            ShowStatusesCommand = new DelegateCommand(ShowStatuses);
            ShowAddCommand = new DelegateCommand(ShowAdd);
            AddVisibililty = Visibility.Hidden.ToString();
            EditVisibililty = Visibility.Hidden.ToString();
            ShowEditVisibililty = Visibility.Hidden.ToString();
            StatusesVisibililty = Visibility.Visible.ToString();
        }

        #region Properties

        private ObservableCollection<StatusModel> _statusCollection;

        public ObservableCollection<StatusModel> StatusCollection
        {
            get { return _statusCollection; }
            set { SetProperty(ref _statusCollection, value); }
        }

        private StatusModel _selectedStatus;

        public StatusModel SelectedStatus
        {

            get { return _selectedStatus; }
            set
            {
                SetProperty(ref _selectedStatus, value);
                AddVisibililty = Visibility.Hidden.ToString();
                StatusesVisibililty = Visibility.Hidden.ToString();
                ShowEditVisibililty = Visibility.Visible.ToString();
                EditVisibililty = Visibility.Visible.ToString();
            }
        }

        private string _selectedName;

        public string SelectedName
        {
            get { return SelectedStatus.Name; }
            set
            {
                SelectedStatus.Name = value;
                SetProperty(ref _selectedName, value);
                ValidateCustomErrors(nameof(SelectedName));
            }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                ValidateDataAnnotationsErrors(nameof(Name), value, _selectedStatus);
            }
        }

        private string _editVisibililty;

        public string EditVisibililty
        {
            get { return _editVisibililty; }
            set { SetProperty(ref _editVisibililty, value); }
        }

        private string _addVisibililty;

        public string AddVisibililty
        {
            get { return _addVisibililty; }
            set { SetProperty(ref _addVisibililty, value); }
        }

        private string _showEditVisibililty;

        public string ShowEditVisibililty
        {
            get { return _showEditVisibililty; }
            set { SetProperty(ref _showEditVisibililty, value); }
        }

        private string _statusesVisibililty;

        public string StatusesVisibililty
        {
            get { return _statusesVisibililty; }
            set { SetProperty(ref _statusesVisibililty, value); }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            ClearErrors(propertyName);

            switch (propertyName)
            {
                case nameof(SelectedName):
                {
                    if (string.IsNullOrEmpty(SelectedName))
                    {
                        AddError(propertyName, "Name can not be empty");
                        UpdateCommand.CanExecute(false);
                    }
                    break;
                }
            }
        }

        #endregion

        #region Commands
        
        public ICommand UpdateCommand { get; set; }
        public ICommand ShowAddCommand { get; set; }
        public ICommand ShowStatusesCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        

        private void ShowAdd()
        {
            AddVisibililty = Visibility.Visible.ToString();
            EditVisibililty = Visibility.Hidden.ToString();
            ShowEditVisibililty = Visibility.Visible.ToString();
            StatusesVisibililty = Visibility.Hidden.ToString();
        }

        private void ShowStatuses()
        {
            AddVisibililty = Visibility.Hidden.ToString();
            EditVisibililty = Visibility.Hidden.ToString();
            ShowEditVisibililty = Visibility.Hidden.ToString();
            StatusesVisibililty = Visibility.Visible.ToString();
        }

        private void UpdateStatus()
        {
           _statusService.Update(SelectedStatus, _token);
        }

        private void AddStatus()
        {
           _statusService.Add(new StatusModel { Name = Name }, _token);
           StatusCollection = new ObservableCollection<StatusModel>(SwapToModels(_statusService.Get(_token)));

        }

        private void DeleteStatus()
        {
            _statusService.Delete(SelectedStatus.IDStatus, _token);
            StatusCollection = new ObservableCollection<StatusModel>(SwapToModels(_statusService.Get(_token)));
        }

        private bool CanAdd()
        {
            return !string.IsNullOrEmpty(Name);
        }

        private bool CanExecute()
        {
            if (SelectedStatus != null)
                return !string.IsNullOrEmpty(SelectedName);
            return false;
        }

        #endregion

        #region Swap

        private static StatusModel SwapToModel(IStatusModel dto)
        {
            return new StatusModel
            {
                IDStatus = dto.IDStatus,
                Name = dto.Name,
            };
        }

        private static IEnumerable<StatusModel> SwapToModels(IEnumerable<IStatusModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        #endregion
    }
}
