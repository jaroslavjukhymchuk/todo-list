﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Prism.Events;
using TDL.WPF.Events;
using System.Windows.Input;
using Prism.Commands;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Infrastructure;
using TDL.WebApi.IService.IModels;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class TaskViewModel : NotifyDataErrorInfoBase
    {
        private readonly IEventAggregator _eventAggregator;

        private readonly ITaskService _taskService;

        private readonly TaskModel _taskModel;

        private readonly string _token;

        private PaginationSeting _paginationSetting;

        public TaskViewModel(IEventAggregator eventAggregator, ITaskService taskService, IStatusService statusService)
        {
            _paginationSetting = new PaginationSeting();
            _token = Application.Current.Resources["Token"].ToString();
            _eventAggregator = eventAggregator;
            eventAggregator.GetEvent<DetailsEvent>().Subscribe(Details);
            _taskService = taskService;
            _shortTaskModel = new ShortTaskModel();
            _taskModel = new TaskModel();
            StatusCollection = new ObservableCollection<StatusModel>(SwapToStatusModels(statusService.Get(_token)));
            ShowSearchCommand = new DelegateCommand(ShowSearch);
            ShowAddCommand = new DelegateCommand(ShowAdd);
            AddCommand = new DelegateCommand(AddTask, CanAdd).ObservesProperty(() => Name).ObservesProperty(() => Status);
            DeleteCommand = new DelegateCommand(DeleteTask, CanExecute).ObservesProperty(() => SelectedShortTask);
            SearchCommand = new DelegateCommand(SearchTask, CanSearch).ObservesProperty(() => SearchName);
            DetailsCommand = new DelegateCommand(DetailsTask, CanExecute).ObservesProperty(() => SelectedShortTask);
            NextPageCommand = new DelegateCommand(NextPage, CanGetNextPage).ObservesProperty(() => ListShortTaskModel);
            PreviousPageCommand = new DelegateCommand(PreviousPage, CanGetPreviousPage).ObservesProperty(() => ListShortTaskModel);
            SearchVisibility = Visibility.Visible.ToString();
            AddVisibility = Visibility.Collapsed.ToString();
            SearchResultVisibility = Visibility.Hidden.ToString();
            AllTaskVisibility = Visibility.Visible.ToString();
            AllVisibility = Visibility.Hidden.ToString();

        }

        #region Properties

        private ObservableCollection<StatusModel> _statusCollection;

        public ObservableCollection<StatusModel> StatusCollection
        {
            get { return _statusCollection; }
            set { SetProperty(ref _statusCollection, value); }
        }

        private ObservableCollection<ShortTaskModel> _shortTaskCollection;

        public ObservableCollection<ShortTaskModel> ShortTaskCollection
        {
            get
            {
                if (_shortTaskCollection != null)
                foreach (var task in _shortTaskCollection)
                {
                    task.DateFinishString = task.DateFinish?.Date.ToShortDateString();
                }
                return _shortTaskCollection;
            }
            set { SetProperty(ref _shortTaskCollection, value); }
        }

        private ObservableCollection<ShortTaskModel> _searchedCollection;

        public ObservableCollection<ShortTaskModel> SearchedCollection
        {
            get { return _searchedCollection; }
            set { SetProperty(ref _searchedCollection, value); }
        }

        private ShortTaskModel _shortTaskModel;

        public ShortTaskModel SelectedShortTask
        {
            get {  return _shortTaskModel; }
            set { SetProperty(ref _shortTaskModel, value); }
        }

        private int _idCategory;

        public int IDCategory
        {
            get { return _idCategory; }
            set { SetProperty(ref _idCategory, value); }
        }

        private StatusModel _statusModel;

        public StatusModel Status
        {
            get { return _statusModel; }
            set { SetProperty(ref _statusModel, value); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                ValidateDataAnnotationsErrors(nameof(Name), value, _shortTaskModel); 
            }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set
            {
                SetProperty(ref _description, value);
                ValidateDataAnnotationsErrors(nameof(Description), value, _taskModel);
            }

        }

        private int _priotity = 1;

        public int Priority
        {
            get { return _priotity; }
            set { SetProperty(ref _priotity, value); }
        }

        private DateTime? _dateFinish;

        public DateTime? DateFinish
        {
            get { return _dateFinish; }
            set
            {
                SetProperty(ref _dateFinish, value);
                ValidateCustomErrors(nameof(DateFinish));
            }
        }

        private string _searchName;

        public string SearchName
        {
            get { return _searchName; }
            set
            {
                SetProperty(ref _searchName, value);
                ValidateCustomErrors(nameof(SearchName));
            }
        }

        private string _searchVisibility;

        public string SearchVisibility
        {
            get { return _searchVisibility; }
            set
            {
                SetProperty(ref _searchVisibility, value);
            }
        }

        private string _addVisibility;

        public string AddVisibility
        {
            get { return _addVisibility; }
            set
            {
                SetProperty(ref _addVisibility, value);
            }
        }

        private string _navigateVisibility;

        public string NavigateVisibility
        {
            get { return _navigateVisibility; }
            set
            {
                SetProperty(ref _navigateVisibility, value);
            }
        }

        private string _searchResultVisibility;

        public string SearchResultVisibility
        {
            get { return _searchResultVisibility; }
            set
            {
                SetProperty(ref _searchResultVisibility, value);
            }
        }

        private string _allTaskVisibility;

        public string AllTaskVisibility
        {
            get { return _allTaskVisibility; }
            set
            {
                SetProperty(ref _allTaskVisibility, value);
            }
        }

        private string _allVisibility;

        public string AllVisibility
        {
            get { return _allVisibility; }
            set
            {
                SetProperty(ref _allVisibility, value);
            }
        }

        private ListShortTaskModel _listShortTaskModel;

        public ListShortTaskModel ListShortTaskModel
        {
            get { return _listShortTaskModel; }
            set { SetProperty(ref _listShortTaskModel, value); }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            ClearErrors(propertyName);

            switch (propertyName)
            {
                case nameof(SearchName):
                {
                    if (string.IsNullOrEmpty(SearchName))
                    {
                        AddError(propertyName, "Search can not be empty");
                    }
                    break;
                }
                case nameof(DateFinish):
                {
                    if (DateFinish != null)
                    {
                        if (DateFinish.Value < DateTime.Today)
                        {
                            AddError(propertyName, "Date finish can not be less today date");
                        }
                    }
                    break;
                }
            }
        }

        #endregion

        #region Commands
        
        public ICommand ShowSearchCommand { get; set; }
        public ICommand ShowAddCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand DetailsCommand { get; set; }
        public ICommand NextPageCommand { get; set; }
        public ICommand PreviousPageCommand { get; set; }
        

        private void ShowSearch()
        {
            SearchVisibility = Visibility.Visible.ToString();
            AddVisibility = Visibility.Collapsed.ToString();
            ShowAllTask();
        }

        private void ShowAdd()
        {
            AddVisibility = Visibility.Visible.ToString();
            SearchVisibility = Visibility.Hidden.ToString();
        }

        private void ShowAllTask()
        {
            AllTaskVisibility = Visibility.Visible.ToString();
            SearchResultVisibility = Visibility.Collapsed.ToString();
            NavigateVisibility = Visibility.Visible.ToString();
        }

        private void Details(int? idCategory)
        {
            if (idCategory == null)
            {
                AllVisibility = Visibility.Hidden.ToString();
            }
            else
            {
                AllVisibility = Visibility.Visible.ToString();
                ShowAllTask();
                IDCategory = (int)idCategory;
                GetTasks();
            }
            
        }

        private void GetTasks(int? pageNumber = 1)
        {
            ListShortTaskModel = SwapToModel(_taskService.Get(IDCategory, _token, _paginationSetting.GetPaginationSetting(), pageNumber));
            if (ListShortTaskModel.PagingParameter.NextPage.ToLower() == "no" &&
                ListShortTaskModel.PagingParameter.PreviousPage.ToLower() == "no")
            {
                NavigateVisibility = Visibility.Hidden.ToString();
            }
            else
            {
                NavigateVisibility = Visibility.Visible.ToString();
            }
            ShortTaskCollection = new ObservableCollection<ShortTaskModel>(SwapToShortModels(ListShortTaskModel.Tasks));
        }

        private void NextPage()
        {
            GetTasks(ListShortTaskModel.PagingParameter.CurrentPage + 1);
        }

        private void PreviousPage()
        {
            GetTasks(ListShortTaskModel.PagingParameter.CurrentPage - 1);
        }

        private void AddTask()
        {
            _taskService.Add(new TaskModel
                {
                    IDCategory = IDCategory,
                    DateCreation = DateTime.Today,
                    Name = Name,
                    Description = Description,
                    Priority = Priority,
                    DateFinish = DateFinish,
                    Status = Status

                }, _token);
            GetTasks();
        }

        private void DeleteTask()
        {
            _taskService.Delete(SelectedShortTask.IDTask, _token);
            GetTasks();
        }

        private void SearchTask()
        {
            SearchResultVisibility = Visibility.Visible.ToString();
            AllTaskVisibility = Visibility.Hidden.ToString();
            NavigateVisibility = Visibility.Hidden.ToString();
            SearchedCollection = new ObservableCollection<ShortTaskModel>(SwapToShortModels(_taskService.Search(IDCategory, SearchName, _token)));
        }

        private void DetailsTask()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("CategoryView");
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("FullTaskView");
            _eventAggregator.GetEvent<DetailsTaskEvent>().Publish(SelectedShortTask.IDTask);
        }

        private bool CanAdd()
        {
            return !String.IsNullOrWhiteSpace(Name) && GetErrors("DateFinish") == null && GetErrors("Description") == null;
        }
        
        private bool CanSearch()
        {
            return !String.IsNullOrWhiteSpace(SearchName);
        }

        private bool CanExecute()
        {
            if (SelectedShortTask != null)
                return SelectedShortTask.Name != null;
            return false;
        }

        private bool CanGetNextPage()
        {
            if (ListShortTaskModel != null)
               return ListShortTaskModel.PagingParameter.NextPage.ToLower() == "yes";
            return false;
        }
   
        private bool CanGetPreviousPage()
        {
            if (ListShortTaskModel != null)
               return ListShortTaskModel.PagingParameter.PreviousPage.ToLower() == "yes";
           return false;
        }

        #endregion

        #region Swap

        private static ListShortTaskModel SwapToModel(IListShortTaskModel dto)
        {
            return new ListShortTaskModel
            {
                Tasks = dto.Tasks,
                PagingParameter = dto.PagingParameter
            };
        }

        private static ShortTaskModel SwapToShortModel(IShortTaskModel dto)
        {
            return new ShortTaskModel
            {
                IDTask = dto.IDTask,
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                Priority = dto.Priority,
                DateFinish = dto.DateFinish,
                DateFinishString = dto.DateFinishString,
                UserName = dto.UserName
            };
        }

        private static IEnumerable<ShortTaskModel> SwapToShortModels(IEnumerable<IShortTaskModel> dtoes)
        {
            return dtoes.Select(SwapToShortModel).ToList();

        }

        private static StatusModel SwapToStatusModel(IStatusModel dto)
        {
            return new StatusModel
            {
                IDStatus = dto.IDStatus,
                Name = dto.Name,
            };
        }

        private static IEnumerable<StatusModel> SwapToStatusModels(IEnumerable<IStatusModel> dtoes)
        {
            return dtoes.Select(SwapToStatusModel).ToList();

        }
        #endregion
    }
}
