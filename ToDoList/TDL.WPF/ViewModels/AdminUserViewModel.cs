﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Prism.Commands;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Infrastructure;
using System.Linq;
using System.Windows;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class AdminUserViewModel : NotifyDataErrorInfoBase
    {
        
        private readonly IUserService _userService;
        private readonly string _token;
        
        public AdminUserViewModel(IUserService userService)
        {
            _token = Application.Current.Resources["Token"].ToString();
            _userService = userService;
            _selectedUsers = new UserModel();
            UsersCollection = new ObservableCollection<UserModel>(SwapToModels(_userService.GetAll(_token)));
            UpdateCommand = new DelegateCommand(UpdateUser, CanUpdate).ObservesProperty(() => SelectedUser).ObservesProperty(() => UserName).ObservesProperty(() => PhoneNumber).ObservesProperty(() => Email);
            DeleteCommand = new DelegateCommand(DeleteUser, CanDelete).ObservesProperty(() => SelectedUser);
            ShowUsersCommand = new DelegateCommand(ShowUsers);
            UsersVisibility = Visibility.Visible.ToString();
            EditVisibility = Visibility.Hidden.ToString();
        }

        #region Properties

        private ObservableCollection<UserModel> _usersCollection;

        public ObservableCollection<UserModel> UsersCollection
        {
            get { return _usersCollection; }
            set { SetProperty(ref _usersCollection, value); }
        }

        private UserModel _selectedUsers;

        public UserModel SelectedUser
        {

            get { return _selectedUsers; }
            set
            {
                UsersVisibility = Visibility.Hidden.ToString();
                EditVisibility = Visibility.Visible.ToString();
                SetProperty(ref _selectedUsers, value);
            }
        }

        private string _userName;

        public string UserName
        {
            get { return SelectedUser.UserName; }
            set
            {
                SelectedUser.UserName = value;
                SetProperty(ref _userName, value);
                ValidateDataAnnotationsErrors(nameof(UserName), value, _selectedUsers);
            }
        }

        private string _email;

        public string Email
        {
            get { return SelectedUser.Email; }
            set
            {
                SelectedUser.Email = value;
                SetProperty(ref _email, value);
                ValidateDataAnnotationsErrors(nameof(Email), value, _selectedUsers);
            }
        }

        private string _phoneNumber;

        public string PhoneNumber
        {
            get { return SelectedUser.PhoneNumber; }
            set
            {
                SelectedUser.PhoneNumber = value;
                SetProperty(ref _phoneNumber, value);
                ValidateDataAnnotationsErrors(nameof(PhoneNumber), value, _selectedUsers);
            }
        }

        private string _editVisibility;

        public string EditVisibility
        {
            get { return _editVisibility; }
            set
            {
                SetProperty(ref _editVisibility, value);
            }
        }

        private string _usersVisibility;

        public string UsersVisibility
        {
            get { return _usersVisibility; }
            set
            {
                SetProperty(ref _usersVisibility, value);
            }
        }

        #endregion

        #region Commands

        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ShowUsersCommand { get; set; }

        private void UpdateUser()
        {
            _userService.Update(SelectedUser, _token, SelectedUser.Id);
            UsersCollection = new ObservableCollection<UserModel>(SwapToModels(_userService.GetAll(_token)));
        }

        private void ShowUsers()
        {
            UsersVisibility = Visibility.Visible.ToString();
            EditVisibility = Visibility.Hidden.ToString();
        }

        private bool CanUpdate()
        {
            if (SelectedUser != null)
            {
                UserName = SelectedUser.UserName;
                Email = SelectedUser.Email;
                PhoneNumber = SelectedUser.PhoneNumber;
                return !HasErrors && !string.IsNullOrEmpty(SelectedUser.UserName);
            }
            
            return false;
        }

        private bool CanDelete()
        {
            return SelectedUser?.UserName != null;
        }

        private void DeleteUser()
        {
            _userService.Delete(_token, SelectedUser.Id);
            UsersCollection = new ObservableCollection<UserModel>(SwapToModels(_userService.GetAll(_token)));
        }

        #endregion

        #region Swap

        private static UserModel SwapToModel(IUserModel dto)
        {
            return new UserModel
            {
                Email = dto.Email,
                PhoneNumber = dto.PhoneNumber,
                UserName = dto.UserName,
                Id = dto.Id,
                IsAdmin = dto.IsAdmin
            };
        }

        private static IEnumerable<UserModel> SwapToModels(IEnumerable<IUserModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        #endregion
    }
}
