﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Events;
using TDL.WPF.Infrastructure;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class CategoryViewModel : NotifyDataErrorInfoBase
    {
        private readonly ICategoryService _categoryService;

        private readonly IEventAggregator _eventAggregator;

        private readonly string _token;

        public CategoryViewModel(IEventAggregator eventAggregator, ICategoryService categoryService)
        {
            _token = Application.Current.Resources["Token"].ToString();
            _eventAggregator = eventAggregator;
            _categoryService = categoryService;
            _categoryModel = new CategoryModel();
            CategoryCollection = new ObservableCollection<CategoryModel>(SwapToModels(_categoryService.Get(_token)));
            AddCommand = new DelegateCommand(AddCategory, CanAdd).ObservesProperty(() => Name);
            UpdateCommand = new DelegateCommand(UpdateCategory, CanUpdate).ObservesProperty(() => SelectedCategory).ObservesProperty(() => SelectedName);
            SearchCommand = new DelegateCommand(SearchCategory, CanSearch).ObservesProperty(() => SearchName);
            DeleteCommand = new DelegateCommand(DeleteCategory, CanExecute).ObservesProperty(() => SelectedCategory);
            ShowAddCommand = new DelegateCommand(ShowAdd);
            ShowSearchCommand = new DelegateCommand(ShowSearch);
            CancelCommand = new DelegateCommand(Cancel);
            NavigationCommand = new DelegateCommand<string>(Navigation);
            AddVisibililty = Visibility.Collapsed.ToString();
            EditVisibililty = Visibility.Collapsed.ToString();
            SearchVisibililty = Visibility.Collapsed.ToString();
            AllVisibililty = Visibility.Visible.ToString();
            ShowSearchVisibility = Visibility.Collapsed.ToString();
            CancelVisibility = Visibility.Collapsed.ToString();
            ViewName = "TaskView.xaml";
        }

        #region Properties

        private int _idCategory;

        public int IDCategory
        {
            get { return _idCategory; }
            set { SetProperty(ref _idCategory, value); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                ValidateDataAnnotationsErrors(nameof(Name), value, _categoryModel);
            }
        }

        private string _searchName;

        public string SearchName
        {
            get { return _searchName; }
            set
            {
                SetProperty(ref _searchName, value);
                ValidateCustomErrors(nameof(SearchName));
            }
        }

        private ObservableCollection<CategoryModel> _categoryCollection;

        public ObservableCollection<CategoryModel> CategoryCollection
        {
            get { return _categoryCollection; }
            set { SetProperty(ref _categoryCollection, value); }
        }

        private CategoryModel _categoryModel;

        public CategoryModel SelectedCategory
        {
            get
            {
                return _categoryModel;
            }
            set
            {
               
                AddVisibililty = Visibility.Collapsed.ToString();
                EditVisibililty = Visibility.Visible.ToString();
                ShowSearchVisibility = Visibility.Collapsed.ToString();
                SetProperty(ref _categoryModel, value);
                ValidateCustomErrors(nameof(SelectedCategory));
                _eventAggregator.GetEvent<DetailsEvent>().Publish(value?.IDCategory);
                if(value != null)
                    CancelVisibility = Visibility.Visible.ToString();
                Navigation("TaskView.xaml");
            }
        }

        private string _selectedName;

        public string SelectedName
        {
            get{return SelectedCategory?.Name; }
            set
            {
                SelectedCategory.Name = value;
                SetProperty(ref _selectedName, value);
                ValidateCustomErrors(nameof(SelectedName));
            }
        }

        private ObservableCollection<CategoryModel> _searchedCollection;

        public ObservableCollection<CategoryModel> SearchedCollection
        {
            get { return _searchedCollection; }
            set { SetProperty(ref _searchedCollection, value); }
        }

        private string _editVisibililty;

        public string EditVisibililty
        {
            get { return _editVisibililty; }
            set { SetProperty(ref _editVisibililty, value); }
        }

        private string _addVisibililty;

        public string AddVisibililty
        {
            get { return _addVisibililty; }
            set { SetProperty(ref _addVisibililty, value); }
        }

        private string _searchVisibililty;

        public string SearchVisibililty
        {
            get { return _searchVisibililty; }
            set { SetProperty(ref _searchVisibililty, value); }
        }

        private string _allVisibililty;

        public string AllVisibililty
        {
            get { return _allVisibililty; }
            set { SetProperty(ref _allVisibililty, value); }
        }

        private string _showSearchVisibililty;

        public string ShowSearchVisibility
        {
            get { return _showSearchVisibililty; }
            set { SetProperty(ref _showSearchVisibililty, value); }
        }

        private string _cancelVisibililty;

        public string CancelVisibility
        {
            get { return _cancelVisibililty; }
            set { SetProperty(ref _cancelVisibililty, value); }
        }

        private string _viewName;

        public string ViewName
        {
            get { return _viewName; }
            set { SetProperty(ref _viewName, value); }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            ClearErrors(propertyName);
            
            switch (propertyName)
            {
                case nameof(SearchName):
                {
                    if (string.IsNullOrEmpty(SearchName))
                    {
                        AddError(propertyName, "Search can not be empty");
                    }
                    break;
                }
                case nameof(SelectedName):
                {
                    if (string.IsNullOrEmpty(SelectedName))
                    {
                        AddError(propertyName, "Name can not be empty");
                        UpdateCommand.CanExecute(false);
                    }
                    break;
                }
            }
        }
        
        #endregion

        #region Commands

        public ICommand AddCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ShowAddCommand { get; set; }
        public ICommand ShowSearchCommand { get; set; }
        public ICommand NavigationCommand { get; set; }


        private void AddCategory()
        {
           _categoryService.Add(new CategoryModel{Name = Name}, _token);
           CategoryCollection = new ObservableCollection<CategoryModel>(SwapToModels(_categoryService.Get(_token)));
        }

        private void UpdateCategory()
        {
           _categoryService.Update(SelectedCategory, _token);
           CategoryCollection = new ObservableCollection<CategoryModel>(SwapToModels(_categoryService.Get(_token)));
        }

        private void SearchCategory()
        {
           SearchedCollection = new ObservableCollection<CategoryModel>(SwapToModels(_categoryService.Search(SearchName, _token)));
            SelectedCategory = null;
            SearchVisibililty = Visibility.Visible.ToString();
            AllVisibililty = Visibility.Collapsed.ToString();
        }

        private void DeleteCategory()
        {

            _categoryService.Delete(SelectedCategory.IDCategory, _token);
            CategoryCollection = new ObservableCollection<CategoryModel>(SwapToModels(_categoryService.Get(_token)));
            ShowSearch();
        }
        

        private void ShowAdd()
        {
            CancelVisibility = Visibility.Visible.ToString();
            SelectedCategory = null;
            AddVisibililty = Visibility.Visible.ToString();
            EditVisibililty = Visibility.Collapsed.ToString();
            ShowSearchVisibility = Visibility.Collapsed.ToString();
        }

        private void ShowSearch()
        {
            CancelVisibility = Visibility.Visible.ToString();
            SelectedCategory = null;
            AddVisibililty = Visibility.Collapsed.ToString();
            EditVisibililty = Visibility.Collapsed.ToString();
            ShowSearchVisibility = Visibility.Visible.ToString();
        }

        private void Navigation(string viewName)
        {
            ViewName = viewName;
        }

        private void Cancel()
        {
            Navigation("TaskView.xaml");
            CancelVisibility = Visibility.Collapsed.ToString();
            SelectedCategory = null;
            AddVisibililty = Visibility.Collapsed.ToString();
            EditVisibililty = Visibility.Collapsed.ToString();
            ShowSearchVisibility = Visibility.Collapsed.ToString();
            SearchVisibililty = Visibility.Collapsed.ToString();
            AllVisibililty = Visibility.Visible.ToString();
        }
        

        private bool CanAdd()
        {
            return !String.IsNullOrWhiteSpace(Name) && Name.Length < 50;
        }

        private bool CanUpdate()
        {
            return !String.IsNullOrWhiteSpace(SelectedName);
        }

        private bool CanSearch()
        {
            return !String.IsNullOrWhiteSpace(SearchName);
        }

        private bool CanExecute()
        {
            return SelectedCategory?.Name != null;
        }

        #endregion

        #region Swap

        private static CategoryModel SwapToModel(ICategoryModel dto)
        {
            return new CategoryModel
            {
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                IDUser = dto.IDUser
            };
        }

        private static IEnumerable<CategoryModel> SwapToModels(IEnumerable<ICategoryModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        #endregion

    }
}
