﻿using System.Collections.Generic;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Events;
using TDL.WPF.Infrastructure;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class SharedTaskViewModel : BindableBase
    {
        private readonly IEventAggregator _eventAggregator;
        
        private readonly ITaskService _taskService;

        private readonly string _token;

        private PaginationSeting _paginationSetting;

        public SharedTaskViewModel(IEventAggregator eventAggregator, ITaskService taskService)
        {
            _paginationSetting = new PaginationSeting();
            _token = Application.Current.Resources["Token"].ToString();
            _eventAggregator = eventAggregator;
            _taskService = taskService;
            GetTasks();
            DetailsCommand = new DelegateCommand(DetailsTask, CanDetailsExecute).ObservesProperty(() => SelectedSharedTask);
            NextPageCommand = new DelegateCommand(NextPage, CanGetNextPage).ObservesProperty(() => ListShortTaskModel);
            PreviousPageCommand = new DelegateCommand(PreviousPage, CanGetPreviousPage).ObservesProperty(() => ListShortTaskModel);
        }

        #region Properties

        private ObservableCollection<ShortTaskModel> _sharedTaskCollection;

        public ObservableCollection<ShortTaskModel> SharedTaskCollection
        {
            get
            {
                foreach (var task in _sharedTaskCollection)
                {
                    task.DateFinishString = task.DateFinish?.Date.ToShortDateString();
                }
                return _sharedTaskCollection;
            }
            set { SetProperty(ref _sharedTaskCollection, value); }
        }

        private ShortTaskModel _sharedTaskModel;

        public ShortTaskModel SelectedSharedTask
        {
            get
            {
                return _sharedTaskModel;
            }
            set { SetProperty(ref _sharedTaskModel, value); }
        }

        private ListShortTaskModel _listShortTaskModel;

        public ListShortTaskModel ListShortTaskModel
        {
            get { return _listShortTaskModel; }
            set { SetProperty(ref _listShortTaskModel, value); }
        }

        private string _pageVisibility;

        public string PageVisibility
        {
            get { return _pageVisibility; }
            set
            {
                SetProperty(ref _pageVisibility, value);
            }
        }

        #endregion

        #region Commands
        
        public ICommand DetailsCommand { get; set; }
        public ICommand NextPageCommand { get; set; }
        public ICommand PreviousPageCommand { get; set; }

        private void DetailsTask()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("SharedTaskView");
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("FullTaskView");
            _eventAggregator.GetEvent<DetailsTaskEvent>().Publish(SelectedSharedTask.IDTask);
        }

        private void NextPage()
        {
            GetTasks(ListShortTaskModel.PagingParameter.CurrentPage + 1);
        }

        private void PreviousPage()
        {
            GetTasks(ListShortTaskModel.PagingParameter.CurrentPage - 1);
        }

        private void GetTasks(int? pageNumber = 1)
        {
            ListShortTaskModel = SwapToModel(_taskService.GetByUser(_token, _paginationSetting.GetPaginationSetting(), pageNumber));
            if (ListShortTaskModel.PagingParameter.NextPage.ToLower() == "no" &&
                ListShortTaskModel.PagingParameter.PreviousPage.ToLower() == "no")
            {
                PageVisibility = Visibility.Hidden.ToString();
            }
            else
            {
                PageVisibility = Visibility.Visible.ToString();
            }
            SharedTaskCollection = new ObservableCollection<ShortTaskModel>(SwapToShortModels(ListShortTaskModel.Tasks));
        }

        private bool CanDetailsExecute()
        {
            return SelectedSharedTask != null;
        }

        private bool CanGetNextPage()
        {
            if (ListShortTaskModel != null)
                return ListShortTaskModel.PagingParameter.NextPage.ToLower() == "yes";
            return false;
        }

        private bool CanGetPreviousPage()
        {
            if (ListShortTaskModel != null)
                return ListShortTaskModel.PagingParameter.PreviousPage.ToLower() == "yes";
            return false;
        }

        #endregion

        #region Swap

        private static ListShortTaskModel SwapToModel(IListShortTaskModel dto)
        {
            return new ListShortTaskModel
            {
               Tasks = dto.Tasks,
                PagingParameter = dto.PagingParameter
            };
        }

        private static ShortTaskModel SwapToShortModel(IShortTaskModel dto)
        {
            return new ShortTaskModel
            {
                IDTask = dto.IDTask,
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                Priority = dto.Priority,
                DateFinish = dto.DateFinish,
                DateFinishString = dto.DateFinishString,
                UserName = dto.UserName
            };
        }

        private static IEnumerable<ShortTaskModel> SwapToShortModels(IEnumerable<IShortTaskModel> dtoes)
        {
            return dtoes.Select(SwapToShortModel).ToList();

        }

        #endregion
    }
}
