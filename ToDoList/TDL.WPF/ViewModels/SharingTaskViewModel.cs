﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using TDL.WPF.Events;
using System.Windows.Input;
using Prism.Commands;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Infrastructure;

namespace TDL.WPF.ViewModels
{
    public class SharingTaskViewModel : NotifyDataErrorInfoBase
    {
        private readonly IEventAggregator _eventAggregator;

        private readonly IUserService _userService;

        private readonly ISharedTaskService _sharedTaskService;

        private readonly string _token;

        public SharingTaskViewModel(IEventAggregator eventAggregator, IUserService userService, ISharedTaskService sharedTaskService)
        {
            _token = Application.Current.Resources["Token"].ToString();
            _eventAggregator = eventAggregator;
            eventAggregator.GetEvent<SharingTaskEvent>().Subscribe(Details);
            _sharedTaskService = sharedTaskService;
            _selectedAddedUser = new SharedTaskModel();
            _userService = userService;
            SearchUserCommand = new DelegateCommand(SearchUser, CanSearch).ObservesProperty(() => UserName);
            AddUserCommand = new DelegateCommand(AddUser, CanAddUser).ObservesProperty(() => SelectedAvailableUser);
            RemoveUserCommand = new DelegateCommand(RemoveUser, CanRemoveUser).ObservesProperty(() => SelectedAddedUser);
            AvailableUsersCollection = new ObservableCollection<ShortUserModel>();
            ShowCategoryCommand = new DelegateCommand(ShowCategory);
        }

        #region Properties

        private int _idTask;

        public int IDTask
        {
            get { return _idTask; }
            set { SetProperty(ref _idTask, value); }
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set
            {
                SetProperty(ref _userName, value);
                ValidateCustomErrors(nameof(UserName));
            }
        }

        private string _taskName;

        public string TaskName
        {
            get { return _taskName; }
            set
            {
                SetProperty(ref _taskName, value);
            }
        }

        private ObservableCollection<SharedTaskModel> _addedUserCollection;

        public ObservableCollection<SharedTaskModel> AddedUsersCollection
        {
            get { return _addedUserCollection; }
            set { SetProperty(ref _addedUserCollection, value); }
        }

        private ObservableCollection<ShortUserModel> _availableUsers;

        public ObservableCollection<ShortUserModel> AvailableUsersCollection
        {
            get { return _availableUsers; }
            set { SetProperty(ref _availableUsers, value); }
        }

        private SharedTaskModel _selectedAddedUser;

        public SharedTaskModel SelectedAddedUser
        {
            get { return _selectedAddedUser; }
            set { SetProperty(ref _selectedAddedUser, value); }
        }

        private ShortUserModel _selectedAvailableUser;

        public ShortUserModel SelectedAvailableUser
        {
            get { return _selectedAvailableUser; }
            set { SetProperty(ref _selectedAvailableUser, value); }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            ClearErrors(propertyName);

            switch (propertyName)
            {
                case nameof(UserName):
                {
                    if (string.IsNullOrEmpty(UserName))
                    {
                        AddError(propertyName, "User Name can not be empty");
                    }
                    break;
                }
            }
        }

        #endregion

        #region Commands

        public ICommand SearchUserCommand { get; set; }
        public ICommand AddUserCommand { get; set; }
        public ICommand RemoveUserCommand { get; set; }
        public ICommand ShowCategoryCommand { get; set; }

        private void SearchUser()
        {
            AvailableUsersCollection = new ObservableCollection<ShortUserModel>(SwapToUserModels(_userService.Get(UserName, _token)));
            for (var i = AvailableUsersCollection.Count - 1; i >= 0; i--)
            {
                foreach (var addedUser in AddedUsersCollection)
                {
                    if (AvailableUsersCollection[i].IdUser == addedUser.IDUser)
                        AvailableUsersCollection.Remove(AvailableUsersCollection[i]);
                }
            }

        }

        private bool CanSearch()
        {
            return !String.IsNullOrWhiteSpace(UserName);
        }

        private void Details(TaskModel model)
        {
            IDTask = model.IDTask;
            TaskName = model.Name;
            AddedUsersCollection = new ObservableCollection<SharedTaskModel>(SwapToModels(_sharedTaskService.Get(IDTask, _token)));
        }

        private void RemoveUser()
        {
            _sharedTaskService.Delete(SelectedAddedUser.IDSharedTask, _token);
            AvailableUsersCollection.Add(SwapToUserModel(SelectedAddedUser.User));
            AddedUsersCollection.Remove(SelectedAddedUser);
        }

        private bool CanRemoveUser()
        {
            return SelectedAddedUser?.User != null;
        }

        private void AddUser()
        {
            _sharedTaskService.Add(new SharedTaskModel
            {
                IDTask = IDTask,
                IDUser = SelectedAvailableUser.IdUser
            }, _token);
            AddedUsersCollection = new ObservableCollection<SharedTaskModel>(SwapToModels(_sharedTaskService.Get(IDTask, _token)));
            AvailableUsersCollection.Remove(SelectedAvailableUser);
        }

        private bool CanAddUser()
        {
            return SelectedAvailableUser?.UserName != null;
        }
        
        private void ShowCategory()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("CategoryView");
        }

        #endregion

        #region Swap

        private static SharedTaskModel SwapToModel(ISharedTaskModel dto)
        {
            return new SharedTaskModel
            {
                IDSharedTask = dto.IDSharedTask,
                IDTask = dto.IDTask,
                IDUser = dto.IDUser,
                User = dto.User
            };
        }

        private static IEnumerable<SharedTaskModel> SwapToModels(IEnumerable<ISharedTaskModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        private static ShortUserModel SwapToUserModel(IShortUserModel dto)
        {
            return new ShortUserModel
            {
                UserName = dto.UserName,
                IdUser = dto.IdUser
            };
        }

        private static IEnumerable<ShortUserModel> SwapToUserModels(IEnumerable<IShortUserModel> dtoes)
        {
            return dtoes.Select(SwapToUserModel).ToList();

        }

        #endregion

    }
}
