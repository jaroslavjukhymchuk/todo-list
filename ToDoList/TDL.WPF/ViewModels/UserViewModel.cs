﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Events;
using TDL.WPF.Infrastructure;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class UserViewModel : NotifyDataErrorInfoBase
    {
        private readonly IEventAggregator _eventAggregator;

        private readonly IUserService _userService;

        private PaginationSeting _paginationSetting;

        private readonly string _token;

        public UserViewModel(IEventAggregator eventAggregator, IUserService userService)
        {
            _token = Application.Current.Resources["Token"].ToString();
            _eventAggregator = eventAggregator;
            _userModel = new UserModel();
            UpdateCommand = new DelegateCommand(UpdateUser, CanUpdate).ObservesProperty(() => UserName).ObservesProperty(() => PhoneNumber).ObservesProperty(() => Email).ObservesProperty(() => PaginationNumber);
            ShowStatusesCommand = new DelegateCommand(ShowStatuses);
            ShowUsersCommand = new DelegateCommand(ShowUsers);
            DeleteCommand = new DelegateCommand(DeleteUser);
            _userService = userService;
            User = SwapToModel(_userService.Get(_token));
            VisibilityAdminInformation = User.IsAdmin ? Visibility.Visible.ToString() : Visibility.Hidden.ToString();
            ShowCategoryCommand = new DelegateCommand(ShowCategory);
            _paginationSetting = new PaginationSeting();
            PaginationNumber = _paginationSetting.GetPaginationSetting();
        }

        #region Properties

        private string _userName;

        public string UserName
        {
            get { return User.UserName; }
            set
            {
                User.UserName = value;
                SetProperty(ref _userName, value);
                ValidateDataAnnotationsErrors(nameof(UserName), value, _userModel);
            }
        }

        private string _email;

        public string Email
        {
            get { return User.Email; }
            set
            {
                User.Email = value;
                SetProperty(ref _email, value);
                ValidateDataAnnotationsErrors(nameof(Email), value, _userModel);
            }
        }

        private int _paginationNumber;

        public int PaginationNumber
        {
            get { return _paginationNumber; }
            set { SetProperty(ref _paginationNumber, value); }
        }

        private string _visibilityAdminInformation;

        public string VisibilityAdminInformation
        {
            get { return _visibilityAdminInformation; }
            set { SetProperty(ref _visibilityAdminInformation, value); }
        }

        private string _phoneNumber;

        public string PhoneNumber
        {
            get { return User.PhoneNumber; }
            set
            {
                User.PhoneNumber = value;
                SetProperty(ref _phoneNumber, value);
                ValidateDataAnnotationsErrors(nameof(PhoneNumber), value, _userModel);
            }
        }

        private UserModel _userModel;

        public UserModel User
        {
            get { return _userModel; }
            set { SetProperty(ref _userModel, value); }
        }

        #endregion

        #region Commands

        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ShowStatusesCommand { get; set; }
        public ICommand ShowUsersCommand { get; set; }
        public ICommand ShowCategoryCommand { get; set; }

        private void UpdateUser()
        {
             _userService.Update(User, _token);
            EditPaginationSetting();
        }

        private void EditPaginationSetting()
        {
            _paginationSetting.EditPaginationSetting(PaginationNumber);
        }

        private bool CanUpdate()
        {
            return !HasErrors && PaginationNumber >= 1;
        }

        private void DeleteUser()
        {
            _userService.Delete(_token);
            System.Diagnostics.Process.Start(
                Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }
        

        private void ShowStatuses()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("StatusView");
        }

        private void ShowUsers()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("AdminUserView");
        }

        private void ShowCategory()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("CategoryView");
        }

        #endregion

        #region Swap

        private static UserModel SwapToModel(IUserModel dto)
        {
            return new UserModel
            {
               Email = dto.Email,
                PhoneNumber = dto.PhoneNumber,
                UserName = dto.UserName,
                Id = dto.Id,
                IsAdmin = dto.IsAdmin
            };
        }

        private static IEnumerable<UserModel> SwapToModels(IEnumerable<IUserModel> dtoes)
        {
            return dtoes.Select(SwapToModel).ToList();

        }

        #endregion
    }
}
