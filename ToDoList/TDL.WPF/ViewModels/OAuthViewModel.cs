﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Prism.Events;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using TDL.WPF.Infrastructure;
using Prism.Commands;
using TDL.WPF.Events;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;

namespace TDL.WPF.ViewModels
{
    public class OAuthViewModel : NotifyDataErrorInfoBase
    {
        
        private readonly IEventAggregator _eventAggregator;

        private readonly IOAuthService _authService;
        
        private readonly OAuthModel _authModel;
        

        public OAuthViewModel(IEventAggregator eventAggregator, IOAuthService authService)
        {
            _eventAggregator = eventAggregator;
            _authModel = new OAuthModel();
            _authService = authService;
            LoginCommand = new DelegateCommand<object>(LoginUser, CanLogin).ObservesProperty(() => Login);
        }

        #region Properties

        private string _userName;

        public string Login
        {
            get { return _userName; }
            set
            {
                SetProperty(ref _userName, value);
                ValidateDataAnnotationsErrors(nameof(Login), value, _authModel);
            }
        }

        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                SetProperty(ref _errorMessage, value);
            }
        }

        private string _registerView = "RegisterView.xaml";

        public string RegisterView
        {
            get { return _registerView; }
            set
            {
                SetProperty(ref _registerView, value);
            }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand { get; set; }

        private void LoginUser(object password)
        {

            try
            {
                string clearTextPassword = null;
                if (password is PasswordBox passwordBox)
                {
                    clearTextPassword = passwordBox.Password;
                }
                Application.Current.Resources["Token"] = _authService.GetTokenDictionary(Login, clearTextPassword)["access_token"];
                _eventAggregator.GetEvent<ShowViewEvent>().Publish("CategoryView");
            }
            catch (KeyNotFoundException)
            {
                ErrorMessage = "Login failed! Please provide some valid credentials.";
            }
        }

        private bool CanLogin(object password)
        {
            return !IsAuthenticated && !String.IsNullOrWhiteSpace(Login);
        }

        public bool IsAuthenticated
        {
            get { return Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        #endregion
        
    }

}
