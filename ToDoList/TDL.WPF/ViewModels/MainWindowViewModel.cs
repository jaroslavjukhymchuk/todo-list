﻿using System.Windows;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System.Windows.Input;
using TDL.WebApi.IService.IService;
using TDL.WPF.Events;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {

        private readonly IRegionManager _regionManager;

        private readonly IOAuthService _authService;
        

        public MainWindowViewModel(IRegionManager regionManager, IEventAggregator eventAggregator, IOAuthService authService)
        {
            _regionManager = regionManager;
            _authService = authService;
            eventAggregator.GetEvent<ShowViewEvent>().Subscribe(Navigate);
            NavigateCommand = new DelegateCommand<string>(Navigate);
            LogOutCommand = new DelegateCommand(LogOut);
            LogOutVisibility = Visibility.Hidden.ToString();
        }

        #region Properties
        

        private string _logOutVisibility;

        public string LogOutVisibility
        {
            get { return _logOutVisibility; }
            set { SetProperty(ref _logOutVisibility, value); }
        }

        #endregion

        #region Commands

        public ICommand LogOutCommand { get; set; }

        public DelegateCommand<string> NavigateCommand { get; set; }

        private void Navigate(string uri)
        {
            if (uri != "OAuthView" && uri != "RegisterView")
            {

                LogOutVisibility = Visibility.Visible.ToString();
            }
            else
            {
                LogOutVisibility = Visibility.Hidden.ToString();
            }
            _regionManager.RequestNavigate("ContentRegion", "CategoryView");
            _regionManager.RequestNavigate("ContentRegion", uri);
        }

        private void LogOut()
        {
            _authService.LogOut(Application.Current.Resources["Token"].ToString());
            System.Diagnostics.Process.Start(
                Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        #endregion

    }
}
