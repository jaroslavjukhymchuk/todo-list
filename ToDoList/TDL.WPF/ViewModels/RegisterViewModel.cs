﻿using Prism.Commands;
using System;
using System.Windows.Controls;
using System.Windows.Input;
using Prism.Events;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Infrastructure;
using TDL.WPF.Events;

namespace TDL.WPF.ViewModels
{
    public class RegisterViewModel : NotifyDataErrorInfoBase
    {

        private readonly IEventAggregator _eventAggregator;

        private readonly IOAuthService _authService;

        private readonly OAuthModel _authModel;

        public RegisterViewModel(IEventAggregator eventAggregator, IOAuthService authService)
        {
            _eventAggregator = eventAggregator;
            _authService = authService;
            _authModel = new OAuthModel();
            RegisterCommand = new DelegateCommand<object>(Register, CanRegister).ObservesProperty(() => Login).ObservesProperty(() => Email);
        }

        #region Properties

        private string _userName;

        public string Login
        {
            get { return _userName; }
            set
            {
                SetProperty(ref _userName, value);
                ValidateDataAnnotationsErrors(nameof(Login), value, _authModel);
            }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set
            {
                SetProperty(ref _email, value);
                ValidateDataAnnotationsErrors(nameof(Email), value, _authModel);
            }
        }

        private string _message;

        public string ErrorMessage
        {
            get { return _message; }
            set
            {
                SetProperty(ref _message, value);
            }
        }

        private string _oauthView = "OAuthView.xaml";

        public string OAuthView
        {
            get { return _oauthView; }
            set
            {
                SetProperty(ref _oauthView, value);
            }
        }

        #endregion

        #region Commands

        public ICommand RegisterCommand { get; set; }

        private void Register(object password)
        {
            var clearTextPassword = ConvertPassword(password);
            if (clearTextPassword  != null)
            {
                var result = _authService.Register(new OAuthModel
                {
                    Login = Login,
                    Email = Email,
                    Password = clearTextPassword,
                    ConfirmPassword = clearTextPassword
                });
                if (result.StatusCode == "OK")
                {
                     _eventAggregator.GetEvent<ShowViewEvent>().Publish("OAuthView");
                }
                else
                {
                    ErrorMessage = result.Messages;
                }
            }
            else
            {
                ErrorMessage = "Please enter password";
            }
        }

        private bool CanRegister(object password)
        {
            return  !String.IsNullOrWhiteSpace(Login) && !String.IsNullOrWhiteSpace(Email) && !HasErrors;

        }

        private string ConvertPassword(object password)
        {
            if (password is PasswordBox passwordBox)
            {
                if(passwordBox.Password.Length != 0)
                return passwordBox.Password;
            }

            return null;
        }

        #endregion

    }
}
