﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;
using TDL.WebApi.IService.IModels;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Models;
using TDL.WPF.Events;
using TDL.WPF.Infrastructure;
using Visibility = TDL.WPF.Infrastructure.Visibility;

namespace TDL.WPF.ViewModels
{
    public class FullTaskViewModel : NotifyDataErrorInfoBase
    {
        
        private readonly IEventAggregator _eventAggregator;

        private readonly ITaskService _taskService;

        private readonly IStatusService _statusService;
        
        private readonly ICommentService _commentService;

        private readonly string _token;

        private PaginationSeting _paginationSetting;

        public FullTaskViewModel(IEventAggregator eventAggregator, ITaskService taskService, IStatusService statusService, ICommentService commentService)
        {
            _paginationSetting = new PaginationSeting();
            _token = Application.Current.Resources["Token"].ToString();
            _commentService = commentService;
            _eventAggregator = eventAggregator;
            _taskModel = new TaskModel();
            _taskService = taskService;
            _commentModel = new CommentModel();
            eventAggregator.GetEvent<DetailsTaskEvent>().Subscribe(Details);
            _statusService = statusService;
            UpdateCommand = new DelegateCommand(UpdateTask, CanUpdate).ObservesProperty(() => TaskModel).ObservesProperty(() => Name).ObservesProperty(() => Status)
                .ObservesProperty(() => Description).ObservesProperty(() => DateFinish);
            AddCommentCommand = new DelegateCommand(AddComment, CanAddComment).ObservesProperty(() => CommentDescription);
            ShowCategoryCommand = new DelegateCommand(ShowCategory);
            ShowCommentsCommand = new DelegateCommand(ShowComments);
            ShowAddCommentsCommand = new DelegateCommand(ShowAddComments);
            SharingTaskCommand = new DelegateCommand(ShowSharingTask);
            DeleteCommentCommand = new DelegateCommand<int?>(DeleteComment, CanDeleteComment).ObservesProperty(() => SelectedComment);
            CommentVisibility = Visibility.Hidden.ToString();
            AddCommentVisibility = Visibility.Hidden.ToString();
        }

        #region Properties

        private TaskModel _taskModel;

        public TaskModel TaskModel
        {
            get { return _taskModel; }
            set { SetProperty(ref _taskModel, value); }
        }

        private ObservableCollection<StatusModel> _statusCollection;

        public ObservableCollection<StatusModel> StatusCollection
        {
            get { return _statusCollection; }
            set { SetProperty(ref _statusCollection, value); }
        }

        private ObservableCollection<CommentModel> _commentCollection;

        public ObservableCollection<CommentModel> CommentCollection
        {
            get
            {
                if (_commentCollection == null)
                {
                    return _commentCollection;
                }
                foreach (var comment in _commentCollection)
                {
                    comment.DateCreationString = comment.DateCreation.Date.ToShortDateString();
                }
                return _commentCollection;
            }
            set { SetProperty(ref _commentCollection, value); }
        }

        private string _commentDescription;

        public string CommentDescription
        {
            get { return _commentDescription; }
            set
            {
                SetProperty(ref _commentDescription, value);
                ValidateCustomErrors(nameof(CommentDescription));
            }
        }

        private CommentModel _commentModel;

        public CommentModel SelectedComment
        {
            get { return _commentModel; }
            set { SetProperty(ref _commentModel, value); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                TaskModel.Name = value;
                SetProperty(ref _name, value);
                ValidateDataAnnotationsErrors(nameof(Name), value, _taskModel);
            }
        }

        private StatusModel _status;

        public StatusModel Status
        {
            get { return _status; }
            set
            {
                TaskModel.Status = value;
                SetProperty(ref _status, value);
                ValidateDataAnnotationsErrors(nameof(Status), value, _taskModel);
            }
        }

        private int _startIndexStatus;

        public int StartIndexStatus
        {
            get { return _startIndexStatus; }
            set { SetProperty(ref _startIndexStatus, value); }
        }

        private string _description;

        public string Description
        {
            get { return TaskModel.Description; }
            set
            {
                TaskModel.Description = value;
                SetProperty(ref _description, value);
                ValidateDataAnnotationsErrors(nameof(Description), value, _taskModel);
            }
        }

        private DateTime? _dateFinish;

        public DateTime? DateFinish
        {
            get { return TaskModel.DateFinish; }
            set
            {
                TaskModel.DateFinish = value;
                SetProperty(ref _dateFinish, value);
                ValidateCustomErrors(nameof(DateFinish));
            }
        }

        private string _commentVisibility;

        public string CommentVisibility
        {
            get { return _commentVisibility; }
            set
            {
                SetProperty(ref _commentVisibility, value);
            }
        }

        private string _addCommentVisibility;

        public string AddCommentVisibility
        {
            get { return _addCommentVisibility; }
            set
            {
                SetProperty(ref _addCommentVisibility, value);
            }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            ClearErrors(propertyName);

            switch (propertyName)
            {
                case nameof(DateFinish):
                {
                    if (DateFinish != null)
                    {
                        if (DateFinish.Value < DateTime.Today)
                        {
                            AddError(propertyName, "Date finish can not be less today date!");
                        }
                    }
                    break;
                }
                case nameof(CommentDescription):
                {
                    if (string.IsNullOrEmpty(CommentDescription))
                    {
                       AddError(propertyName, "Comment can not be empty!");
                    }
                    else
                    {
                        if (CommentDescription.Length > 50)
                        {
                            AddError(propertyName, "Comment can not have lenght more 50!");
                        }
                    }

                    break;
                }
            }
        }
        #endregion

        #region Commands

        public ICommand UpdateCommand { get; set; }
        public ICommand ShowCategoryCommand { get; set; }
        public ICommand DeleteCommentCommand { get; set; }
        public ICommand AddCommentCommand { get; set; }
        public ICommand SharingTaskCommand { get; set; }
        public ICommand ShowCommentsCommand { get; set; }
        public ICommand ShowAddCommentsCommand { get; set; }

        private void UpdateTask()
        {
          _taskService.Update(TaskModel, _token);
        }

        private void AddComment()
        {
           _commentService.Add(new CommentModel
            {
              Description = CommentDescription,
              IDTask = TaskModel.IDTask,
              DateCreation = DateTime.Today
            }, _token);
            CommentCollection = new ObservableCollection<CommentModel>(SwapApiCommentModelsToCommentModels(_commentService.Get(TaskModel.IDTask, _token)));
            AddCommentVisibility = Visibility.Hidden.ToString();
        }

        private void DeleteComment(int? idComment)
        {
           _commentService.Delete((int)idComment, _token);
           CommentCollection = new ObservableCollection<CommentModel>(SwapApiCommentModelsToCommentModels(_commentService.Get(TaskModel.IDTask, _token)));
        }

        private void ShowCategory()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("CategoryView");
        }

        private void ShowComments()
        {
            CommentVisibility = CommentVisibility == "Hidden" ? Visibility.Visible.ToString() : Visibility.Hidden.ToString(); ;
        }

        private void ShowAddComments()
        {
            AddCommentVisibility = AddCommentVisibility == "Hidden" ? Visibility.Visible.ToString() : Visibility.Hidden.ToString();
        }

        private void ShowSharingTask()
        {
            _eventAggregator.GetEvent<ShowViewEvent>().Publish("SharingTaskView");
            _eventAggregator.GetEvent<SharingTaskEvent>().Publish(TaskModel);
        }

        private bool CanDeleteComment(int? idCategory)
        {
            return SelectedComment?.Description != null;
        }

        private bool CanUpdate()
        {
            return !string.IsNullOrWhiteSpace(Name) && Status != null && !HasErrors;
        }

        private bool CanAddComment()
        {
            return !string.IsNullOrWhiteSpace(CommentDescription);
        }

        private void Details(int idTask)
        {
            StatusCollection?.Clear();
            TaskModel = SwapApiTaskModelToTaskModel(_taskService.GetById(idTask, _token, _paginationSetting.GetPaginationSetting()));
            Name = TaskModel.Name;
            StatusCollection = new ObservableCollection<StatusModel>(SwapApiStatusModelsToStatusModels(_statusService.Get(_token)));
            Status = SwapApiStatusModelToStatusModel(TaskModel.Status);
            StartIndexStatus = StatusCollection.IndexOf(Status);
            CommentCollection = new ObservableCollection<CommentModel>(SwapApiCommentModelsToCommentModels(_commentService.Get(TaskModel.IDTask, _token)));
        }

        #endregion

        #region Swap

        private static CommentModel SwapApiCommentModelToCommentModel(ICommentModel dto)
        {
            return new CommentModel
            {
                IDComment = dto.IDComment,
                IDTask = dto.IDTask,
                Description = dto.Description,
                DateCreation = dto.DateCreation,
                IDUser = dto.IDUser,
                User = dto.User,
                DateCreationString = dto.DateCreationString
            };
        }

        private static IEnumerable<CommentModel> SwapApiCommentModelsToCommentModels(IEnumerable<ICommentModel> dtoes)
        {
            return dtoes.Select(SwapApiCommentModelToCommentModel).ToList();

        }

        private static TaskModel SwapApiTaskModelToTaskModel(ITaskModel dto)
        {
            return new TaskModel
            {
                IDTask = dto.IDTask,
                Description = dto.Description,
                DateCreation = dto.DateCreation,
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                Priority = dto.Priority,
                DateFinish = dto.DateFinish,
                Status = dto.Status
            };
        }

        private static IEnumerable<StatusModel> SwapApiStatusModelsToStatusModels(IEnumerable<IStatusModel> dtoes)
        {
            return dtoes.Select(SwapApiStatusModelToStatusModel).ToList();

        }

        private static StatusModel SwapApiStatusModelToStatusModel(IStatusModel dto)
        {
            return new StatusModel
            {
                IDStatus = dto.IDStatus,
                Name = dto.Name
            };
        }


        #endregion
    }
}
