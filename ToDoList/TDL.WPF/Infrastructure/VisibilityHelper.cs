﻿namespace TDL.WPF.Infrastructure
{
        public enum Visibility
        {
            Visible,
            Hidden,
            Collapsed
        }
}
