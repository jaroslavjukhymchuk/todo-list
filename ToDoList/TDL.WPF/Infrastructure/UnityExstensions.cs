﻿using Microsoft.Practices.Unity;

namespace TDL.WPF.Infrastructure
{
    public static class UnityExstensions
    {
        public static void RegisterTypeForNavigation<T>(this IUnityContainer container, string name)
        {
            container.RegisterType(typeof(object), typeof(T), name);
        }
    }
}
