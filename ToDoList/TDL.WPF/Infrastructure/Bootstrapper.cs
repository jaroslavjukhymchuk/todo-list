﻿using System.Windows;
using Microsoft.Practices.Unity;
using Prism.Unity;
using TDL.WebApi.IService.IService;
using TDL.WebApi.Service.Services.CategoryServices;
using TDL.WebApi.Service.Services.CommentServices;
using TDL.WebApi.Service.Services.OAuthServices;
using TDL.WebApi.Service.Services.StatusServices;
using TDL.WebApi.Service.Services.TaskServices;
using TDL.WPF.Views;

namespace TDL.WPF.Infrastructure
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
                Application.Current.MainWindow?.Show();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterTypeForNavigation<CategoryView>("CategoryView");
            Container.RegisterTypeForNavigation<OAuthView>("OAuthView");
            Container.RegisterTypeForNavigation<RegisterView>("RegisterView");
            Container.RegisterTypeForNavigation<TaskView>("TaskView");
            Container.RegisterTypeForNavigation<FullTaskView>("FullTaskView");
            Container.RegisterTypeForNavigation<SharedTaskView>("SharedTaskView");
            Container.RegisterTypeForNavigation<SharingTaskView>("SharingTaskView");
            Container.RegisterTypeForNavigation<UserView>("UserView");
            Container.RegisterTypeForNavigation<StatusView>("StatusView");
            Container.RegisterTypeForNavigation<AdminUserView>("AdminUserView");
            Container.RegisterType<ICategoryService, CategoryService>();
            Container.RegisterType<ICommentService, CommentService>();
            Container.RegisterType<IOAuthService, OAuthService>();
            Container.RegisterType<ISharedTaskService, SharedTaskService>();
            Container.RegisterType<IStatusService, StatusService>();
            Container.RegisterType<ITaskService, TaskService>();
            Container.RegisterType<IUserService, UserService>();
        }
    }
}
