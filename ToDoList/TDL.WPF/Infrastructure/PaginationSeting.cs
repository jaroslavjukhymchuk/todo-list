﻿using System.Configuration;

namespace TDL.WPF.Infrastructure
{
    public class PaginationSeting
    {
        public void EditPaginationSetting(int newPageSize)
        {
           ConfigurationManager.AppSettings["PageSize"] = newPageSize.ToString();
        }

        public int GetPaginationSetting()
        {
            return int.Parse(ConfigurationManager.AppSettings["PageSize"]);
        }
    }
}
