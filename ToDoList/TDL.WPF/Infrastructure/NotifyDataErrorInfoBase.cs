﻿using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TDL.WPF.Infrastructure
{
    public class NotifyDataErrorInfoBase : BindableBase, INotifyDataErrorInfo
    {
        private readonly Dictionary<string, List<string>> _errorsByProperyName = new Dictionary<string, List<string>>();

        public bool HasErrors => _errorsByProperyName.Any();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public IEnumerable GetErrors(string propertyName)
        {
            return _errorsByProperyName.ContainsKey(propertyName) ? _errorsByProperyName[propertyName] : null;
        }

        protected virtual void OnErrorsChanged(string properyName)
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(properyName));
        }

        protected void AddError(string propertyName, string error)
        {
            if (!_errorsByProperyName.ContainsKey(propertyName))
            {
                _errorsByProperyName[propertyName] = new List<string>();
            }

            if (!_errorsByProperyName[propertyName].Contains(error))
            {
                _errorsByProperyName[propertyName].Add(error);
                OnErrorsChanged(propertyName);
            }
        }

        protected void ClearErrors(string propertyName)
        {
            if (_errorsByProperyName.ContainsKey(propertyName))
            {
                _errorsByProperyName.Remove(propertyName);
                OnErrorsChanged(propertyName);
            }
        }

        protected void ValidateDataAnnotationsErrors(string propertyName, object currentValue, object instance)
        {
            ClearErrors(propertyName);
            var results = new List<ValidationResult>();
            var context = new ValidationContext(instance) { MemberName = propertyName };
            Validator.TryValidateProperty(currentValue, context, results);
            foreach (var result in results)
            {
                AddError(propertyName, result.ErrorMessage);
            }
        }

    }
}
