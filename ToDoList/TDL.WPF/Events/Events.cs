﻿using Prism.Events;
using TDL.WebApi.Service.Models;

namespace TDL.WPF.Events
{
    public class DetailsEvent : PubSubEvent<int?>
    {
    }

    public class ShowViewEvent : PubSubEvent<string>
    {
    }
    
    public class DetailsTaskEvent : PubSubEvent<int>
    {
    }

    public class SharingTaskEvent : PubSubEvent<TaskModel>
    {
    }

}
