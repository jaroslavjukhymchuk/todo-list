﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface ICategoryService
    {
        IEnumerable<ICategoryModel> Get(string token);

        void Add(ICategoryModel newCategory, string token);

        void Update(ICategoryModel updateCategory, string token);

        void Delete(int idCategory, string token);

        IEnumerable<ICategoryModel> Search(string nameCategory, string token);

        ICategoryModel GetById(int id, string token);
    }
}
