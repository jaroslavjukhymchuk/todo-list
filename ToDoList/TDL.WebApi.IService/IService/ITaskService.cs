﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface ITaskService
    {
        IListShortTaskModel Get(int idCategory, string token, int pageSize, int? pageNumber = 1);

        ITaskModel GetById(int idTask, string token, int pageSize = 1);

        IEnumerable<IShortTaskModel> Search(int idCategory, string nameTask, string token);

        IListShortTaskModel GetByUser(string token, int pageSize, int? pageNumber = 1);

        void Add(ITaskModel newTask, string token);

        void Update(ITaskModel updateTask, string token);

        void Delete(int idTask, string token);
    }
}
