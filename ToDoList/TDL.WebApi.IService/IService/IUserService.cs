﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface IUserService
    {
        IEnumerable<IShortUserModel> Get(string nameUser, string token);

        IUserModel Get(string token, int? idUser = null);

        IEnumerable<IUserModel> GetAll(string token);

        IMessageModel Update(IUserModel updateUser, string token, int? idUser = null);

        void Delete(string token, int? idUser = null);
    }
}
