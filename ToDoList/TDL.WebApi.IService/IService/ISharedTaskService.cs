﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface ISharedTaskService
    {
        IEnumerable<ISharedTaskModel> Get(int idTask, string token);

        void Add(ISharedTaskModel newShared, string token);

        void Delete(int idSharedTask, string token);
    }
}
