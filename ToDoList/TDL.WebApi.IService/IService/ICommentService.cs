﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface ICommentService
    {
        IEnumerable<ICommentModel> Get(int idTask, string token);

        void Add(ICommentModel comment, string token);

        void Delete(int idComment, string token);
    }
}
