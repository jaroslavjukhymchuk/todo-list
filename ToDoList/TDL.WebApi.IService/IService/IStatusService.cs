﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface IStatusService
    {
        IEnumerable<IStatusModel> Get(string token);

        void Add(IStatusModel newStatus, string token);

        void Update(IStatusModel updateStatus, string token);

        IStatusModel GetById(int id, string token);

        void Delete(int idStatus, string token);
    }
}
