﻿using System.Collections.Generic;
using TDL.WebApi.IService.IModels;

namespace TDL.WebApi.IService.IService
{
    public interface IOAuthService
    {
        Dictionary<string, string> GetTokenDictionary(string userName, string password);

        IMessageModel Register(IOAuthModel registerModel);

        void LogOut(string token);
        IEnumerable<IExternalLoginModel> GetExternalLogins();

        Dictionary<string, string> ExternalLogin(IExternalLoginDataModel model);
    }
}
