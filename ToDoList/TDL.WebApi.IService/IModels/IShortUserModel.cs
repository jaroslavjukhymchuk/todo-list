﻿namespace TDL.WebApi.IService.IModels
{
    public interface IShortUserModel
    {
         string UserName { get; }
        
         int IdUser { get; }
    }
}
