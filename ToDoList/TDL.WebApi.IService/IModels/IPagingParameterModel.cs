﻿
namespace TDL.WebApi.IService.IModels
{
    public interface IPagingParameterModel
    {
         int PageSize { get; }
        
         int CurrentPage { get; }
        
         int TotalPages { get; }
        
         string PreviousPage { get; }
        
         string NextPage { get; }
    }
}
