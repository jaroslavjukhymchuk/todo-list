﻿using System;

namespace TDL.WebApi.IService.IModels
{
     public interface ITaskModel 
     {
         int IDTask { get; }

         int IDCategory { get; }
        
         string Name { get; }
        
         string Description { get; }

         int Priority { get; }

         DateTime DateCreation { get; }

         DateTime? DateFinish { get; }
        
         IStatusModel Status { get; }
    }
}
