﻿
namespace TDL.WebApi.IService.IModels
{
    public interface ICategoryModel 
    {
         int IDCategory { get; }

         int IDUser { get; }
        
         string Name { get; }
    }
}
