﻿using System;

namespace TDL.WebApi.IService.IModels
{
    public interface ICommentModel
    {
         int IDComment { get; }
        
         int IDTask { get; }
        
         string Description { get; }
        
         DateTime DateCreation { get; }
        
         int IDUser { get; }

         IShortUserModel User { get; }

         string DateCreationString { get; }
    }
}
