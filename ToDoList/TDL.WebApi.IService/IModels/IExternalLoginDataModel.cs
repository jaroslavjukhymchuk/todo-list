﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDL.WebApi.IService.IModels
{
    public interface IExternalLoginDataModel
    {
        string LoginProvider { get; }

        string ProviderKey { get; }

        string DefaultUserName { get; }

        string Email { get; }
    }
}
