﻿using System.Collections.Generic;

namespace TDL.WebApi.IService.IModels
{
    public interface IListShortTaskModel
    {
         IEnumerable<IShortTaskModel> Tasks { get; }

         IPagingParameterModel PagingParameter { get; }
    }
}
