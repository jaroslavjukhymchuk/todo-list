﻿

namespace TDL.WebApi.IService.IModels
{
    public interface IStatusModel
    {
         int IDStatus { get; }
        
         string Name { get; }

    }
      
    
}
