﻿

namespace TDL.WebApi.IService.IModels
{
    public interface ISharedTaskModel
    {
         int IDSharedTask { get; }
        
         int IDTask { get; }
        
         int IDUser { get; }
        
         IShortUserModel User { get; }
    }
}
