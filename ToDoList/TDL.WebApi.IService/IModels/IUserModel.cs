﻿

namespace TDL.WebApi.IService.IModels
{
    public interface IUserModel
    {
         string Email { get; }
        
         string PhoneNumber { get; }
        
         string UserName { get; }

         int Id { get; }

         bool IsAdmin { get; }
    }
}
