﻿
namespace TDL.WebApi.IService.IModels
{
    public interface IOAuthModel
    {
         string Login { get; }
        
         string Email { get; }
        
         string Password { get; }
        
         string ConfirmPassword { get; }

    }
}
