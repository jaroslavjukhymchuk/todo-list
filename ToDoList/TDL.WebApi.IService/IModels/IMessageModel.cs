﻿namespace TDL.WebApi.IService.IModels
{
    public interface IMessageModel
    {
         string StatusCode { get; }

         string Messages { get; }
    }
}
