﻿using System;

namespace TDL.WebApi.IService.IModels
{
    public interface IShortTaskModel 
    {

         int IDTask { get; }

         int IDCategory { get; }
        
         string Name { get; }

         int Priority { get; }

         DateTime? DateFinish { get; }

         string DateFinishString { get; }

         string UserName { get; }
    }
}
