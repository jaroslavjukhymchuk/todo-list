﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace TDL.WebApi.Results
{
    /// <summary>
    /// An ActionResult that on execution invokes AuthenticationManager.ChallengeAsync.
    /// </summary>
    public class ChallengeResult : IHttpActionResult
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="loginProvider">login provider</param>
        /// <param name="controller">api controler</param>
        public ChallengeResult(string loginProvider, ApiController controller)
        {
            LoginProvider = loginProvider;
            Request = controller.Request;
        }

        /// <summary>
        /// login provider
        /// </summary>
        public string LoginProvider { get; set; }
        /// <summary>
        /// request message
        /// </summary>
        public HttpRequestMessage Request { get; set; }

        /// <summary>
        /// get request message
        /// </summary>
        /// <param name="cancellationToken">cancellation Token</param>
        /// <returns>request message</returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            Request.GetOwinContext().Authentication.Challenge(LoginProvider);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.RequestMessage = Request;
            return Task.FromResult(response);
        }
    }
}
