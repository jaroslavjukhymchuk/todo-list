﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using ToDoList.Identity.Infrastructure;
using TDL.WebApi.Models;
using ToDoList.Identity.Models.Users;
using System.Web;
using Newtonsoft.Json;
using System.Web.Security;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// Api controller for work with user
    /// </summary>
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [SwaggerResponse(System.Net.HttpStatusCode.Unauthorized, "Need authorization")]
    public class UserController : ApiController
    {
        private EPUserManager _userManager;

        /// <summary>
        /// Search user by name
        /// </summary>
        /// <param name="name">name user</param>
        /// <returns>list User</returns>
        [HttpGet]
        [Route("api/Users/Search")]
        public IEnumerable<ShortUserModel> Get(string name)
        {
            var user = UserManager.Users.Where(users => users.UserName.Contains(name)).ToList();
            return user.Select(ShortModelToModel);

        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns>list User</returns>
        [HttpGet]
        [Route("api/Users")]
        [Authorize(Roles = "Administrators")]
        public IEnumerable<User> Get()
        {
            var user = UserManager.Users.ToList();
            return user;
        }

        private static ShortUserModel ShortModelToModel(User user)
        {
            return new ShortUserModel
            {
                IdUser = user.Id,
                UserName = user.UserName
            };
        }
        /// <summary>
        /// Get user by idUser
        /// </summary>
        /// <param name="idUser">idUser user</param>
        /// <returns>model user</returns>
        public User GetById(int? idUser = null)
        {
            User user;

            var idUserLogin = GetUserId();
            if (idUser == null)
            {
                 user = UserManager.Users.SingleOrDefault(users => users.Id == idUserLogin);
            }
            else
            {
                
                    user = UserManager.Users.SingleOrDefault(users => users.Id == idUser);
            }
            
                HttpContext.Current.Response.Headers.Add("IsAdmin",
                    UserManager.IsInRole(idUserLogin, "Administrators").ToString());

            return user;

        }

        /// <summary>
        /// Update information about user
        /// </summary>
        /// <param name="idUser">idUser user</param>
        /// <param name="user">update model user</param>
        /// <returns>http code</returns>
        [HttpPut]
        public IHttpActionResult Update( [FromBody]User user, int? idUser = null)
        {
           
         
            var idUserLogin = GetUserId();
            var result = new IdentityResult();
            if (idUser == null)
            {
                var userModel = UserManager.FindById(idUserLogin);
                userModel.Email = user.Email;
                userModel.PhoneNumber = user.PhoneNumber;
                userModel.UserName = user.UserName;
               result = UserManager.Update(userModel);
            }
            else
            {
                if (UserManager.IsInRole(idUserLogin, "Administrators"))
                {
                    var userModel = UserManager.FindById((int)idUser);
                    userModel.Email = user.Email;
                    userModel.PhoneNumber = user.PhoneNumber;
                    userModel.UserName = user.UserName;
                    result = UserManager.Update(userModel);
                }
            }


            if (result.Succeeded)
                return Ok();
            HttpContext.Current.Response.Headers.Add("ValidationErrors", result.Errors.First());
            return BadRequest();
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="idUser">idUser user</param>
        /// <returns>http code</returns>
        public IHttpActionResult Delete(int? idUser=null)
        {
            var idUserLogin = GetUserId();
            var result = new IdentityResult();
            if (idUser==null)
            {
                var userModel = UserManager.FindById(idUserLogin);
                 result = UserManager.Delete(userModel);
            }
            else
            {
               if(UserManager.IsInRole(idUserLogin, "Administrators"))
                {
                    var userModel = UserManager.FindById((int)idUser);
                     result = UserManager.Delete(userModel);
                }
            }
           
            
            if (result.Succeeded)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// initialization user manager
        /// </summary>
        public EPUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<EPUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private int GetUserId()
        {
            return UserManager.FindByName(User.Identity.Name).Id;
        }

    }
}
