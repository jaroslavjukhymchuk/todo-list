﻿using BL.Interface.IDTOs;
using BL.Interface.IServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TDL.WebApi.Models;
using ToDoList.Identity.Infrastructure;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// Api controller for work with category
    /// </summary>
    [SwaggerResponse(System.Net.HttpStatusCode.Unauthorized,"Need authorization")]
    public class CategoryController : ApiController
    {

        private readonly ICategoryService _categoryService;
       

        /// <summary>
        /// constructor without param
        /// </summary>
        public CategoryController()
        {
        }

        /// <summary>
        /// constructor for initializing ninject
        /// </summary>
        /// <param name="categoryService">interface service category</param>
        public CategoryController(ICategoryService categoryService)
        {

            _categoryService = categoryService;
        }
        

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private EPUserManager _userManager;

        public EPUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<EPUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        /// <summary>
        /// Get list category by user id or name category or category id
        /// </summary>
        /// <param name="idCategory">category id</param>
        ///  <param name="nameCategory">category name</param>
        /// <returns>list category</returns>
        [Route("api/Categories/")]
        public IEnumerable<CategoryModel> Get( int? idCategory=null, string nameCategory = null)
        {
            var result = new List<CategoryModel>();
            int idUser = GetUserId();
            if (idCategory == null && nameCategory == null)
            {
              
                result = BlDtoesToModels(_categoryService.GetByUser(idUser));
            }
            else
            {
                if(idCategory != null)
                {
                     result.Add(BlDtoToModel(_categoryService.GetById(idUser, (int)idCategory)));
                }
                else
                {
                         result = BlDtoesToModels(_categoryService.GetByName(idUser, nameCategory));
                }
            }
            
            return result;

        }
        

        /// <summary>
        /// Create category
        /// </summary>
        /// <param name="model">model new category</param>
        /// <returns>http code</returns>
        public IHttpActionResult CreateCategory([FromBody]CategoryModel model)
        {
            model.IDUser= GetUserId();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _categoryService.Create(model);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Update category
        /// </summary>
        /// <param name="id">id category</param>
        /// <param name="model">update model category</param>
        /// <returns>http code</returns>
        [HttpPut]
        public IHttpActionResult EditCategory(int id, [FromBody]CategoryModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            model.IDUser = GetUserId();
            var result = _categoryService.Update(model);
            if (result)
                return Ok();
            return BadRequest();
        }
        
        /// <summary>
        /// Delete category
        /// </summary>
        /// <param name="id">id category</param>
        /// <returns>http code</returns>
        public IHttpActionResult Delete(int id)
        {
            var result = _categoryService.Delete(id);
            if (result)
                return Ok();
            return BadRequest();
        }

        #region DTO conversion

       

        /// <summary>
        /// convert bl dto to view model
        /// </summary>
        /// <param name="dto"> bl dto category</param>
        /// <returns>view model category</returns>
        private static CategoryModel BlDtoToModel(IBLCategoryDto dto)
        {
            return new CategoryModel
            {
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                IDUser = dto.IDUser
            };
        }

        /// <summary>
        /// convert list bl dto by list view model
        /// </summary>
        /// <param name="dtoes">list bl dto category</param>
        /// <returns>list view model category</returns>
        private static List<CategoryModel> BlDtoesToModels(IEnumerable<IBLCategoryDto> dtoes)
        {
            return dtoes.Select(BlDtoToModel).ToList();

        }

        #endregion

        private int GetUserId()
        {
           return UserManager.FindByName(Authentication.User.Identity.Name).Id;
        }
    }
}
