﻿using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// controler for render page
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// render page category
        /// </summary>
        /// <returns>view</returns>
        public ActionResult Category()
        {
            ViewBag.Title = "Category";

            return View();
        }

        /// <summary>
        /// render page task
        /// </summary>
        /// <param name="id">id category</param>
        /// <returns> view</returns>
        public ActionResult Task(int id)
        {
            ViewBag.Id = id;
            ViewBag.Title = "Task";

            return View();
        }

        /// <summary>
        /// render page user information
        /// </summary>
        /// <returns>page</returns>
        public ActionResult UserInformation()
        {
            ViewBag.UserId = User.Identity.GetUserId();
            ViewBag.Title = "User Information";

            return View();
        }

    }
}
