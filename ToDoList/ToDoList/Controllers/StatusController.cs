﻿using BL.Interface.IDTOs;
using BL.Interface.IServices;
using Microsoft.AspNet.Identity;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using TDL.WebApi.Models;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// Api controller for work with status
    /// </summary>
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [SwaggerResponse(HttpStatusCode.Unauthorized, "Need authorization")]
   
    public class StatusController : ApiController
    {
        private readonly IStatusService _statusService;

        /// <summary>
        /// constructor without param
        /// </summary>
        public StatusController()
        {
        }

        /// <summary>
        /// constructor for initializing ninject
        /// </summary>
        /// <param name="statusService">interface service status</param>
        public StatusController(IStatusService statusService)
        {

            _statusService = statusService;
        }

        /// <summary>
        /// Get all status
        /// </summary>
        /// <returns>list status</returns>
         [Route("api/Statuses/")]
        public IEnumerable<StatusModel> GetStatus()
        {
            var result = _statusService.GetAll();
            return BlDtoesToModels(result);

        }


        /// <summary>
        /// create new status
        /// </summary>
        /// <param name="model">model status</param>
        /// <returns>http code</returns>
        [Authorize(Roles = "Administrators")]
        public IHttpActionResult CreateStatus([FromBody]StatusModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _statusService.Create(model);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// get status by id
        /// </summary>
        /// <param name="id">id status</param>
        /// <returns>status view model</returns>
        public StatusModel GetStatus(int id)
        {

            var result = _statusService.GetById(id);
            return BlDtoToModel(result);

        }
        /// <summary>
        /// Update status
        /// </summary>
        /// <param name="id">id status</param>
        /// <param name="model">update model status</param>
        /// <returns>http code</returns>
        [HttpPut]
        [Authorize(Roles = "Administrators")]
        public IHttpActionResult EditStatus(int id, [FromBody]StatusModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _statusService.Update(model);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Delete status
        /// </summary>
        /// <param name="id">id status</param>
        /// <returns>http code</returns>
        [Authorize(Roles = "Administrators")]
        public IHttpActionResult Delete(int id)
        {
            var result = _statusService.Delete(id);
            if (result)
                return Ok();
            return BadRequest();
        }

        #region DTO conversion

        /// <summary>
        /// convert view model  to bl dto
        /// </summary>
        /// <param name="model">view model  status</param>
        /// <returns>bl dto status</returns>
        internal static IBLStatusDto ModelToBlDto(StatusModel model)
        {
            return new StatusModel
            {
                IDStatus = model.IDStatus,
                Name = model.Name
            };
        }

        /// <summary>
        /// convert bl dto to view model
        /// </summary>
        /// <param name="dto"> bl dto status</param>
        /// <returns>view model status</returns>
        internal static StatusModel BlDtoToModel(IBLStatusDto dto)
        {
            return new StatusModel
            {
                IDStatus = dto.IDStatus,
                Name = dto.Name
            };
        }

        /// <summary>
        /// convert list bl dto by list view model
        /// </summary>
        /// <param name="dtoes">list bl dto status</param>
        /// <returns>list view model status</returns>
        private static IEnumerable<StatusModel> BlDtoesToModels(IEnumerable<IBLStatusDto> dtoes)
        {
            return dtoes.Select(BlDtoToModel);

        }

        #endregion
    }
}
