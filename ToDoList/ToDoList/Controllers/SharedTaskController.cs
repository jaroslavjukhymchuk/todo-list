﻿using BL.Interface.IDTOs;
using BL.Interface.IServices;
using Microsoft.AspNet.Identity;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using TDL.WebApi.Models;
using ToDoList.Identity.Infrastructure;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// Api controller for work with shared task
    /// </summary>
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [SwaggerResponse(HttpStatusCode.Unauthorized, "Need authorization")]
    public class SharedTaskController : ApiController
    {
        private readonly ISharedTaskService _sharedTaskService;

        /// <summary>
        /// constructor without param
        /// </summary>
        public SharedTaskController()
        {
        }

        /// <summary>
        /// constructor for initializing ninject
        /// </summary>
        /// <param name="sharedTaskService">interface service sharedTask</param>
        public SharedTaskController(ISharedTaskService sharedTaskService)
        {

            _sharedTaskService = sharedTaskService;
        }

        /// <summary>
        /// Create new sharing
        /// </summary>
        /// <param name="model">model new sharing</param>
        /// <returns>http code</returns>
        [HttpPost]
        public IHttpActionResult CreateSharingTask([FromBody]SharedTaskModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _sharedTaskService.Create(model);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Get all user who can read task
        /// </summary>
        /// <param name="idTask">id task</param>
        /// <returns>list shared task view model</returns>
        [Route("api/SharedTasks/{idTask}")]
        public IEnumerable<SharedTaskModel> Get(int idTask)
        {
            var result = _sharedTaskService.GetUsers(idTask);
            return BlDtoesToModel(result);

        }

        /// <summary>
        /// Delete sharing task
        /// </summary>
        /// <param name="id">id sharing</param>
        /// <returns>http code</returns>
        public IHttpActionResult Delete(int id)
        {
            var result = _sharedTaskService.Delete(id);
            if (result)
                return Ok();
            return BadRequest();
        }

        #region DTO conversion
        
        
        private SharedTaskModel BlDtoToModel(IBLSharedTaskDto dto)
        {
            var user = UserManager.Users.SingleOrDefault(users => users.Id == dto.IDUser);
            return new SharedTaskModel
            {
                IDSharedTask = dto.IDSharedTask,
                IDTask = dto.IDTask,
                IDUser = dto.IDUser,
                User = new ShortUserModel
                {
                    IdUser = user.Id,
                    UserName = user.UserName
                }
            };
        }

        
        private IEnumerable<SharedTaskModel> BlDtoesToModel(IEnumerable<IBLSharedTaskDto> dtoes)
        {
            return dtoes.Select(BlDtoToModel);

        }


        #endregion

        private EPUserManager _userManager;

        /// <summary>
        /// initialization user manager
        /// </summary>
        public EPUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<EPUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}
