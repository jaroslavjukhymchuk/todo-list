﻿using BL.Interface.IDTOs;
using BL.Interface.IServices;
using Microsoft.AspNet.Identity;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using TDL.WebApi.Models;
using ToDoList.Identity.Infrastructure;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// Api controller for work with comment
    /// </summary>
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [SwaggerResponse(System.Net.HttpStatusCode.Unauthorized, "Need authorization")]
    public class CommentController : ApiController
    {
        private readonly ICommentService _commentService;

        /// <summary>
        /// constructor without param
        /// </summary>
        public CommentController()
        {
        }

        /// <summary>
        /// constructor for initializing ninject
        /// </summary>
        /// <param name="commentService">interface service comment</param>
        public CommentController(ICommentService commentService)
        {

            _commentService = commentService;
        }

        /// <summary>
        /// Get list comment by id task
        /// </summary>
        /// <param name="idTask">id task</param>
        /// <returns>list comment</returns>
        [Route("api/Comments/{idTask}")]
        public IEnumerable<CommentModel> Get(int idTask)
        {
            var result = _commentService.GetByTask(idTask);
            return BlDtoesToModels(result);

        }

        /// <summary>
        /// Create comment
        /// </summary>
        /// <param name="model">model comment</param>
        /// <returns>http code</returns>
        public IHttpActionResult CreateComment([FromBody]CommentModel model)
        {
            model.IDUser = GetUserId();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _commentService.Create(model);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// get comment by id
        /// </summary>
        /// <param name="id">id comment</param>
        /// <returns>comment view model</returns>
        public CommentModel GetComment(int id)
        {

            var result = _commentService.GetById(id);
            return BlDtoToModel(result);

        }
        
        /// <summary>
        /// update comment
        /// </summary>
        /// <param name="id">id comment</param>
        /// <param name="model">update model comment</param>
        /// <returns>http code</returns>
        [HttpPut]
        [Route("api/Comment/{id}")]
        public IHttpActionResult PutComment(int id, [FromBody]CommentModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _commentService.Update(model);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Delete comment
        /// </summary>
        /// <param name="id">id comment</param>
        /// <returns>http code</returns>
        [Route("api/Comment/{id}")]
        public IHttpActionResult Delete(int id)
        {
            var result = _commentService.Delete(id);
            if (result)
                return Ok();
            return BadRequest();
        }

        #region DTO conversion



        private CommentModel BlDtoToModel(IBLCommentDto dto)
        {
            var user = UserManager.Users.SingleOrDefault(users => users.Id == dto.IDUser);
            return new CommentModel
            {
                IDComment = dto.IDComment,
                IDTask = dto.IDTask,
                Description = dto.Description,
                DateCreation = dto.DateCreation,
                IDUser = dto.IDUser,
                User = new ShortUserModel
                {
                    IdUser = user.Id,
                    UserName = user.UserName
                } 
            };
        }

        private IEnumerable<CommentModel> BlDtoesToModels(IEnumerable<IBLCommentDto> dtoes)
        {
            return dtoes.Select(BlDtoToModel);

        }

        #endregion

        private EPUserManager _userManager;

        /// <summary>
        /// initialization user manager
        /// </summary>
        public EPUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<EPUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private int GetUserId()
        {
            return UserManager.FindByName(User.Identity.Name).Id;
        }

    }
}
