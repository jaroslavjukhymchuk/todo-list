﻿using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using ToDoList.Identity.Infrastructure;
using Swashbuckle.Swagger.Annotations;
using ToDoList.Identity.Models.Users;
using System.Web;
using TDL.WebApi.Results;
using System.Security.Claims;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using TDL.WebApi.Models;
using System;
using Newtonsoft.Json.Linq;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// controller for work with account user
    /// </summary>
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [SwaggerResponse(System.Net.HttpStatusCode.Unauthorized, "Need authorization")]
    [RoutePrefix("api/Auth")]
    public class OAuthController : ApiController
    {
        
        private EPUserManager _userManager;

        /// <summary>
        ///  constructor without param
        /// </summary>
        public OAuthController()
        {
        }

        /// <summary>
        /// constructor for initializing manager for work with user
        /// </summary>
        /// <param name="userManager">manager for work with user</param>
        /// <param name="accessTokenFormat">the data format used to protect the information contained in the access token</param>
        public OAuthController(EPUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        /// <summary>
        /// manager for work with user
        /// </summary>
        public EPUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<EPUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// initialization the data format used to protect the information contained in the access token
        /// </summary>
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        /// <summary>
        /// action for log out user
        /// </summary>
        /// <returns>http code</returns>
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        /// <summary>
        /// action for register user
        /// </summary>
        /// <param name="model">model new user</param>
        /// <returns>http code</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Register")]
        public IHttpActionResult Register(RegisterModel model)
        {
            
            var user = new User() { UserName = model.Login, Email = model.Email  };

            var result =  UserManager.Create(user, model.Password);
            
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    HttpContext.Current.Response.Headers.Add("ValidationErrors", error);
                }
                
                return GetErrorResult(result);
            }
            user = UserManager.FindByName(model.Login);
            UserManager.AddToRole(user.Id, "User");

            return Ok();
        }
        
        /// <summary>
        /// dispose user manager
        /// </summary>
        /// <param name="disposing">true or false</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (result.Succeeded) return null;
            if (result.Errors != null)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            if (ModelState.IsValid)
            {
                return BadRequest();
            }

            return BadRequest(ModelState);

        }

        #endregion

        /// <summary>
        /// External Login or register
        /// </summary>
        /// <param name="model">external login model</param>
        /// <returns>dictionary with tiken</returns>
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        [HttpPost]
        public JObject GetExternalLogin(ExternalLoginDataModel model)
        {
            User user = UserManager.FindByEmail(model.Email);
            bool hasRegistered = user == null;

            if (hasRegistered)
            {
             user = RegisterExternal(model);
            }
          return  GenerateLocalAccessTokenResponse(user.UserName);
           
        }

        private JObject GenerateLocalAccessTokenResponse(string userName)
        {
            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, userName));
            identity.AddClaim(new Claim("role", "user"));

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

            JObject tokenResponse = new JObject(
                                        new JProperty("userName", userName),
                                        new JProperty("access_token", accessToken),
                                        new JProperty("token_type", "bearer"),
                                        new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
                                        new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                                        new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
        );

            return tokenResponse;
        }

        
        private User RegisterExternal(ExternalLoginDataModel model)
        {
           var  user = new User() { UserName = model.DefaultUserName, Email = model.Email };
            var result = UserManager.Create(user);
            result =  UserManager.AddLoginAsync(user.Id, new UserLoginInfo(model.LoginProvider, model.ProviderKey)).Result;
            return UserManager.FindByEmail(user.Email);
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        /// <summary>
        /// get information about external logins
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="generateState"></param>
        /// <returns>information about external logins</returns>
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }
    }
}
