﻿using System.Web.Mvc;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// render view account
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// render view registration
        /// </summary>
        /// <returns>view</returns>
        public ActionResult Register()
        {
            return View();
        }

    }
}
