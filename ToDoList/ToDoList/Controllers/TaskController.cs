﻿using BL.Interface.IDTOs.IBLTaskDtoes;
using BL.Interface.IServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TDL.WebApi.Models;
using TDL.WebApi.Models.TaskModels;
using ToDoList.Identity.Infrastructure;

namespace TDL.WebApi.Controllers
{
    /// <summary>
    /// Api controller for work with task
    /// </summary>
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [SwaggerResponse(System.Net.HttpStatusCode.Unauthorized, "Need authorization")]
    public class TaskController : ApiController
    {
        private readonly ITaskService _taskService;

        /// <summary>
        /// constructor without param
        /// </summary>
        public TaskController()
        {
        }

        /// <summary>
        /// constructor for initializing ninject
        /// </summary>
        /// <param name="taskService">interface service task</param>
        public TaskController(ITaskService taskService)
        {

            _taskService = taskService;
        }

        /// <summary>
        /// Get tasks
        /// </summary>
        /// <param name="idCategory">id category</param>
        /// <param name="idTask">id task </param>
        /// <param name="nameTask">name task for search</param>
        /// <param name="pagingParameterModel">number page and count elements</param>
        /// <returns>list task and paging parameter</returns>
        [Route("api/Tasks")]
        public IEnumerable<TaskModel> Get(int idCategory, [FromUri]PagingParameterModel pagingParameterModel, int? idTask = null, string nameTask = null)
        {
            var returnValue = new List<TaskModel>();
            if (idTask == null && nameTask == null)
            {
                var result = BlListDtoToListModel(_taskService.GetByCategory(idCategory, pagingParameterModel.PageNumber - 1, pagingParameterModel.PageSize));

                returnValue = ShortModelsToModels(result.Tasks);

                var currentPage = pagingParameterModel.PageNumber;

                var pageSize = pagingParameterModel.PageSize;

                var totalPages = result.TotalPages;

                var previousPage = currentPage > 1 ? "Yes" : "No";

                var nextPage = currentPage < totalPages ? "Yes" : "No";

                var paginationMetadata = new
                {
                    pageSize,
                    currentPage,
                    totalPages,
                    previousPage,
                    nextPage
                };
                HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            }
            else
            {
                if (idTask != null)
                {
                    returnValue.Add(GetTask((int)idTask));
                }
                else
                {
                        returnValue = ShortModelsToModels(SearchTask(idCategory, nameTask));
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Get task by id
        /// </summary>
        /// <param name="id">id task</param>
        /// <returns>task model</returns>
        private TaskModel GetTask(int id)
        {
            var result = _taskService.GetById(id);
            return BlDtoToModel(result);

        }

        /// <summary>
        /// Get all task what can read user
        /// </summary>
        /// <param name="pagingParameterModel">number page and count elements</param>
        /// <returns>list task</returns>
        [Route("api/Tasks/User")]
        public IEnumerable<ShortTaskModel> GetByUser( [FromUri]PagingParameterModel pagingParameterModel)
        {
            var idUser = GetUserId();
             var result = BlListDtoToListModel(_taskService.GetByUserId(idUser, pagingParameterModel.PageNumber - 1, pagingParameterModel.PageSize));

            var currentPage = pagingParameterModel.PageNumber;

            var pageSize = pagingParameterModel.PageSize;

            var totalPages = result.TotalPages;

            var previousPage = currentPage > 1 ? "Yes" : "No";

            var nextPage = currentPage < totalPages ? "Yes" : "No";

            var paginationMetadata = new
            {
                pageSize,
                currentPage,
                totalPages,
                previousPage,
                nextPage
            };
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            return result.Tasks;
        }

        /// <summary>
        /// Update task
        /// </summary>
        /// <param name="id">id task</param>
        /// <param name="model">model update task</param>
        /// <returns>http code</returns>
        [HttpPut]
        public IHttpActionResult EditTask(int id, [FromBody]CreateTaskModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _taskService.Update(ModelToBlDto(model));
           if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Delete task
        /// </summary>
        /// <param name="id">id task</param>
        /// <returns>http code</returns>
        public IHttpActionResult Delete(int id)
        {
            var result = _taskService.Delete(id);
            if (result)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Search task by name
        /// </summary>
        /// <param name="name">name task</param>
        /// <param name="idCategory">id category</param>
        /// <returns>list task</returns>
        private IEnumerable<ShortTaskModel> SearchTask(int idCategory, string name)
        {
            var result = _taskService.GetByName(idCategory, name);
            return BlDtoesToModels(result);

        }

        /// <summary>
        /// Create task
        /// </summary>
        /// <param name="model">model new task</param>
        /// <returns>http code</returns>
        public IHttpActionResult CreateTask([FromBody]CreateTaskModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _taskService.Create(ModelToBlDto(model));
            if (result)
                return Ok();
            return BadRequest();
        }

        #region DTO conversion

        /// <summary>
        /// convert view model dto to bl dto
        /// </summary>
        /// <param name="model">view model test</param>
        /// <returns>bl dto test</returns>
        private static IBLTaskDto ModelToBlDto(CreateTaskModel model)
        {
            return new TaskModel
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                Status = StatusController.BlDtoToModel(model.Status),
                DateCreation = model.DateCreation,
                DateFinish = model.DateFinish
            };
        }

        /// <summary>
        /// convert bl dto to view model
        /// </summary>
        /// <param name="dto"> bl dto test</param>
        /// <returns>view model test</returns>
        private static TaskModel BlDtoToModel(IBLTaskDto dto)
        {
            return new TaskModel
            {
                IDTask = dto.IDTask,
                IDCategory = dto.IDCategory,
                Name = dto.Name,
                Description = dto.Description,
                Priority = dto.Priority,
                Status = StatusController.BlDtoToModel(dto.Status),
                DateCreation = dto.DateCreation,
                DateFinish = dto.DateFinish
            };
        }

        /// <summary>
        /// convert bl short dto in short form view model
        /// </summary>
        /// <param name="model">bl short dto task</param>
        /// <returns>view model short task</returns>
        private static ShortTaskModel BlDtoToShortModel(IBLShortTaskDto model)
        {
            return new ShortTaskModel
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Priority = model.Priority,
                DateFinish = model.DateFinish,
                UserName = model.UserName
            };
        }

        /// <summary>
        /// convert list bl short dto by short form list view model
        /// </summary>
        /// <param name="dtoes">list bl short dto task</param>
        /// <returns>short form list view model task</returns>
        private static ListShortTaskModel BlListDtoToListModel(IBLListShortTaskDto dtoes)
        {
            return new ListShortTaskModel
            {
                TotalPages = dtoes.TotalPages,
                Tasks = dtoes.Tasks.Select(BlDtoToShortModel)
            };

        }
        

        /// <summary>
        /// convert list bl dto by list view model
        /// </summary>
        /// <param name="dto">list bl dto test</param>
        /// <returns>list view model test</returns>
        private static IEnumerable<ShortTaskModel> BlDtoesToModels(IEnumerable<IBLShortTaskDto> dto)
        {
            return dto.Select(BlDtoToShortModel);

        }

        private static List<TaskModel> ShortModelsToModels (IEnumerable<ShortTaskModel> models)
        {
            return models.Select(ShortModelToModel).ToList();
        }

        private static TaskModel ShortModelToModel(ShortTaskModel model)
        {
            return new TaskModel
            {
                IDTask = model.IDTask,
                IDCategory = model.IDCategory,
                Name = model.Name,
                Priority = model.Priority,
                DateFinish = model.DateFinish
            };
        }

        #endregion

        private EPUserManager _userManager;

        /// <summary>
        /// initialization user manager
        /// </summary>
        public EPUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<EPUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private int GetUserId()
        {
            return UserManager.FindByName(User.Identity.Name).Id;
        }

    }
}
