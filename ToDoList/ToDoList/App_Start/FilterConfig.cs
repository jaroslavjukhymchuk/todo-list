﻿using System.Web;
using System.Web.Mvc;

namespace TDL.WebApi
{
    /// <summary>
    /// class filter config
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// register global filters in project
        /// </summary>
        /// <param name="filters">global filter collection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
