﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using TDL.WebApi.Providers;
using ToDoList.Identity.Infrastructure;

namespace TDL.WebApi
{
    /// <summary>
    /// The Startup class configures the services and application request conveyor.
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// Configuration options for OAuthMiddleware.
        /// </summary>
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        /// <summary>
        /// client id
        /// </summary>
        public static string PublicClientId { get; private set; }
        
        /// <summary>
        /// configure auth
        /// </summary>
        /// <param name="app">app builder</param>
        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(AppIdentityDbContext.ContextCreate);
            app.CreatePerOwinContext<EPUserManager>(EPUserManager.Create);
            
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            

            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true,
            };
            
            app.UseOAuthBearerTokens(OAuthOptions);
           

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "789901147987-ad5oaj6talu9c1vna7upc1bf0ukqnu79.apps.googleusercontent.com",
                ClientSecret = "uBDtNpB-Ck7yaT-zpe271Dfl",
                
            });

            app.UseFacebookAuthentication(new Microsoft.Owin.Security.Facebook.FacebookAuthenticationOptions()
            {
                AppId = "183965775740001",
                AppSecret = "ac83b78405ffea90e47d6e43734a231d"
            });

            app.UseMicrosoftAccountAuthentication(new Microsoft.Owin.Security.MicrosoftAccount.MicrosoftAccountAuthenticationOptions()
            {
                ClientId = "721d2046-a96d-44e1-a768-8fea89c66893",
                ClientSecret = "fecYNMH0127)baneHLE6*%:"
            });

            app.UseTwitterAuthentication( new Microsoft.Owin.Security.Twitter.TwitterAuthenticationOptions()
            {
                ConsumerKey = "voJMGOENp7wYjh8EqpLOtMCH5",
                ConsumerSecret = "q7dFIcNYmpuTX0W7Q1ir00IQt5mPewaFA01U6kEbJUKcmY1e6z"
            });
        }
    }
}
