﻿using System.Web;
using System.Web.Optimization;

namespace TDL.WebApi
{
    /// <summary>
    /// class bundle config
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// register bundles in project
        /// </summary>
        /// <param name="bundles">bundle collection</param>
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/knockout-3.4.2.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/script/Category/js").Include(
                        "~/Scripts/Category/GetCategory.*",
                        "~/Scripts/Category/WriteResponse.*",
                        "~/Scripts/Category/GetAllCategories.*",
                        "~/Scripts/Category/DeleteCategory.*",
                        "~/Scripts/Category/AddCategory.*",
                        "~/Scripts/Category/EditCategory.*",
                        "~/Scripts/Category/SearchCategory.*"));

            bundles.Add(new ScriptBundle("~/script/Task/js").Include(
                        "~/Scripts/Task/AddTask.*",
                        "~/Scripts/Task/DeleteTask.*",
                        "~/Scripts/Task/EditTask.*",
                        "~/Scripts/Task/GetAllTask.*",
                        "~/Scripts/Task/GetStatus.*",
                        "~/Scripts/Task/GetTask.*",
                        "~/Scripts/Task/SearchTask.*",
                        "~/Scripts/Task/SharingTask.*",
                        "~/Scripts/Task/ShowTask.*",
                        "~/Scripts/Task/WriteResponse.*",
                        "~/Scripts/Task/AddSharingTask.*",
                        "~/Scripts/Task/GetSharedTask.*",
                        "~/Scripts/Task/DetailsTask.*",
                        "~/Scripts/Task/TaskPagination.*"));

            bundles.Add(new ScriptBundle("~/script/Comment/js").Include(
                "~/Scripts/Comment/GetAllComment.*",
                "~/Scripts/Comment/WriteComments.*",
                "~/Scripts/Comment/DeleteComment.*",
                "~/Scripts/Comment/AddComment.*"
                ));

            bundles.Add(new ScriptBundle("~/script/User/js").Include(
                "~/Scripts/User/GetUserInformation.*",
                "~/Scripts/User/WriteUserInformation.*",
                "~/Scripts/User/GetUser.*",
                "~/Scripts/User/EditUserInformation.*",
                "~/Scripts/User/DeleteUser.*"
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/account.js",
                      "~/Scripts/jquery.datetimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/font-awesome.css",
                     "~/Content/site.css",
                     "~/Contetnt/jquery.datetimepicker.css"));
        }
    }
}
