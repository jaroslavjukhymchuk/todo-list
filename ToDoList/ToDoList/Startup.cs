﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TDL.WebApi.Startup))]

namespace TDL.WebApi
{
    /// <summary>
    /// class owin startup
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// configuration owin
        /// </summary>
        /// <param name="app">app builder</param>
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
