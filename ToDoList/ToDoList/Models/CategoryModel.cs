﻿using BL.Interface.IDTOs;
using System.ComponentModel.DataAnnotations;

namespace TDL.WebApi.Models
{
    /// <summary>
    /// view model category
    /// </summary>
    public class CategoryModel : IBLCategoryDto
    {
        /// <summary>
        /// id category
        /// </summary>
        public int IDCategory { get; set; }

        /// <summary>
        /// id user who create category
        /// </summary>
        public int IDUser { get; set; }

        /// <summary>
        /// name category
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}