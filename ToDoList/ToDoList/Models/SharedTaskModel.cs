﻿using BL.Interface.IDTOs;

namespace TDL.WebApi.Models
{
    /// <summary>
    /// shared task view model
    /// </summary>
    public class SharedTaskModel : IBLSharedTaskDto
    {
        /// <summary>
        /// id shared task
        /// </summary>
        public int IDSharedTask { get; set; }

        /// <summary>
        /// id task what shared
        /// </summary>
        public int IDTask { get; set; }

        /// <summary>
        /// id user who can read task
        /// </summary>
        public int IDUser { get; set; }

        /// <summary>
        ///  user who can read task
        /// </summary>
        public ShortUserModel User { get; set; }
    }
}