﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDL.WebApi.Models
{
    public class ExternalLoginDataModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

        public string DefaultUserName { get; set; }

        public string Email { get; set; }
    }
}