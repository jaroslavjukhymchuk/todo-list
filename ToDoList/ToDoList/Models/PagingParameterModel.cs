﻿namespace TDL.WebApi.Models
{
    /// <summary>
    /// model for pagination
    /// </summary>
    public class PagingParameterModel
    {
        private const int MaxPageSize = 20;

        /// <summary>
        /// number page
        /// </summary>
        public int PageNumber { get; set; } = 1;

        private int _pageSize { get; set; } = 10;

        /// <summary>
        /// how many elements in page
        /// </summary>
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
            }
        }
    }
}