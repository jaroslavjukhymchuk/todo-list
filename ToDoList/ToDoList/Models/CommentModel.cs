﻿using BL.Interface.IDTOs;
using System;
using System.Collections.Generic;

namespace TDL.WebApi.Models
{
    /// <summary>
    /// comment view model
    /// </summary>
    public class CommentModel : IBLCommentDto
    {
        /// <summary>
        /// id comment
        /// </summary>
        public int IDComment { get; set; }

        /// <summary>
        /// id task parent comment
        /// </summary>
        public int IDTask { get; set; }

        /// <summary>
        /// text comment
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// date when create comment
        /// </summary>
        public DateTime DateCreation { get; set; }

        /// <summary>
        /// id user who create comment
        /// </summary>
        public int IDUser { get; set; }

        /// <summary>
        /// user who create comment
        /// </summary>
        public ShortUserModel User { get; set; }

    }
}