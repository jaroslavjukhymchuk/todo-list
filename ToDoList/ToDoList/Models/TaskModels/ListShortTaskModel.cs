﻿using System.Collections.Generic;

namespace TDL.WebApi.Models.TaskModels
{
    /// <summary>
    /// list short task view model
    /// </summary>
    public class ListShortTaskModel
    {
        /// <summary>
        /// page number
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// total pages number
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// list short task
        /// </summary>
        public IEnumerable<ShortTaskModel> Tasks { get; set; }
    }
}