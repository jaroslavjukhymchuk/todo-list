﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDL.WebApi.Models.TaskModels
{
    /// <summary>
    /// create task view model
    /// </summary>
    public class CreateTaskModel
    {
        /// <summary>
        /// id task
        /// </summary>
        public int IDTask { get; set; }
      
        /// <summary>
        /// id category parent task
        /// </summary>
        public int IDCategory { get; set; }

        /// <summary>
        /// name task
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// description task
        /// </summary>
        [StringLength(50)]
        public string Description { get; set; }

        /// <summary>
        /// priority task
        /// </summary>
        [Range(1, 10)]
        public int Priority { get; set; }

        /// <summary>
        /// date creation task
        /// </summary>
        public DateTime DateCreation { get; set; }

        /// <summary>
        /// date when need finish task
        /// </summary>
        public DateTime? DateFinish { get; set; }

        /// <summary>
        /// status task
        /// </summary>
        public StatusModel Status { get; set; }
        
    }
}