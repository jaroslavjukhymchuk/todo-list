﻿using BL.Interface.IDTOs.IBLTaskDtoes;
using System;
using BL.Interface.IDTOs;
using System.ComponentModel.DataAnnotations;

namespace TDL.WebApi.Models.TaskModels
{
    /// <summary>
    /// task view model
    /// </summary>
    public class TaskModel : IBLTaskDto
    {
        /// <summary>
        /// id task
        /// </summary>
        public int IDTask { get; set; }

        /// <summary>
        /// id parent category
        /// </summary>
        public int IDCategory { get; set; }

        /// <summary>
        /// name task
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// description task
        /// </summary>
        [StringLength(50)]
        public string Description { get; set; }
        
        /// <summary>
        /// prioriry task
        /// </summary>
        [Range(1, 10)]
        public int Priority { get; set; }

        /// <summary>
        /// date creation task
        /// </summary>
        public DateTime DateCreation { get; set; }

        /// <summary>
        /// date when need finish task
        /// </summary>
        public DateTime? DateFinish { get; set; }

        /// <summary>
        /// status task
        /// </summary>
        public IBLStatusDto Status { get; set; }
    }
}