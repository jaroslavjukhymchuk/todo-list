﻿using System;

namespace TDL.WebApi.Models.TaskModels
{
    /// <summary>
    /// short task view model
    /// </summary>
    public class ShortTaskModel
    {
        /// <summary>
        /// id task
        /// </summary>
        public int IDTask { get; set; }

        /// <summary>
        /// id parent category
        /// </summary>
        public int IDCategory { get; set; }

        /// <summary>
        /// name task
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// priority task
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// date when need finish task
        /// </summary>
        public DateTime? DateFinish { get; set; }

        /// <summary>
        /// user name
        /// </summary>
        public string UserName { get; set; }
    }
}