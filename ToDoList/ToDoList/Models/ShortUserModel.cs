﻿namespace TDL.WebApi.Models
{
    /// <summary>
    /// short user model
    /// </summary>
    public class ShortUserModel
    {
        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// id User
        /// </summary>
        public int IdUser { get; set; }
    }
}