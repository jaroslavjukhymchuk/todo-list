﻿using BL.Interface.IDTOs;

namespace TDL.WebApi.Models
{
    /// <summary>
    /// status view model
    /// </summary>
    public class StatusModel : IBLStatusDto
    {
        /// <summary>
        /// id status
        /// </summary>
        public int IDStatus { get; set; }

        /// <summary>
        /// name status
        /// </summary>
        public string Name { get; set; }
    }
}