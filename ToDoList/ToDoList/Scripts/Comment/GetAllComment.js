﻿$(document).ready(function () {
    $("#comment").click(function (event) {
        if ($(this).hasClass("closeComment")) {
            $("#arrow").toggleClass("glyphicon-arrow-down");
            $("#arrow").toggleClass("glyphicon-arrow-up");
            $("#commentBlock").html("");
            $(this).toggleClass('closeComment');
        } else {
            $("#arrow").toggleClass("glyphicon-arrow-down");
            $("#arrow").toggleClass("glyphicon-arrow-up");
            $(this).toggleClass('closeComment');
            GetAllComment($('#detailsId').text());
        }
    });
});
function GetAllComment(idTask) {
    $.ajax({
        url: '/api/Comments/' + idTask,
        type: 'GET',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $.each(data, function (key, value) {
                $.ajax({
                    url: '/api/User/' + value.IDUser,
                    type: 'GET',
                    beforeSend: function (xhr) {

                        var token = sessionStorage.getItem(tokenKey);
                        xhr.setRequestHeader("Authorization", "Bearer " + token);
                    },
                    success: function (users) {
                        if ($("#arrow").hasClass("glyphicon-arrow-down")) {
                            $("#arrow").toggleClass("glyphicon-arrow-down");
                            $("#arrow").toggleClass("glyphicon-arrow-up");
                            $("#comment").toggleClass('closeComment');
                        }
                        value.IDUser = users.UserName
                        WriteComments(data);

                    },
                    error: function (x, y, z) {
                        alert(x + '\n' + y + '\n' + z);
                    }
                });
            });
            
            },
                
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}