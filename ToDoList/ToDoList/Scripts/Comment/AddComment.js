﻿$(document).ready(function () {
    $("#addComment").click(function (event) {
        event.preventDefault();
        AddComment();
    });

});
function AddComment() {
    var comment = {
        Description: $('#newComment').val(),
        IDTask: $('#detailsId').text(),
        DateCreation: new Date()
    };

    $.ajax({
        url: '/api/Comment',
        type: 'POST',
        data: JSON.stringify(comment),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $('#errors').empty();
            $("#newComment").val("");
            GetAllComment($('#detailsId').text());
        },
        error: (function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['Message'] + "</h2>");
            if (response['ModelState']['model.Description']) {
                $.each(response['ModelState']['model.Description'], function (index, item) {
                    $('#errors').append("<p>" + item + "</p>");
                });
            }
        })
    });
}