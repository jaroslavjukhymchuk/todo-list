﻿function DeleteComments(el) {
    var id = $(el).attr('data-item');
    DeleteComment(id);
}
function DeleteComment(id) {
    $.ajax({
        url: '/api/Comment/' + id,
        type: 'DELETE',
        contentType: "application/json;charset=utf-8",
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            GetAllComment();
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}