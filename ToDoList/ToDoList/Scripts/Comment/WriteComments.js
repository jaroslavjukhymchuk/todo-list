﻿function WriteComments(comments) {
    if (comments.length == 0) {
        $("#tableBlock").html("<h1 style='color:red;'>Tasks not found</h1>");
    }
    else {
        var strResult = "<table class='table table-hover'><th>Description</th><th>Date creation</th><th>User</th><th></th>";
        $.each(comments, function (index, comment) {

            strResult += "<tr><td>" + comment.Description + "</td><td>" + comment.DateCreation.substring(0, comment.DateCreation.indexOf("T")) + "</td><td>" + comment.IDUser + "</td><td><a id='delItem' data-item='" + comment.IDComment + "' onclick='DeleteComments(this);' ><span class='glyphicon glyphicon-remove'></span></a></td></tr>";
        });
        strResult += "</table>";
        $("#commentBlock").html(strResult);
    }
}