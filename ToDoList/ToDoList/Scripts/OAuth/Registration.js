﻿$(document).ready(function () {
    $('#formLogin').click(function (e) {
        $('.registration').css('display', 'none');
        $('.login').css('display', 'block');
        $('#errors').empty();
    });
    $('#formRegister').click(function (e) {
        $('.registration').css('display', 'block');
        $('.login').css('display', 'none');
        $('#errors').empty();
    });
    $('#submit').click(function (e) {
        e.preventDefault();
        var data = {
            Login: $('#login').val(),
            Email: $('#email').val(),
            Password: $('#password').val(),
            ConfirmPassword: $('#confirm').val()
        };

        $.ajax({
            type: 'POST',
            url: '/api/OAuth/Register',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data)
        }).success(function (data) {
            $('.registration').css('display', 'none');
            $('.login').css('display', 'block');
        }).fail(function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            if (response['ModelState']['model.Login']) {

                $.each(response['ModelState']['model.Login'], function (index, item) {
                    $('#errors').append("<p>" + item + "</p>");
                });
            }
            if (response['ModelState']['model.Email']) {
                $.each(response['ModelState']['model.Email'], function (index, item) {
                    $('#errors').append("<p>" + item + "</p>");
                });
            }
            if (response['ModelState']['model.Password']) {
                $.each(response['ModelState']['model.Password'], function (index, item) {
                    $('#errors').append("<p>" + item + "</p>");
                });
            }
            if (response['ModelState']['']) {
                $.each(response['ModelState'][''], function (index, item) {
                    $('#errors').append("<p>" + item + "</p>");
                });
            }
        });
    });
});