﻿$(document).ready(function () {
    $('#submitLogin').click(function (e) {
        e.preventDefault();
        var loginData = {
            grant_type: 'password',
            username: $('#loglogin').val(),
            password: $('#logpassword').val()
        };

        $.ajax({
            type: 'POST',
            url: '/Token',
            data: loginData
        }).success(function (data) {
            sessionStorage.setItem(tokenKey, data.access_token);
            console.log(data.access_token);
            $(location).attr('href', url);
        }).fail(function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['error_description'] + "</h2>");
        });
    });
});