﻿$(document).ready(function () {
    $("#information").click(function (event) {
        event.preventDefault();
        GetUser();
    });
});


function GetUser(id) {
    $.ajax({
        url: '/api/User/' + id,
        type: 'GET',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            WriteUser(data);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
function WriteUser(user) {
    $("#IdUser").val(user.Id);
    $("#editEmail").val(user.Email);
    $("#editPhone").val(user.PhoneNumber);
    $("#editName").val(user.UserName);
}