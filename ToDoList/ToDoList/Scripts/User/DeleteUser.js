﻿$(document).ready(function () {
    $("#deleteUser").click(function (event) {
        event.preventDefault();
        DeleteUser(UserID);
    });

});
function DeleteUser(id) {
    $.ajax({
        url: '/api/User/' + id,
        type: 'DELETE',
        contentType: "application/json;charset=utf-8",
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $('#errors').empty();
            $(location).attr('href', url);
        },
        error: function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['Message'] + "</h2>");
        }
    });
}