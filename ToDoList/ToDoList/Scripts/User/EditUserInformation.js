﻿$(document).ready(function () {
    $("#editUser").click(function (event) {
        event.preventDefault();
        EditUser();
    });

});
function EditUser() {
    var id = $('#IdUser').val()
    var user = {
        Id: id,
        Email: $('#editEmail').val(),
        UserName: $('#editName').val(),
        PhoneNumber: $('#editPhone').val(),
    };
    $.ajax({
        url: '/api/User/' + id,
        type: 'PUT',
        data: JSON.stringify(user),
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            GetUserInformation(id);
            $('#errors').empty();
        },
        error: function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['Message'] + "</h2>");
            if (response['ModelState']['model.Name']) {
                $.each(response['ModelState']['model.Name'], function (index, item) {
                    $('#errors').append("<p>" + item + "</p>");
                });
            }
        }
    });
}
function EditItem(el) {
    var id = $(el).attr('data-item');
    GetUser(id);
}