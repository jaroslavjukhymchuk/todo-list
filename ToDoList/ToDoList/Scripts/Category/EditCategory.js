﻿$(document).ready(function () {
    $("#editCategory").click(function (event) {
        event.preventDefault();
        EditCategory();
    });

});
function EditCategory() {
    var id = $('#editId').val()
    var category = {
        IDCategory: $('#editId').val(),
        Name: $('#editName').val()
};
$.ajax({
    url: '/api/Category/' + id,
    type: 'PUT',
    data: JSON.stringify(category),
    beforeSend: function (xhr) {
        var token = sessionStorage.getItem(tokenKey);
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    },
    contentType: "application/json;charset=utf-8",
    success: function (data) {
        GetAllCategories();
$('#errors').empty();
                },
error: function (jxqr, error, status) {
    var response = jQuery.parseJSON(jxqr.responseText);
    $('#errors').empty();
    $('#errors').append("<h2>" + response['Message'] + "</h2>");
    if (response['ModelState']['model.Name']) {
        $.each(response['ModelState']['model.Name'], function (index, item) {
            $('#errors').append("<p>" + item + "</p>");
        });
    }
}
            });
}
function EditItem(el) {
    var id = $(el).attr('data-item');
    GetCategory(id);
}