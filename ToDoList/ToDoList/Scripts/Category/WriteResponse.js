﻿function WriteResponse(categories) {
    if (categories.length == 0) {
        $("#tableBlock").html("<h1 style='color:red;'>Category not found</h1>")
    }
    else {
        var strResult = "<table class='table table-hover'><th>ID</th><th>Name</th><th></th><th></th><th>";
        $.each(categories, function (index, category) {
            var ids = category.IDCategory;
            strResult += "<tr><td>" + category.IDCategory + "</td><td> <a href="+url+"/" + category.IDCategory + ">" + category.Name + "</td> <td><a data-toggle='modal' data-target='#myModal' data-item='" + category.IDCategory + "' onclick='EditItem(this);' ><span class='glyphicon glyphicon-pencil'></span></a></td><td><a id='delItem' data-item='" + category.IDCategory + "' onclick='DeleteItem(this);' ><span class='glyphicon glyphicon-remove'></span></a></td></tr>";
        });
        strResult += "</table>";
        $("#tableBlock").html(strResult);
    }
}