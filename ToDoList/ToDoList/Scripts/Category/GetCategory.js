﻿function ShowCategory(category) {
    $("#editId").val(category[0].IDCategory);
    $("#editName").val(category[0].Name)

}
function GetCategory(id) {
    $.ajax({
        url: '/api/Categories?idCategory=' + id,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            ShowCategory(data);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}