﻿
function DeleteCategory(id) {
    $.ajax({
        url: '/api/Category/' + id,
        type: 'DELETE',
        contentType: "application/json;charset=utf-8",
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $('#errors').empty();
            GetAllCategories();
},
error: function (jxqr, error, status) {
    var response = jQuery.parseJSON(jxqr.responseText);
    $('#errors').empty();
    $('#errors').append("<h2>" + response['Message'] + "</h2>");
}
            });
}
function DeleteItem(el) {
    var id = $(el).attr('data-item');
    DeleteCategory(id);
}