﻿$(document).ready(function () {
    $("#addCategory").click(function (event) {
        event.preventDefault();
        AddCategory();
    });

});
function AddCategory() {
    var category = {
        Name: $('#addName').val()
};

$.ajax({
    url: '/api/Category',
    type: 'POST',
    data: JSON.stringify(category),
    contentType: "application/json;charset=utf-8",
    beforeSend: function (xhr) {
        var token = sessionStorage.getItem(tokenKey);
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    },
    success: function (data) {
        $('#errors').empty();
        $("#addName").val("");
        GetAllCategories();
                },
error: (function (jxqr, error, status) {
    var response = jQuery.parseJSON(jxqr.responseText);
    $('#errors').empty();
    $('#errors').append("<h2>" + response['Message'] + "</h2>");
    if (response['ModelState']['model.Name']) {
        $.each(response['ModelState']['model.Name'], function (index, item) {
            $('#errors').append("<p>" + item + "</p>");
        });
    }
})
            });
        }