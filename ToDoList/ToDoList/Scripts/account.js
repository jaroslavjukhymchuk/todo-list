﻿$(function () {
    var tokenKey = "tokenInfo";
    $('#logOut').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/api/Auth/Logout',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (xhr) {

                var token = sessionStorage.getItem(tokenKey);
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            },
        }).success(function (data) {
            var url = "/";
            sessionStorage.removeItem(tokenKey);
            $(location).attr('href', url);
        }).fail(function (data) {
            alert("Error");
        });
    });
});