﻿$(document).ready(function () {
    $("#editTask").click(function (event) {
        event.preventDefault();
        $('#errors').empty();
        EditTask();
    });
});
function EditItem(el) {
    $('#errors').empty();
    var id = $(el).attr('data-item');
    GetTask(id);
}
function EditTask() {
    var id = $('#editId').val()
    var Task = {
        IDTask: $('#editId').val(),
        IDCategory: $('#editIdCategory').val(),
        DateCreation: $('#editCreate').val(),
        Description: $('#editDescription').val(),
        Priority: $('#editPriority').val(),
        DateFinish: $('#editDate').val(),
        Name: $('#editName').val(),
        Status: {
            IDStatus: $("#my_select :selected").val(),
            Name: $("#my_select :selected").text()
        },
    };
    if (Task.Priority == "")
        Task.Priority = 0;
    $.ajax({
        url: '/api/Task/' + id,
        type: 'PUT',
        data: JSON.stringify(Task),
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#errors').empty();
            GetAllTask(CategoryID, startPage, howGive);
},
error: function (jxqr, error, status) {
    var response = jQuery.parseJSON(jxqr.responseText);
    $('#errors').empty();
    if (response['ModelState']['model.Name']) {

        $.each(response['ModelState']['model.Name'], function (index, item) {
            $('#errors').append("<p>" + item + "</p>");
        });
    }
    if (response['ModelState']['model.Priority']) {

        $.each(response['ModelState']['model.Priority'], function (index, item) {
            $('#errors').append("<p>" + item + "</p>");
        });
    }
}
            });
        }