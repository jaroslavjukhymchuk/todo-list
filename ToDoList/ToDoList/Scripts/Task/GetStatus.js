﻿$(document).ready(function () {
    $("#newTask").click(function (event) {
        event.preventDefault();
        GetStatus();
    });
});
function GetStatus() {
    $.ajax({
        url: '/api/Statuses',
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (status) {
            $("#newMy_select").find('option').remove();
            $.each(status, function (key, value) {
                $("#newMy_select").append($('<option value="' + value.IDStatus + '">' + value.Name + '</option>'));

            });
            var dateControl = document.querySelector('input[id="newDate"]');
            var mounth = new Date().getMonth() + 1;
            if (mounth < 10) {
                mounth = "0" + mounth;
            }
            var day = new Date().getDate();
            if (day < 10) {
                day = "0" + day;
            }
            dateControl.setAttribute("min", new Date().getFullYear() + "-" + mounth + "-" + day);
        },
        error: function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['Message'] + "</h2>");
        }

    });
}