﻿function ShowTask(Task, Status) {
    $('#errors').empty();
    $("#editId").val(Task.IDTask);
    $("#editIdCategory").val(Task.IDCategory);
    $("#editCreate").val(Task.DateCreation);
    $("#editName").val(Task.Name);
    $('#editDescription').val(Task.Description);
    $('#editPriority').val(Task.Priority);
    var dateControl = document.querySelector('input[type="date"]');
    if (Task.DateFinish == null) {
        dateControl.value = "";
    }
    else {
        dateControl.value = Task.DateFinish.substring(0, Task.DateFinish.indexOf("T"));
    }
    var mounth = new Date().getMonth() + 1;
    if (mounth < 10) {
        mounth = "0" + mounth;
    }
    var day = new Date().getDate();
    if (day < 10) {
        day = "0" + day;
    }
    dateControl.setAttribute("min", new Date().getFullYear() + "-" + mounth + "-" + day);
    $.each(Status, function (key, value) {
        $("#my_select").append($('<option value="' + value.IDStatus + '">' + value.Name + '</option>'));
    });
    $("#my_select :contains('" + Task.Status.Name + "')").first().attr("selected", "selected");
}