﻿$(document).ready(function () {
   
    $("#searchUser").click(function (event) {
        event.preventDefault();
        $('#errors').empty();
        SearchUser();
        
    });
    check = "";
    $("#sharingTask").click(function (event) {
        event.preventDefault();
        TaskSharedID = $("#detailsId").text();
        $("#sharingModal").attr("z-index","10");
    });
});
function SearchUser() {
    var name = $('#nameUser').val();
    $.ajax({
        url: '/api/Users/Search?name=' + encodeURIComponent(name),
        type: 'Get',
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $.ajax({
                url: '/api/SharedTask?idTask=' + TaskSharedID,
                type: 'GET',
                dataType: 'json',
                beforeSend: function (xhr) {
                    var token = sessionStorage.getItem(tokenKey);
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success: function (users) {
                    $('#errors').empty();
                        if (data.length == 0) {
                            $("#tableBlock").html("<h1 style='color:red;'>Users not found</h1>");
                        }
                        else {
                            var strResult = "<table class='table table-hover'><th>Name</th><th></th>";

                            $.each(data, function (index, user) {
                                 
                                $.each(users, function (key, value) {
                                    if (user.Id == value.IDUser)
                                         check = "checked";
                                        });
                                strResult += "<tr><td>" + user.UserName + "</td><td><input type='checkbox' "+check+" value=" + user.IdUser + "></td></tr>";
                            });
                            strResult += "</table>";
                            $("#resultSearch").html(strResult);
                        }
                },
                error: function (jxqr, error, status) {
                    var response = jQuery.parseJSON(jxqr.responseText);
                    $('#errors').empty();
                    $('#errors').append("<h2>" + response['Message'] + "</h2>");
                }
            });
        },
                error: function (jxqr, error, status) {
                    var response = jQuery.parseJSON(jxqr.responseText);
                    $('#errors').empty();
                    $('#errors').append("<h2>" + response['Message'] + "</h2>");
                }
    });
}
