﻿$(document).ready(function () {
    $("#addTask").click(function (event) {
        event.preventDefault();
        $('#errors').empty();
        AddTask();
    });
});
function AddTask() {
    $('#errors').empty();
    var Task = {
        IDCategory: CategoryID,
    DateCreation: new Date(),
        Description: $('#newDescription').val(),
            Priority: $('#newPriority').val(),
                DateFinish: $('#newDate').val(),
                    Name: $('#newName').val(),
                        Status :{
        IDStatus: $("#newMy_select :selected").val(),
            Name: $("#newMy_select :selected").text()
    },
};
if (Task.Priority == "")
    Task.Priority = 0;
$.ajax({
    url: '/api/Task',
    type: 'POST',
    data: JSON.stringify(Task),
    contentType: "application/json;charset=utf-8",
    beforeSend: function (xhr) {
        var token = sessionStorage.getItem(tokenKey);
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    },
    success: function (data) {
        $('#errors').empty();
        GetAllTask(CategoryID, startPage, howGive);

                },
error: function (jxqr, error, status) {
    var response = jQuery.parseJSON(jxqr.responseText);
    $('#errors').empty();
    if (response['ModelState']['model.Name']) {

        $.each(response['ModelState']['model.Name'], function (index, item) {
            $('#errors').append("<p>" + item + "</p>");
        });
    }
    if (response['ModelState']['model.Priority']) {

        $.each(response['ModelState']['model.Priority'], function (index, item) {
            $('#errors').append("<p>" + item + "</p>");
        });
    }
}
            });
        }