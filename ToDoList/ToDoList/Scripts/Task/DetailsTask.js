﻿function DetailsItem(el) {
    var id = $(el).attr('data-item');
    $('#errors').empty();
    GetDetailsTask(id);
}
function GetDetailsTask(id) {
    $('#errors').empty();
    $.ajax({
        url: '/api/Tasks?idCategory=' + CategoryID +'&idTask=' + id,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
                    ShowDetailsTask(data[0]);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
function ShowDetailsTask(Task) {
    $('#errors').empty();
    $("#detailsId").text(Task.IDTask);
    $("#detailsIdCategory").text(Task.IDCategory);
    $("#editCreate").text(Task.DateCreation);
    $("#nameTask").text(Task.Name);
    $('#descriptionTask').text(Task.Description);
    $('#priorityTask').text(Task.Priority);
    $('#dateStart').text(Task.DateCreation.substring(0, Task.DateCreation.indexOf("T")));
    var dateFin=""
    if (Task.DateFinish == null) {
        dateFin= "";
    }
    else {
        dateFin = Task.DateFinish.substring(0, Task.DateFinish.indexOf("T"));
    }
    $('#dateFinish').text(dateFin);
    $("#statusTask").text(Task.Status.Name);
}