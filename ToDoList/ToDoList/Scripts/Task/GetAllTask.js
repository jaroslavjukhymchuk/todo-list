﻿function GetAllTask(id, numPage, howGive) {
    $.ajax({
        url: '/api/Tasks?idCategory=' + id + '&pagingParameterModel.pageNumber=' + numPage + '&pagingParameterModel.pageSize=' + howGive,
        type: 'GET',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data, request, textStatus) {
            $('#errors').empty();
            var resp = JSON.parse(textStatus.getResponseHeader('paging-headers'));
            WriteResponse(data,resp);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}