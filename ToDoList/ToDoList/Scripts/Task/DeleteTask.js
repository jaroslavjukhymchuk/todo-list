﻿function DeleteItem(el) {
    var id = $(el).attr('data-item');
    $('#errors').empty();
    DeleteTask(id);
}
function DeleteTask(id) {
    $.ajax({
        url: '/api/Task/' + id,
        type: 'DELETE',
        contentType: "application/json;charset=utf-8",
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $('#errors').empty();
            GetAllTask(CategoryID, startPage, howGive);
},
error: function (x, y, z) {
    alert(x + '\n' + y + '\n' + z);
}
            });
        }