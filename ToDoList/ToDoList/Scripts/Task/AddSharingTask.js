﻿$(document).ready(function () {
    $("#newSharingTask").click(function (event) {
        event.preventDefault();
        $('#errors').empty();
        var boxes = $("input:checkbox");
        sharingTask = new Array();
        deleteSharing = new Array();
        for (var i = 0; i < boxes.length; i++) {
            var box = boxes[i];
            if ($(box).prop('checked')) {
                sharingTask[sharingTask.length] = $(box).val();
            }
            if (!$(box).prop('checked')) {
                deleteSharing[deleteSharing.length] = $(box).val();
            }
        }
        if (check == "" && sharingTask.length > 0)
            AddSharingTask(sharingTask[0]);
        else {
            if (check == "checked" && deleteSharing.length > 0) {
                DeleteSharingTask(deleteSharing[0]);
            }
        }
    });
});

function AddSharingTask(sharingTask) {
    $('#errors').empty();
    var shared = {
        IDUser: sharingTask,
        IDTask: TaskSharedID
    };

    

        $.ajax({
            url: '/api/SharedTask',
            type: 'POST',
            data: JSON.stringify(shared),
            contentType: "application/json;charset=utf-8",
            beforeSend: function (xhr) {
                var token = sessionStorage.getItem(tokenKey);
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            },
            success: function (data) {
                $('#errors').empty();
                alert("Save");
            },
            error: function (jxqr, error, status) {
                var response = jQuery.parseJSON(jxqr.responseText);
                $('#errors').empty();
                $('#errors').append("<h2>" + response['Message'] + "</h2>");

            }
        });
    }
        