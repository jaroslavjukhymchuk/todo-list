﻿function GetTask(id) {
    $.ajax({
        url: '/api/Tasks?idCategory=' + CategoryID+'&idTask='+id,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {

            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $.ajax({
                url: '/api/Statuses',
                type: 'GET',
                dataType: 'json',
                beforeSend: function (xhr) {

                    var token = sessionStorage.getItem(tokenKey);
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success: function (status) {

                    ShowTask(data[0], status);
                },
                error: function (jxqr, error, status) {
                    var response = jQuery.parseJSON(jxqr.responseText);
                    $('#errors').empty();
                    $('#errors').append("<h2>" + response['Message'] + "</h2>");
                }

            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}