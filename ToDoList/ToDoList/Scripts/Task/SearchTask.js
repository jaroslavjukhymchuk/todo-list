﻿$(document).ready(function () {
    $("#search").click(function (event) {
        $('#errors').empty();
        event.preventDefault();
        SearchTask(CategoryID );
});
});
function SearchTask(id) {
   
        var name = $('#name').val()
    $.ajax({
        url: '/api/Tasks?idCategory=' + CategoryID+'&nameTask=' + encodeURIComponent(name),
        type: 'Get',
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data) {
            $('#errors').empty();
            WriteSearchResponse(data);
        },
        error: function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['Message'] + "</h2>");
        }
    });
}
function WriteSearchResponse(tasks) {
    if (tasks.length == 0) {
        $("#tableBlock").html("<h1 style='color:red;'>Tasks not found</h1>");
    }
    else {
        var strResult = "<table class='table table-hover'><th>ID</th><th>Name</th><th>Priority</th><th>DateFinish</th><th></th><th></th><th></th>";
        $.each(tasks, function (index, task) {

            strResult += "<tr><td>" + task.IDTask + "</td><td>" + task.Name + "</td><td>" + task.Priority + "</td> <td>" + task.DateFinish.substring(0, task.DateFinish.indexOf("T")) + "</td><td><a data-toggle='modal' data-target='#myModal' data-item='" + task.IDTask + "' onclick='EditItem(this);' ><span class='glyphicon glyphicon-pencil'></span></a></td><td><a id='delItem' data-item='" + task.IDTask + "' onclick='DeleteItem(this);' ><span class='glyphicon glyphicon-remove'></span></a></td><td><a id='detailsItem' data-toggle='modal' data-target='#detailsTask' data-item='" + task.IDTask + "' onclick='DetailsItem(this);' ><span class='fa fa-search-plus'></span></a></td></tr>";
        });
        strResult += "</table>";
        $("#tableBlock").html(strResult);
    }
    $("#pagination").html("");
   
}