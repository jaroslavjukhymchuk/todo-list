﻿$(document).ready(function () {
    $("#sharedTask").click(function (event) {
        $('#errors').empty();
        GetSharedTask(startPage);
    });
});
function GetPreviosSharedPage() {
    var pageNumber = $("#thisPage").text() - 1;
    $('#errors').empty();
    GetSharedTask( pageNumber);
}
function GetNextSharedPage() {
    var pageNumber = $("#thisPage").text() - 1 + 2;
    $('#errors').empty();
    GetSharedTask( pageNumber);
}
function GetSharedTask(numPage) {
    $.ajax({
        url: '/api/Tasks/User?pagingParameterModel.pageNumber=' + numPage + '&pagingParameterModel.pageSize=' + howGive,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            var token = sessionStorage.getItem(tokenKey);
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (data, request, textStatus) {
            $('#errors').empty();
            var resp = JSON.parse(textStatus.getResponseHeader('paging-headers'));
            WriteSharedResponse(data.Tasks, resp);
        },
        error: function (jxqr, error, status) {
            var response = jQuery.parseJSON(jxqr.responseText);
            $('#errors').empty();
            $('#errors').append("<h2>" + response['Message'] + "</h2>");
        }
    });
}
function WriteSharedResponse(tasks, resp) {
    $('#errors').empty();
    if (tasks.length == 0) {
        $("#tableBlock").html("<h1 style='color:red;'>Tasks not found</h1>");
    }
    else {
        var strResult = "<table class='table table-hover'><th>ID</th><th>Name</th><th>Priority</th><th>DateFinish</th><th></th><th></th><th></th>";
        $.each(tasks, function (index, task) {
            var dateFin = "";
            if (task.DateFinish == null) {
                dateFin = "";
            }
            else {
                dateFin = task.DateFinish.substring(0, task.DateFinish.indexOf("T"));
            }
            strResult += "<tr><td>" + task.IDTask + "</td><td>" + task.Name + "</td><td>" + task.Priority + "</td> <td>" + dateFin + "</td><td><a data-toggle='modal' data-target='#myModal' data-item='" + task.IDTask + "' onclick='EditItem(this);' ><span class='glyphicon glyphicon-pencil'></span></a></td><td><a id='delItem' data-item='" + task.IDTask + "' onclick='DeleteItem(this);' ><span class='glyphicon glyphicon-remove'></span></a></td><td><a id='detailsItem' data-toggle='modal' data-target='#detailsTask' data-item='" + task.IDTask + "' onclick='DetailsItem(this);' ><span class='fa fa-search-plus'></span></a></td></tr>";
        });
        strResult += "</table>";
        $("#tableBlock").html(strResult);
    }
    var dis;
    if (resp.previousPage == "No") {
        dis = "disabled";
    }

    var pagination = "<nav aria-label='Page navigation example'><ul class='pagination'>";
    pagination += "<li class='page-item'><a class='" + dis + "'  id='previousShared'  onclick='GetPreviosSharedPage();'>Previous</a></li>";
    pagination += "<li class='page-item'><a class='page-link'  id='thisPage'>" + resp.currentPage + "</a></li>";
    if (resp.nextPage == "Yes") {
        dis = "";
    } else {
        dis = "disabled";
    }
    pagination += "<li class='page-item'><a class='" + dis + "' id='nextShared' onclick='GetNextSharedPage();'>Next</a></li></ul></nav>";
    $("#pagination").html(pagination);
}