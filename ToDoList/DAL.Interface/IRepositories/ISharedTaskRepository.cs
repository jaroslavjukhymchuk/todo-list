﻿using DAL.Interface.IDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.IRepositories
{
    public interface ISharedTaskRepository
    {
        bool Create(IDataSharedTask dto);

        bool Delete(int id);

        IEnumerable<IDataSharedTask> GetUsers(int idTask);

    }
}
