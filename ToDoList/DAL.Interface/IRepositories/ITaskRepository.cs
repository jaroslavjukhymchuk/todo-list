﻿using System.Collections.Generic;
using DAL.Interface.IDTOs.IDataTasks;

namespace DAL.Interface.IRepositories
{
    public interface ITaskRepository
    {

        /// <summary>
        /// Create new task in database
        /// </summary>
        /// <param name="dto">dto task</param>
        /// <returns>true if the created object else false</returns>
        bool Create(IDataTask dto);

        /// <summary>
        /// update task in database
        /// </summary>
        /// <param name="dto">update dto task</param>
        /// <returns>true if the update object else false</returns>
        bool Update(IDataTask dto);

        /// <summary>
        /// delete task in database
        /// </summary>
        /// <param name="id">id task what need delete</param>
        /// <returns>true if the deleted object else false</returns>
        bool Delete(int id);

        /// <summary>
        /// Get task by id
        /// </summary>
        /// <param name="id">task id</param>
        /// <returns>task dto</returns>
        IDataTask GetById(int id);

        /// <summary>
        /// Get task by user id
        /// </summary>
        /// <param name="id">user id</param>
        /// <param name="numPage">number page what need</param>
        /// <param name="howGive">how task give</param>
        /// <returns>list short form task</returns>
        IDataListShortTask GetByUserId(int id, int numPage, int howGive);

        /// <summary>
        /// Get list short task with paggination by category id
        /// </summary>
        /// <param name="idCategory">id category what need task </param>
        /// <param name="numPage">number page what need</param>
        /// <param name="howGive">how task give</param>
        /// <returns>list short form task</returns>
        IDataListShortTask GetByCategory(int idCategory, int numPage, int howGive);

        /// <summary>
        /// Get tasks by name
        /// </summary>
        /// <param name="name">name task what need</param>
        /// <param name="categoryId">category id what need tasks</param>
        /// <returns>list dto tasks</returns>
        IEnumerable<IDataShortTask> GetByName(int categoryId, string name);
    }
}
