﻿using System.Collections.Generic;
using DAL.Interface.IDTOs;

namespace DAL.Interface.IRepositories
{
    public interface IStatusRepository
    {

        /// <summary>
        /// Create new status in database
        /// </summary>
        /// <param name="dto">dto status</param>
        /// <returns>true if the created object else false</returns>
        bool Create(IDataStatus dto);

        /// <summary>
        /// update status in database
        /// </summary>
        /// <param name="dto">update dto status</param>
        /// <returns>true if the update object else false</returns>
        bool Update(IDataStatus dto);

        /// <summary>
        /// delete status in database
        /// </summary>
        /// <param name="id">id status what need delete</param>
        /// <returns>true if the deleted object else false</returns>
        bool Delete(int id);

        /// <summary>
        /// Get status by id
        /// </summary>
        /// <param name="id">status id</param>
        /// <returns>status dto</returns>
        IDataStatus GetById(int id);

        /// <summary>
        /// Get all status
        /// </summary>
        /// <returns>list status dto</returns>
        IEnumerable<IDataStatus> GetAll();
    }
}
