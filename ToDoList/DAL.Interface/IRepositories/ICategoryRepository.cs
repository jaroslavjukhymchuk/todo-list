﻿using System.Collections.Generic;
using DAL.Interface.IDTOs;

namespace DAL.Interface.IRepositories
{
    public interface ICategoryRepository
    {
        /// <summary>
        /// Create new category in database
        /// </summary>
        /// <param name="dto">dto Category</param>
        /// <returns>true if the created object else false</returns>
        bool Create(IDataCategory dto);

        /// <summary>
        /// update category in database
        /// </summary>
        /// <param name="dto">update dto Category</param>
        /// <returns>true if the update object else false</returns>
        bool Update(IDataCategory dto);

        /// <summary>
        /// delete category in database
        /// </summary>
        /// <param name="id">id category what need delete</param>
        /// <returns>true if the deleted object else false</returns>
        bool Delete(int id);

        /// <summary>
        /// Get category by  id
        /// </summary>
        /// <param name="id">id category</param>
        /// <returns>dto category</returns>
        IDataCategory GetById(int idUser, int id);

        /// <summary>
        /// Get all categories by user id
        /// </summary>
        /// <param name="userId">user id what need categories</param>
        /// <returns>list category</returns>
        IEnumerable<IDataCategory> GetByUser(int userId);

        /// <summary>
        /// Get category by name
        /// </summary>
        /// <param name="name">name categories what need</param>
        /// <param name="userId">user id what need categories</param>
        /// <returns>dto category</returns>
        IEnumerable<IDataCategory> GetByName(int userId, string name);
    }
}
