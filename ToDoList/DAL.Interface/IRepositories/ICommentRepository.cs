﻿using System.Collections.Generic;
using DAL.Interface.IDTOs;

namespace DAL.Interface.IRepositories
{
    public interface ICommentRepository
    {

        /// <summary>
        /// Create new comment in database
        /// </summary>
        /// <param name="dto">dto Comment</param>
        /// <returns>true if the created object else false</returns>
        bool Create(IDataComment dto);

        /// <summary>
        /// update comment in database
        /// </summary>
        /// <param name="dto">update dto comment</param>
        /// <returns>true if the update object else false</returns>
        bool Update(IDataComment dto);

        /// <summary>
        /// delete comment in database
        /// </summary>
        /// <param name="id">id comment what need delete</param>
        /// <returns>true if the deleted object else false</returns>
        bool Delete(int id);

        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="id">comment id</param>
        /// <returns>comment dto</returns>
        IDataComment GetById(int id);

        /// <summary>
        /// Get all comment by task id
        /// </summary>
        /// <param name="taskId">task id what need comment</param>
        /// <returns>list comment</returns>
        IEnumerable<IDataComment> GetByTask(int taskId);
    }
}
