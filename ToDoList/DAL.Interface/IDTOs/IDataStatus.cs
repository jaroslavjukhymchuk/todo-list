﻿namespace DAL.Interface.IDTOs
{
    public interface IDataStatus
    {
        int IDStatus { get; }

        string Name { get; }

    }
}
