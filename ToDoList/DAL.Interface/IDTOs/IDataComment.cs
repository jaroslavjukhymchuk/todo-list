﻿using System;

namespace DAL.Interface.IDTOs
{
    public interface IDataComment
    {
        int IDComment { get; }

        int IDTask { get; }

        string Description { get; }

        DateTime DateCreation { get; }

        int IDUser { get; }
    }
}
