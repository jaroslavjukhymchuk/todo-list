﻿using System.Collections.Generic;

namespace DAL.Interface.IDTOs.IDataTasks
{
    public interface IDataListShortTask
    {
        int TotalPages { get; }

        IEnumerable<IDataShortTask> Tasks { get; }
    }
}
