﻿using System;

namespace DAL.Interface.IDTOs.IDataTasks
{
    public interface IDataTask
    {
        int IDTask { get; }

        int IDCategory { get; }

        string Name { get; }

        string Description { get; }

        int Priority { get; }

        IDataStatus Status { get; }

        DateTime DateCreation { get; }

        DateTime? DateFinish { get; }
    }
}
