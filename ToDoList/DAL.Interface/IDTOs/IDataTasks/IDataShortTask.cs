﻿using System;

namespace DAL.Interface.IDTOs.IDataTasks
{
    public interface IDataShortTask
    {
        int IDTask { get; }

        int IDCategory { get; }

        string Name { get; }

        int Priority { get; }

        DateTime? DateFinish { get; }
        string UserName { get; }
    }
}
