﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.IDTOs
{
    public interface IDataSharedTask
    {
        int IDSharedTask { get; }

        int IDTask { get; }

        int IDUser { get; }
    }
}
