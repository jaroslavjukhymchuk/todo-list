﻿namespace DAL.Interface.IDTOs
{
    public  interface IDataCategory
    {
        int IDCategory { get; }

        int IDUser { get; }

        string Name { get; }
    }
}
